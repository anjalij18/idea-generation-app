(self["webpackChunkIdeaApp"] = self["webpackChunkIdeaApp"] || []).push([["src_app_tab1_tab1_module_ts"],{

/***/ 42580:
/*!*********************************************!*\
  !*** ./src/app/tab1/tab1-routing.module.ts ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab1PageRoutingModule": function() { return /* binding */ Tab1PageRoutingModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab1.page */ 46923);




const routes = [
    {
        path: '',
        component: _tab1_page__WEBPACK_IMPORTED_MODULE_0__.Tab1Page,
    }
];
let Tab1PageRoutingModule = class Tab1PageRoutingModule {
};
Tab1PageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], Tab1PageRoutingModule);



/***/ }),

/***/ 2168:
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.module.ts ***!
  \*************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab1PageModule": function() { return /* binding */ Tab1PageModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab1.page */ 46923);
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../explore-container/explore-container.module */ 581);
/* harmony import */ var _tab1_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tab1-routing.module */ 42580);








let Tab1PageModule = class Tab1PageModule {
};
Tab1PageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule,
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_1__.ExploreContainerComponentModule,
            _tab1_routing_module__WEBPACK_IMPORTED_MODULE_2__.Tab1PageRoutingModule
        ],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_0__.Tab1Page]
    })
], Tab1PageModule);



/***/ }),

/***/ 46923:
/*!***********************************!*\
  !*** ./src/app/tab1/tab1.page.ts ***!
  \***********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab1Page": function() { return /* binding */ Tab1Page; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_tab1_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./tab1.page.html */ 5683);
/* harmony import */ var _tab1_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tab1.page.scss */ 99474);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _services_hero_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/hero.service */ 19405);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ 16738);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
var Tab1Page_1;





// import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";


let Tab1Page = Tab1Page_1 = class Tab1Page {
    constructor(heroService, loadingController) {
        this.heroService = heroService;
        this.loadingController = loadingController;
        this.section = "desc";
        this.allData = {};
        this.allDd = [];
    }
    segmentChanged(ev) {
        console.log('Segment changed', ev);
        console.log('Segment changed to', ev.detail.value);
        this.section = ev.detail.value;
    }
    // ionViewDidEnter(){
    // }
    ionViewWillEnter() {
        //////////////////
        this.allData.requestNumber = undefined;
        this.allData.ideaDesc = undefined;
        this.allData.approxSavings = undefined;
        this.allData.approxWeight = undefined;
        this.allData.ideaResource = undefined;
        this.allData.otherCat = undefined;
        this.allData.mobileNumber = undefined;
        //////////////////
        this.allData.filesArray = [];
        this.allData.partsAddArray = [
            {
                index: 0,
                part_name: "",
                part_type: "ADD"
            }
        ];
        this.allData.partsModArray = [
            {
                index: 0,
                part_name: "",
                part_type: "MOD"
            }
        ];
        this.allData.modelAppArray = [
            {
                index: 0,
                model_name: ""
            }
        ];
        this.allData.fieldsArray = [
            {
                index: 0,
                field_name: "",
                field_id: "",
                value: null
            }
        ];
        this.allData.configValues = [];
        this.allData.vehPlatformsArray = [];
        this.allData.sectorsArray = [];
        this.allData.systemArray = [];
        this.allData.ideacategoryArray = [];
        this.allData.sector = undefined;
        this.allData.vehiclePlt = undefined;
        this.allData.appSys = undefined;
        this.allData.idaCat = undefined;
        this.allData.catBtn = false;
        this.allData.showSaveBtn = true;
        this.allData.showSearchMenu = true;
        this.allData.alreadySavedPartsCount = 0;
        this.allData.alreadySavedDocsCount = 0;
        this.allData.alreadySavedFieldsCount = 0;
        this.allData.alreadyDelPartsCount = 0;
        this.allData.alreadySavedModelCount = 0;
        this.allData.alreadySavedDocs = [];
        this.allData.alreadySavedParts = [];
        this.allData.alreadyDelParts = [];
        this.allData.alreadySavedModel = [];
        this.allData.alreadySavedFields = [];
        debugger;
        this._getRoles();
    }
    static rolesParam() {
        var parameters = [];
        parameters['dn'] = "";
        return parameters;
    }
    mobValidation(val) {
        debugger;
        let num = val.toString();
        if (num.length <= 10) {
            return val;
        }
        else {
            let val1 = num.substr(0, 10);
            return Number(val1);
        }
    }
    _getRoles() {
        var method = 'GetRoles';
        var parameters = [];
        this.callSubCode12(parameters, method, '', 'roles');
    }
    _doFurtherWithRoles(obj) {
        let that = this;
        debugger;
        if (obj != undefined) {
            console.log("check roles123: ", obj);
            let mnmCount = that.heroService.otoa(obj[0].role).filter((d) => {
                return d.description == "IGP_MnM";
            });
            console.log("check mnmCount: ", mnmCount);
            let supplierCount = that.heroService.otoa(obj[0].role).filter((d) => {
                return d.description == "IGP_Supplier";
            });
            console.log("check supplierCount: ", supplierCount);
            if (mnmCount.length > 0) {
                that.allData.showSearchMenu = true;
                if (localStorage.getItem("loggedinuser") != null) {
                    let user = localStorage.getItem("loggedinuser");
                    let params = {
                        UM_USER_ID: user
                    };
                    var method = 'GetUserMasterObject';
                    var parameters = [];
                    that.callSubCode12(parameters, method, params, 'userData');
                }
            }
            else if (supplierCount.length > 0) {
                that.allData.showSearchMenu = false;
                that.allData.showSaveBtn = false;
                that._getSupplierData();
            }
        }
    }
    _getSupplierData() {
        let that = this;
        let user = localStorage.getItem("loggedinuser");
        let params = {
            vendorCode: user
        };
        var method = 'GetSupplierDetail';
        var parameters = [];
        that.callSubCode12(parameters, method, params, 'supplierData');
    }
    checkValidation(data, eve) {
        console.log("check event: ", eve);
        let nData = data.toString();
        let n = nData.split(".");
        if (n[0].length > 25) {
            return 0;
        }
        else {
            if (n[1] != undefined) {
                if (n[1].length > 2) {
                    return 0;
                }
                else {
                    return data;
                }
            }
            else {
                return data;
            }
        }
    }
    static userParam(user) {
        var parameters = [];
        parameters["UM_USER_ID"] = user;
        return parameters;
    }
    static supplierParam(user) {
        var parameters = [];
        parameters["vendorCode"] = user;
        return parameters;
    }
    callSubCode12(parameters, method, params, key) {
        let that = this;
        if (key == "roles") {
            parameters[method + " xmlns='http://schemas.cordys.com/1.0/ldap'"] = Tab1Page_1.rolesParam();
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else {
            if (key == "userData") {
                parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = Tab1Page_1.userParam(params.UM_USER_ID);
                parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
                // parameters[method + ' xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/" xmlns="http://schemas.cordys.com/igp" preserveSpace="no" qAccess="0" qValues=""'] = Tab1Page.userParam(params.UM_USER_ID);
                // parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
            }
            else if (key == "supplierData") {
                parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = Tab1Page_1.supplierParam(params.vendorCode);
                parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
            }
        }
        that.loadingController.create({
            spinner: 'bubbles',
            message: 'Please wait...'
        }).then((loadEl) => {
            loadEl.present();
            that.heroService.testService1(parameters, function (err, response) {
                loadEl.dismiss();
                if (response) {
                    let s = response['SOAP:Envelope'];
                    let faultString = s['SOAP:Body'][0]['SOAP:Fault'];
                    if (faultString != undefined) {
                        if (faultString[0].faultstring[0]._ == "Unable to bind the artifact to a SAML assertion.") {
                            that._toastrErrorMsg("Token is expired. Please logout and login to access the app.");
                            return;
                        }
                        console.log("fault string: ", faultString[0].faultstring[0]._);
                    }
                    debugger;
                    if (key == "roles") {
                        let a = s['SOAP:Body'][0].GetRolesResponse;
                        if (a != undefined) {
                            console.log("check roles: ", a[0]);
                            let obj = $.cordys.json.findObjects(a[0], "user");
                            if (obj != undefined) {
                                that._doFurtherWithRoles(obj);
                            }
                        }
                        else {
                            that.heroService._toastrErrorMsg("Error occured while fetching roles data. Please contact administrator.");
                        }
                    }
                    else if (key == "userData") {
                        let a = s['SOAP:Body'][0].GetUserMasterObjectResponse;
                        let b = a[0].tuple;
                        that.allData.userDetails = b[0].old[0].USER_MASTER[0];
                        console.log("get userdetails:", that.allData.userDetails);
                        that._getConfigData();
                    }
                    else if (key == "supplierData") {
                        let a = s['SOAP:Body'][0].GetSupplierDetailResponse;
                        if (a != undefined) {
                            console.log("check supplierData: ", a[0]);
                            let obj = $.cordys.json.findObjects(a[0], "SUPPLIER_MASTER");
                            if (obj != undefined) {
                                if (obj.length > 0) {
                                    that.allData.userDetails = obj[0];
                                }
                                else {
                                    that.heroService._toastrErrorMsg("Supplier's data not found. Please try logout and login again.");
                                }
                            }
                        }
                        else {
                            that.heroService._toastrErrorMsg("Error occured while fetching supplier's data. Please contact administrator.");
                        }
                        that._getConfigData();
                    }
                }
                else {
                    console.log("error found in err tag: ", err);
                    console.log("no response cought");
                    console.log("not getting response becz of err: ", err);
                }
            });
        });
    }
    static serviceParam(reqType) {
        var parameters = [];
        parameters["reqType"] = reqType;
        return parameters;
    }
    static UIDParam(uidNum, key) {
        var parameters = [];
        if (key == "sendMail") {
            parameters["UIDNumber"] = uidNum;
        }
        else {
            if (key == "_getAddedParts" || key == "_getIdeaDocOnUID" || key == "_getDeletedParts" || key == "_getSavedModels") {
                parameters["UID"] = uidNum;
            }
        }
        return parameters;
    }
    _getSectorList() {
        let that = this;
        let params = {
            reqType: "SECTOR"
        };
        var method = 'GetLOVObjectForIdeaGen';
        var parameters = [];
        that.callSubCode(parameters, method, params, 'sector');
        let params1 = {
            reqType: "VAVE_LEVER"
        };
        that.callSubCode(parameters, method, params1, 'ideaCat');
    }
    callSubCode(parameters, method, params, key) {
        let that = this;
        parameters[method + ' xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/" xmlns="http://schemas.cordys.com/igp" preserveSpace="no" qAccess="0" qValues=""'] = Tab1Page_1.serviceParam(params.reqType);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        // this.url.startLoading().present();
        that.heroService.testService1(parameters, function (err, response) {
            // that.url.stopLoading();
            // debugger
            if (response) {
                var s = response['SOAP:Envelope'];
                var a = s['SOAP:Body'][0].GetLOVObjectForIdeaGenResponse;
                var b = a[0].tuple;
                let daa = b.map((d) => { return d.old[0].LOV_MASTER_AUTO[0]; });
                console.log("get drop down debugger:", b);
                if (key == 'sector') {
                    that.allData.sectorsArray = daa;
                }
                else if (key == 'VehiclePlt') {
                    that.allData.vehiclePlt = undefined;
                    that.allData.vehPlatformsArray = daa;
                    let params1 = {
                        reqType: "SYSTEM###" + that.allData.sector
                    };
                    let parameters123 = [];
                    let method = 'GetLOVObjectForIdeaGen';
                    that.callSubCode(parameters123, method, params1, 'appSys');
                }
                else if (key == 'appSys') {
                    that.allData.appSys = undefined;
                    that.allData.systemArray = daa;
                }
                else if (key == 'ideaCat') {
                    that.allData.ideacategoryArray = daa;
                }
            }
            else {
                console.log("error found in err tag: ", err);
                console.log("no response cought");
                console.log("not getting response becz of err: ", err);
                // that.toastCtrl.create({
                //   message: "We are unable to connect server. Please check your net connection or contact admin if issue still persists.",
                //   position: 'bottom',
                //   duration: 3000
                // }).present();
                // that.isUnchanged1 = false;
            }
        });
        // that.soapService.post(that.urls._baseURL, "GenerateSAML", {
        //   username: that.loginForm.value.username,
        //   password: that.loginForm.value.password
        // }).then((result) => {
        //   console.log("logged in result: ", result);
        // }).catch(error => {
        //   console.log("check error: ", error);
        // });
        // that.router.navigateByUrl('idea/menu');
    }
    callthis(value) {
        debugger;
        console.log("im here: ", value);
        let that = this;
        if (value != null) {
            let params = {
                reqType: "MODEL_AFFECTED###" + value
            };
            var method = 'GetLOVObjectForIdeaGen';
            var parameters = [];
            that.callSubCode(parameters, method, params, 'VehiclePlt');
            // let params1 = {
            //   reqType: "SYSTEM###" + value
            // }
            // that.callSubCode(parameters, method, params1, 'appSys');
        }
    }
    _downloadFile(file) {
        console.log("check file object: ", file);
        var filPath = "http://43.242.214.148:81/home/devmahindra/" + file.file_path.split("shared/")[1];
        // var filPath = window.location.href.split("/com")[0] + "/" + file.file_path.split("shared/")[1];
        // http://43.242.214.148:81/home/devmahindra/MAHINDRA_UPLOADS/IGP/Doc_Uploads/Screenshot_20210716-183934_Idea App.jpg
        console.log("check file path: ", filPath);
        var dnldFile;
        dnldFile = document.createElement("A");
        dnldFile.href = filPath;
        dnldFile.download = filPath.substr(filPath.lastIndexOf('/') + 1).replace(/^.*[\\\/]/, "");
        console.log("check substracted file path: ", filPath.substr(filPath.lastIndexOf('/') + 1).replace(/^.*[\\\/]/, ""));
        console.log("dnldFile: ", dnldFile);
        document.body.appendChild(dnldFile);
        dnldFile.click();
        document.body.removeChild(dnldFile);
    }
    onChangeCat(value) {
        console.log("selected idea: ", value);
        let that = this;
        if (value == 'Other') {
            // if (value == 'VAVE_LEVER_OTHER') { // checking with id
            that.allData.catBtn = true;
        }
        else {
            that.allData.catBtn = false;
        }
    }
    // public tempmodalRef: BsModalRef;
    _openModalXlsx(template) {
        // let that = this;
        $("#uploadBtn").click();
        // let temp = that.allData.filesArray.filter((d) => {
        //   return d.file_name == "";
        // });
        // if (temp.length == 0) {
        //   that.allData.filesArray.push({
        //     index: that.allData.filesArray.length,
        //     file_name: "",
        //     file_path: "",
        //     flag: false
        //   })
        // }
        // that.allData.base64ContentFinal = "";
        // that.allData.UploadFile = "Choose a file or drag it here";
        // that.tempmodalRef = this.modalService.show(template, { class: "modal-lg" });
    }
    _fileBrowseHandlerFinal(files) {
        this._prepareFilesListFinal(files.files);
    }
    _prepareFilesListFinal(files) {
        let that = this;
        // that.allData.base64ContentFinal = "";
        // that.allData.UploadFile = "Choose a file or drag it here";
        // that._setupReader(files[0]);
        debugger;
        that.allData.filesArray.push({
            file_name: "",
            file_path: ""
        });
        for (let i = 0; i < files.length; i++) {
            that.allData.base64ContentFinal = "";
            that.allData.UploadFile = "Choose a file or drag it here";
            that._setupReader(files[i]);
        }
    }
    _setupReader(file) {
        let that = this;
        var reader = new FileReader();
        reader.onload = function (e) {
            var temp = reader.result;
            that.allData.base64ContentFinal = String(temp).split(",")[1];
            that.allData.UploadFile = file.name;
            for (let i = 0; i < that.allData.filesArray.length; i++) {
                if (file.name == that.allData.filesArray[i].file_name) {
                    that._toastrErrorMsg("This file is already attached.");
                    break;
                }
                // that.allData.filesArray.push({
                //   file_name: "",
                //   file_path: ""
                // });
                if (that.allData.filesArray[i].file_name == "") {
                    that.allData.filesArray[i].file_name = file.name;
                    that.allData.filesArray[i].file_path = that.allData.base64ContentFinal;
                    break;
                }
            }
            that._cancel();
        };
        reader.readAsDataURL(file);
    }
    _cancel() {
        let that = this;
        // that.tempmodalRef.hide();
        that.allData.errorMsg = "";
        that.allData.fileName = "";
        that.allData.base64Content = "";
        that.allData.selectedFile = "Choose a file or drag it here";
    }
    _addPartRow(_tYpe) {
        let that = this;
        debugger;
        if (_tYpe == "ADD") {
            let temp = that.allData.partsAddArray.filter((d) => {
                return d.part_name == "";
            });
            if (temp.length == 0) {
                that.allData.partsAddArray.push({
                    index: that.allData.partsAddArray.length,
                    part_name: "",
                    part_type: "ADD"
                });
                setTimeout(() => {
                    that.rows.last.nativeElement.focus();
                }, 0);
            }
        }
        else if (_tYpe == "MOD") {
            let temp = that.allData.partsModArray.filter((d) => {
                return d.part_name == "";
            });
            if (temp.length == 0) {
                that.allData.partsModArray.push({
                    index: that.allData.partsModArray.length,
                    part_name: "",
                    part_type: "MOD"
                });
                setTimeout(() => {
                    that.rows1.last.nativeElement.focus();
                }, 0);
            }
        }
        else if (_tYpe == "Model") {
            let temp = that.allData.modelAppArray.filter((d) => {
                return d.model_name == "";
            });
            if (temp.length == 0) {
                that.allData.modelAppArray.push({
                    index: that.allData.modelAppArray.length,
                    model_name: ""
                });
                setTimeout(() => {
                    that.rows3.last.nativeElement.focus();
                }, 0);
            }
        }
        else if (_tYpe == "Field") {
            let rtml = that.allData.fieldsArray;
            rtml.push({
                field_id: "",
                field_name: "",
                value: null
            });
            that.allData.fieldsArray = rtml;
        }
    }
    _deletePart(type, index) {
        let that = this;
        if (type == 'ADD') {
            for (let i = 0; i < that.allData.partsAddArray.length; i++) {
                if (index == i) {
                    if (that.allData.partsAddArray[i].prm_key != undefined) {
                        that._deletePartsService(that.allData.partsAddArray[i].prm_key);
                    }
                    else {
                        if (that.allData.partsAddArray.length > 1) {
                            that.allData.partsAddArray.splice(i, 1);
                        }
                        else {
                            that.allData.partsAddArray[i].part_name = '';
                            that.allData.partsAddArray[i].part_type = "ADD";
                        }
                    }
                }
            }
        }
        else if (type == 'MOD') {
            for (let i = 0; i < that.allData.partsModArray.length; i++) {
                if (index == i) {
                    if (that.allData.partsModArray[i].prm_key != undefined) {
                        that._deletePartsService(that.allData.partsModArray[i].prm_key);
                    }
                    else {
                        if (that.allData.partsModArray.length > 1) {
                            that.allData.partsModArray.splice(i, 1);
                        }
                        else {
                            that.allData.partsModArray[i].part_name = '';
                            that.allData.partsModArray[i].part_type = "MOD";
                        }
                    }
                }
            }
        }
        else if (type == 'Model') {
            for (let i = 0; i < that.allData.modelAppArray.length; i++) {
                if (index == i) {
                    if (that.allData.modelAppArray[i].prm_key != undefined) {
                        that._deleteModelService(that.allData.modelAppArray[i].prm_key);
                    }
                    else {
                        if (that.allData.modelAppArray.length > 1) {
                            that.allData.modelAppArray.splice(i, 1);
                        }
                        else {
                            that.allData.modelAppArray[i].model_name = '';
                        }
                    }
                }
            }
        }
    }
    _deletePartsService(prm_key) {
        let that = this;
        let dataObj3 = {
            tuple: {
                old: {
                    IGP_PART_DETAILS: {
                        PRT_SEQ_NO: prm_key
                    }
                }
            }
        };
        var parameters = [];
        that.callSubCodeBasic(parameters, dataObj3, "UpdateIgpPartDetails", 'http://schemas.cordys.com/igp', 'deleteParts', '');
    }
    _deleteModelService(prm_key) {
        let that = this;
        let dataObj3 = {
            tuple: {
                old: {
                    IGP_MODEL_APP: {
                        IMA_SEQ_NO: prm_key
                    }
                }
            }
        };
        var parameters = [];
        that.callSubCodeBasic(parameters, dataObj3, "UpdateIgpModelApp", 'http://schemas.cordys.com/igp', 'deleteModels', '');
    }
    _deleteFile(index) {
        let that = this;
        for (let i = 0; i < that.allData.filesArray.length; i++) {
            if (index == i) {
                // that.allData.filesArray.splice(i, 1);
                if (that.allData.filesArray[i].prm_key != undefined) {
                    that._deleteFileService(that.allData.filesArray[i].prm_key);
                }
                else {
                    that.allData.filesArray.splice(i, 1);
                    // if (that.allData.filesArray[i].file_name != '') {
                    //   if (that.allData.filesArray.length > 5) {
                    //     that.allData.filesArray.splice(i, 1);
                    //   } else {
                    //     that.allData.filesArray[i].file_name = '';
                    //     that.allData.filesArray[i].file_path = '';
                    //     that.allData.filesArray[i].flag = false;
                    //   }
                    // } else {
                    //   that.allData.filesArray[i].flag = false;
                    // }
                }
            }
        }
    }
    _deleteFileService(prm_key) {
        let that = this;
        let dataObj3 = {
            tuple: {
                old: {
                    IGP_DOCS: {
                        IGD_SNO: prm_key
                    }
                }
            }
        };
        var parameters = [];
        that.callSubCodeBasic(parameters, dataObj3, "UpdateIgpDocs", 'http://schemas.cordys.com/igp', 'deleteFiles', '');
    }
    static saveParam(data) {
        let dataObj;
        if (data.tuple.old == undefined) {
            dataObj = {
                "tuple": {
                    "new": {
                        "IGP_IDEA_DETAILS": {
                            "IID_IDEA_DESCRIPTION": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_DESCRIPTION,
                            "IID_APP_SAVINGS": data.tuple.new.IGP_IDEA_DETAILS.IID_APP_SAVINGS,
                            "IID_APP_WT_REDUCTION": data.tuple.new.IGP_IDEA_DETAILS.IID_APP_WT_REDUCTION,
                            "IID_IDEA_RESOURCE": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_RESOURCE,
                            "IID_IDEA_CAT": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_CAT,
                            "IID_VEHICLE_PLAT": data.tuple.new.IGP_IDEA_DETAILS.IID_VEHICLE_PLAT,
                            "IID_MOBILE_NUMBER": data.tuple.new.IGP_IDEA_DETAILS.IID_MOBILE_NUMBER,
                            "IID_APP_SYSTEM": data.tuple.new.IGP_IDEA_DETAILS.IID_APP_SYSTEM,
                            "IID_IDEA_GEN_TOKEN_NO": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_GEN_TOKEN_NO,
                            "IID_IDEA_GEN_NAME": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_GEN_NAME,
                            "IID_IDEA_GEN_EMAIL": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_GEN_EMAIL,
                            "IID_IDEA_DATE": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_DATE,
                            "IID_STATUS": data.tuple.new.IGP_IDEA_DETAILS.IID_STATUS,
                            "IID_IS_ACTIVE": data.tuple.new.IGP_IDEA_DETAILS.IID_IS_ACTIVE,
                            // "IID_IDEA_TITLE": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_TITLE,
                            "IID_SECTOR": data.tuple.new.IGP_IDEA_DETAILS.IID_SECTOR,
                            "IID_IDEA_CAT_OTH": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_CAT_OTH
                        }
                    }
                }
            };
        }
        else {
            dataObj = {
                "tuple": {
                    "old": {
                        "IGP_IDEA_DETAILS": {
                            "IID_REQUEST_ID": data.tuple.old.IGP_IDEA_DETAILS.IID_REQUEST_ID
                        }
                    },
                    "new": {
                        "IGP_IDEA_DETAILS": {
                            "IID_IDEA_DESCRIPTION": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_DESCRIPTION,
                            "IID_APP_SAVINGS": data.tuple.new.IGP_IDEA_DETAILS.IID_APP_SAVINGS,
                            "IID_APP_WT_REDUCTION": data.tuple.new.IGP_IDEA_DETAILS.IID_APP_WT_REDUCTION,
                            "IID_IDEA_RESOURCE": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_RESOURCE,
                            "IID_IDEA_CAT": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_CAT,
                            "IID_VEHICLE_PLAT": data.tuple.new.IGP_IDEA_DETAILS.IID_VEHICLE_PLAT,
                            "IID_MOBILE_NUMBER": data.tuple.new.IGP_IDEA_DETAILS.IID_MOBILE_NUMBER,
                            "IID_APP_SYSTEM": data.tuple.new.IGP_IDEA_DETAILS.IID_APP_SYSTEM,
                            "IID_IDEA_GEN_TOKEN_NO": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_GEN_TOKEN_NO,
                            "IID_IDEA_GEN_NAME": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_GEN_NAME,
                            "IID_IDEA_GEN_EMAIL": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_GEN_EMAIL,
                            "IID_IDEA_DATE": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_DATE,
                            "IID_STATUS": data.tuple.new.IGP_IDEA_DETAILS.IID_STATUS,
                            "IID_IS_ACTIVE": data.tuple.new.IGP_IDEA_DETAILS.IID_IS_ACTIVE,
                            // "IID_IDEA_TITLE": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_TITLE,
                            "IID_SECTOR": data.tuple.new.IGP_IDEA_DETAILS.IID_SECTOR,
                            "IID_IDEA_CAT_OTH": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_CAT_OTH
                        }
                    }
                }
            };
        }
        return dataObj;
    }
    callSubCodeNew(parameters, params, funcKey) {
        let that = this;
        parameters["UpdateIgpIdeaDetails xmlns='http://schemas.cordys.com/igp'"] = Tab1Page_1.saveParam(params);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        that.loadingController.create({
            message: "Please wait...",
            spinner: 'bubbles'
        }).then((loadEl) => {
            loadEl.present();
            that.heroService.testService1(parameters, function (err, response) {
                loadEl.dismiss();
                debugger;
                if (response) {
                    var s = response['SOAP:Envelope'];
                    if (s['SOAP:Body'][0]["SOAP:Fault"] != undefined) {
                        if (s['SOAP:Body'][0]["SOAP:Fault"][0].faultstring[0]._ == "Error occurred while processing the request. Error from database server or driver.ORA-01438: value larger than specified precision allowed for this column\n.") {
                            if (funcKey == 'Save')
                                that.heroService._toastrErrorMsg("Error occured while saving data. Please provide (14+2) digits for Approx. Savings (RS./Veh).");
                            else if (funcKey == 'Submit')
                                that.heroService._toastrErrorMsg("Error occured while submitting data. Please provide (14+2) digits for Approx. Savings (RS./Veh).");
                        }
                        else {
                            if (funcKey == 'Save')
                                that.heroService._toastrErrorMsg("Error occured while saving data. Please contact administrator.");
                            else if (funcKey == 'Submit')
                                that.heroService._toastrErrorMsg("Error occured while submitting data. Please contact administrator.");
                        }
                    }
                    else {
                        var a = s['SOAP:Body'][0].UpdateIgpIdeaDetailsResponse;
                        var b = a[0].tuple;
                        if (a == undefined || b == undefined) {
                            that._toastrMsg("Error occured while saving data. Please contact administrator.");
                        }
                        else {
                            console.log("check saved data: " + b);
                            if (b[0].new[0].IGP_IDEA_DETAILS[0].IID_REQUEST_ID[0] != undefined) {
                                that.allData.requestNumber = b[0].new[0].IGP_IDEA_DETAILS[0].IID_REQUEST_ID[0];
                                that._insertInPartsMaster(that.allData.requestNumber, funcKey);
                            }
                        }
                    }
                }
                else {
                    console.log("error found in err tag: ", err);
                }
            });
        });
    }
    _save() {
        let that = this;
        // if (that.allData.ideaTitle == '' || that.allData.ideaTitle == undefined) {
        //   that._toastrMsg("Please Enter IDEA Title field to proceed further.");
        // } else 
        if (that.allData.ideaDesc == '' || that.allData.ideaDesc == undefined) {
            that.heroService._toastrErrorMsg("Please Enter IDEA Description field to proceed further.");
        }
        else if (that.allData.sector == '' || that.allData.sector == undefined) {
            that.heroService._toastrErrorMsg("Please Select IDEA Sector field to proceed further.");
        }
        else if (that.allData.vehiclePlt == '' || that.allData.vehiclePlt == undefined) {
            that.heroService._toastrErrorMsg("Please Select Vehicle Platform field to proceed further.");
        }
        else if (that.allData.mobileNumber == null || that.allData.mobileNumber == undefined) {
            that.heroService._toastrErrorMsg("Please Enter Mobile Number to proceed further.");
        }
        else if (String(that.allData.mobileNumber).length < 10) {
            that.heroService._toastrErrorMsg("Please Enter Valid Mobile Number to proceed further.");
        }
        else {
            let dataObj = {};
            if (that.allData.requestNumber != "" && that.allData.requestNumber != undefined) {
                dataObj = {
                    tuple: {
                        old: {
                            IGP_IDEA_DETAILS: {
                                IID_REQUEST_ID: that.allData.requestNumber
                            }
                        },
                        new: {
                            IGP_IDEA_DETAILS: {
                                IID_IDEA_DESCRIPTION: (that.allData.ideaDesc ? that.allData.ideaDesc : ""),
                                IID_APP_SAVINGS: (that.allData.approxSavings ? that.allData.approxSavings : ""),
                                IID_IDEA_RESOURCE: (that.allData.ideaResource ? that.allData.ideaResource : ""),
                                IID_APP_WT_REDUCTION: (that.allData.approxWeight ? that.allData.approxWeight : ""),
                                IID_IDEA_CAT: (that.allData.idaCat ? that.allData.idaCat : ""),
                                IID_VEHICLE_PLAT: (that.allData.vehiclePlt ? that.allData.vehiclePlt : ""),
                                IID_MOBILE_NUMBER: (that.allData.mobileNumber ? that.allData.mobileNumber : 0),
                                IID_APP_SYSTEM: (that.allData.appSys ? that.allData.appSys : ""),
                                IID_IDEA_GEN_TOKEN_NO: that.allData.userDetails.UM_USER_ID[0],
                                IID_IDEA_GEN_NAME: that.allData.userDetails.UM_USER_NAME[0],
                                IID_IDEA_GEN_EMAIL: that.allData.userDetails.UM_USER_EMAIL[0],
                                IID_IDEA_DATE: moment__WEBPACK_IMPORTED_MODULE_3__(new Date()).format(),
                                IID_STATUS: "Saved",
                                IID_IS_ACTIVE: "A",
                                // IID_IDEA_TITLE: that.allData.ideaTitle,
                                IID_SECTOR: (that.allData.sector ? that.allData.sector : ""),
                                IID_IDEA_CAT_OTH: (that.allData.otherCat ? that.allData.otherCat : "")
                            }
                        }
                    }
                };
            }
            else {
                dataObj = {
                    tuple: {
                        new: {
                            IGP_IDEA_DETAILS: {
                                IID_IDEA_DESCRIPTION: (that.allData.ideaDesc ? that.allData.ideaDesc : ""),
                                IID_APP_SAVINGS: (that.allData.approxSavings ? that.allData.approxSavings : ""),
                                IID_IDEA_RESOURCE: (that.allData.ideaResource ? that.allData.ideaResource : ""),
                                IID_APP_WT_REDUCTION: (that.allData.approxWeight ? that.allData.approxWeight : ""),
                                IID_IDEA_CAT: (that.allData.idaCat ? that.allData.idaCat : ""),
                                IID_VEHICLE_PLAT: (that.allData.vehiclePlt ? that.allData.vehiclePlt : ""),
                                IID_MOBILE_NUMBER: (that.allData.mobileNumber ? that.allData.mobileNumber : 0),
                                IID_APP_SYSTEM: (that.allData.appSys ? that.allData.appSys : ""),
                                IID_IDEA_GEN_TOKEN_NO: that.allData.userDetails.UM_USER_ID[0],
                                IID_IDEA_GEN_NAME: that.allData.userDetails.UM_USER_NAME[0],
                                IID_IDEA_GEN_EMAIL: that.allData.userDetails.UM_USER_EMAIL[0],
                                IID_IDEA_DATE: moment__WEBPACK_IMPORTED_MODULE_3__(new Date()).format(),
                                IID_STATUS: "Saved",
                                IID_IS_ACTIVE: "A",
                                // IID_IDEA_TITLE: that.allData.ideaTitle,
                                IID_SECTOR: (that.allData.sector ? that.allData.sector : ""),
                                IID_IDEA_CAT_OTH: (that.allData.otherCat ? that.allData.otherCat : "")
                            }
                        }
                    }
                };
            }
            var parameters = [];
            that.callSubCodeNew(parameters, dataObj, 'Save');
        }
    }
    _submit() {
        let that = this;
        // if (that.allData.ideaTitle == '' || that.allData.ideaTitle == undefined) {
        //   that._toastrMsg("Please Enter IDEA Title field to proceed further.");
        // } else 
        if (that.allData.ideaDesc == '' || that.allData.ideaDesc == undefined) {
            that.heroService._toastrErrorMsg("Please Enter IDEA Description field to proceed further.");
        }
        else if (that.allData.sector == '' || that.allData.sector == undefined) {
            that.heroService._toastrErrorMsg("Please Select IDEA Sector field to proceed further.");
        }
        else if (that.allData.vehiclePlt == '' || that.allData.vehiclePlt == undefined) {
            that.heroService._toastrErrorMsg("Please Select Vehicle Platform field to proceed further.");
        }
        else if (String(that.allData.mobileNumber).length < 10) {
            that.heroService._toastrErrorMsg("Please Enter Valid Mobile Number to proceed further.");
        }
        else {
            let dataObj = {};
            if (that.allData.requestNumber != "" && that.allData.requestNumber != undefined) {
                dataObj = {
                    tuple: {
                        old: {
                            IGP_IDEA_DETAILS: {
                                IID_REQUEST_ID: that.allData.requestNumber
                            }
                        },
                        new: {
                            IGP_IDEA_DETAILS: {
                                IID_IDEA_DESCRIPTION: that.allData.ideaDesc,
                                IID_APP_SAVINGS: (that.allData.approxSavings ? that.allData.approxSavings : ""),
                                IID_IDEA_RESOURCE: (that.allData.ideaResource ? that.allData.ideaResource : ""),
                                IID_APP_WT_REDUCTION: (that.allData.approxWeight ? that.allData.approxWeight : ""),
                                IID_IDEA_CAT: (that.allData.idaCat ? that.allData.idaCat : ""),
                                IID_VEHICLE_PLAT: (that.allData.vehiclePlt ? that.allData.vehiclePlt : ""),
                                IID_MOBILE_NUMBER: (that.allData.mobileNumber ? that.allData.mobileNumber : 0),
                                IID_APP_SYSTEM: (that.allData.appSys ? that.allData.appSys : ""),
                                IID_IDEA_GEN_TOKEN_NO: (that.allData.userDetails.UM_USER_ID ? that.allData.userDetails.UM_USER_ID[0] : that.allData.userDetails.SM_VENDOR_CODE[0]),
                                IID_IDEA_GEN_NAME: (that.allData.userDetails.UM_USER_NAME ? that.allData.userDetails.UM_USER_NAME[0] : that.allData.userDetails.SM_SUPPLIER_NAME[0]),
                                IID_IDEA_GEN_EMAIL: (that.allData.userDetails.UM_USER_EMAIL ? that.allData.userDetails.UM_USER_EMAIL[0] : that.allData.userDetails.SM_SUPPLIER_EMAIL[0]),
                                IID_IDEA_DATE: moment__WEBPACK_IMPORTED_MODULE_3__(new Date()).format(),
                                IID_STATUS: "Submitted",
                                IID_IS_ACTIVE: "A",
                                // IID_IDEA_TITLE: that.allData.ideaTitle,
                                IID_SECTOR: (that.allData.sector ? that.allData.sector : ""),
                                IID_IDEA_CAT_OTH: (that.allData.otherCat ? that.allData.otherCat : "")
                            }
                        }
                    }
                };
            }
            else {
                dataObj = {
                    tuple: {
                        new: {
                            IGP_IDEA_DETAILS: {
                                IID_IDEA_DESCRIPTION: that.allData.ideaDesc,
                                IID_APP_SAVINGS: (that.allData.approxSavings ? that.allData.approxSavings : ""),
                                IID_IDEA_RESOURCE: (that.allData.ideaResource ? that.allData.ideaResource : ""),
                                IID_APP_WT_REDUCTION: (that.allData.approxWeight ? that.allData.approxWeight : ""),
                                IID_IDEA_CAT: (that.allData.idaCat ? that.allData.idaCat : ""),
                                IID_VEHICLE_PLAT: (that.allData.vehiclePlt ? that.allData.vehiclePlt : ""),
                                IID_MOBILE_NUMBER: (that.allData.mobileNumber ? that.allData.mobileNumber : 0),
                                IID_APP_SYSTEM: (that.allData.appSys ? that.allData.appSys : ""),
                                IID_IDEA_GEN_TOKEN_NO: (that.allData.userDetails.UM_USER_ID ? that.allData.userDetails.UM_USER_ID[0] : that.allData.userDetails.SM_VENDOR_CODE[0]),
                                IID_IDEA_GEN_NAME: (that.allData.userDetails.UM_USER_NAME ? that.allData.userDetails.UM_USER_NAME[0] : that.allData.userDetails.SM_SUPPLIER_NAME[0]),
                                IID_IDEA_GEN_EMAIL: (that.allData.userDetails.UM_USER_EMAIL ? that.allData.userDetails.UM_USER_EMAIL[0] : that.allData.userDetails.SM_SUPPLIER_EMAIL[0]),
                                IID_IDEA_DATE: moment__WEBPACK_IMPORTED_MODULE_3__(new Date()).format(),
                                IID_STATUS: "Submitted",
                                IID_IS_ACTIVE: "A",
                                // IID_IDEA_TITLE: that.allData.ideaTitle,
                                IID_SECTOR: (that.allData.sector ? that.allData.sector : ""),
                                IID_IDEA_CAT_OTH: (that.allData.otherCat ? that.allData.otherCat : "")
                            }
                        }
                    }
                };
            }
            var parameters = [];
            that.callSubCodeNew(parameters, dataObj, "Submit");
        }
    }
    _insertInPartsMaster(obj, funcKey) {
        let that = this;
        if (that.allData.alreadySavedPartsCount > 0) {
            for (let i = 0; i < that.allData.partsAddArray.length; i++) {
                for (let j = 0; j < that.allData.alreadySavedParts.length; j++) {
                    if (that.allData.partsAddArray[i].part_name == that.allData.alreadySavedParts[j].PRT_PART_DESC[0]) {
                        that.allData.partsAddArray[i].prm_key = that.allData.alreadySavedParts[j].PRT_SEQ_NO[0];
                        break;
                    }
                }
            }
        }
        //////////// insert data in part master table
        let dataObj3 = [];
        let temp = that.allData.partsAddArray.filter((d) => {
            return d.part_name != "";
        });
        if (temp.length > 0) {
            for (let i = 0; i < temp.length; i++) {
                if (temp[i].prm_key == undefined) {
                    dataObj3.push({
                        new: {
                            IGP_PART_DETAILS: {
                                IID_REQUEST_ID: obj,
                                PRT_PART_DESC: temp[i].part_name,
                                PRT_PART_TYPE: temp[i].part_type
                            }
                        }
                    });
                }
                else {
                    dataObj3.push({
                        old: {
                            IGP_PART_DETAILS: {
                                PRT_SEQ_NO: String(temp[i].prm_key)
                            }
                        },
                        new: {
                            IGP_PART_DETAILS: {
                                PRT_SEQ_NO: String(temp[i].prm_key),
                                IID_REQUEST_ID: obj,
                                PRT_PART_DESC: temp[i].part_name,
                                PRT_PART_TYPE: temp[i].part_type
                            }
                        }
                    });
                }
            }
        }
        if (that.allData.alreadyDelPartsCount > 0) {
            for (let i = 0; i < that.allData.partsModArray.length; i++) {
                for (let j = 0; j < that.allData.alreadyDelParts.length; j++) {
                    if (that.allData.partsModArray[i].part_name == that.allData.alreadyDelParts[j].PRT_PART_DESC[0]) {
                        //   tempPartsArray1.push(that.allData.partsModArray[i]);
                        that.allData.partsModArray[i].prm_key = that.allData.alreadyDelParts[j].PRT_SEQ_NO[0];
                        break;
                    }
                }
            }
        }
        let temp1 = that.allData.partsModArray.filter((d) => {
            return d.part_name != "";
        });
        if (temp1.length > 0) {
            for (let i = 0; i < temp1.length; i++) {
                if (temp1[i].prm_key == undefined) {
                    dataObj3.push({
                        new: {
                            IGP_PART_DETAILS: {
                                IID_REQUEST_ID: obj,
                                PRT_PART_DESC: temp1[i].part_name,
                                PRT_PART_TYPE: temp1[i].part_type
                            }
                        }
                    });
                }
                else {
                    dataObj3.push({
                        old: {
                            IGP_PART_DETAILS: {
                                PRT_SEQ_NO: temp1[i].prm_key
                            }
                        },
                        new: {
                            IGP_PART_DETAILS: {
                                PRT_SEQ_NO: temp1[i].prm_key,
                                IID_REQUEST_ID: obj,
                                PRT_PART_DESC: temp1[i].part_name,
                                PRT_PART_TYPE: temp1[i].part_type
                            }
                        }
                    });
                }
            }
        }
        if (dataObj3.length > 0) {
            var parameters = [];
            that.callSubCodeBasic(parameters, dataObj3, "UpdateIgpPartDetails", 'http://schemas.cordys.com/igp', 'insertParts', funcKey);
        }
        else {
            that._insertInDocs(obj, funcKey);
        }
    }
    _insertInModelApp(obj, funcKey) {
        let that = this;
        if (that.allData.alreadySavedModelCount > 0) {
            for (let i = 0; i < that.allData.modelAppArray.length; i++) {
                for (let j = 0; j < that.allData.alreadySavedModel.length; j++) {
                    if (that.allData.modelAppArray[i].model_name == that.allData.alreadySavedModel[j].IMA_MODEL_APPLICABILITY[0]) {
                        that.allData.modelAppArray[i].prm_key = that.allData.alreadySavedModel[j].IMA_SEQ_NO[0];
                        break;
                    }
                }
            }
        }
        let dataObj3 = [];
        let temp = that.allData.modelAppArray.filter((d) => {
            return d.model_name != "";
        });
        if (temp.length > 0) {
            for (let i = 0; i < temp.length; i++) {
                if (temp[i].prm_key == undefined) {
                    dataObj3.push({
                        new: {
                            IGP_MODEL_APP: {
                                IID_REQUEST_ID: obj,
                                IMA_MODEL_APPLICABILITY: temp[i].model_name
                            }
                        }
                    });
                }
                else {
                    dataObj3.push({
                        old: {
                            IGP_MODEL_APP: {
                                IMA_SEQ_NO: temp[i].prm_key
                            }
                        },
                        new: {
                            IGP_MODEL_APP: {
                                IMA_SEQ_NO: temp[i].prm_key,
                                IID_REQUEST_ID: obj,
                                IMA_MODEL_APPLICABILITY: temp[i].model_name
                            }
                        }
                    });
                }
            }
        }
        if (dataObj3.length > 0) {
            var parameters = [];
            that.callSubCodeBasic(parameters, dataObj3, "UpdateIgpModelApp", 'http://schemas.cordys.com/igp', 'insertModels', funcKey);
        }
        else {
            // that._insertInDocs(obj, funcKey);
            that._insertInfields(obj, funcKey);
        }
    }
    _insertInDocs(obj, funcKey) {
        let that = this;
        //////////// Insert data in docs table
        let temp11;
        let temp_file_name = "", temp_file_path = "";
        if (that.allData.alreadySavedDocsCount > 0) {
            let tempArray = [];
            for (let i = 0; i < that.allData.filesArray.length; i++) {
                if (that.allData.filesArray[i].file_path.split(":")[1] == undefined) {
                    tempArray.push(that.allData.filesArray[i]);
                }
            }
            temp11 = tempArray.filter((d) => {
                return d.file_name != "";
            });
            if (temp11.length > 0) {
                for (let i = 0; i < temp11.length; i++) {
                    temp_file_name += temp11[i].file_name + "###";
                    temp_file_path += temp11[i].file_path + "###";
                }
                var parameters = [];
                let dataObj = {
                    FileName: temp_file_name,
                    FileContent: temp_file_path,
                    requestID: obj
                };
                that.callSubCodeBasic(parameters, dataObj, "UploadDocument", 'http://schemas.cordys.com/igp', 'insertDocs', funcKey);
            }
            else {
                that._insertInModelApp(obj, funcKey);
            }
        }
        else {
            temp11 = that.allData.filesArray.filter((d) => {
                return d.file_name != "";
            });
            if (temp11.length > 0) {
                for (let i = 0; i < temp11.length; i++) {
                    temp_file_name += temp11[i].file_name + "###";
                    temp_file_path += temp11[i].file_path + "###";
                }
                var parameters = [];
                let dataObj = {
                    FileName: temp_file_name,
                    FileContent: temp_file_path,
                    requestID: obj
                };
                that.callSubCodeBasic(parameters, dataObj, "UploadDocument", 'http://schemas.cordys.com/igp', 'insertDocs', funcKey);
            }
            else {
                that._insertInModelApp(obj, funcKey);
            }
        }
    }
    _insertInfields(obj, funcKey) {
        let that = this;
        /////// Insert data in fields config table
        debugger;
        if (that.allData.alreadySavedFieldsCount > 0) {
            for (let i = 0; i < that.allData.fieldsArray.length; i++) {
                for (let j = 0; j < that.allData.alreadySavedFields.length; j++) {
                    if (that.allData.fieldsArray[i].field_id == that.allData.alreadySavedFields[j].ICF_CONFIG_FIELD[0]) {
                        that.allData.fieldsArray[i].prm_key = that.allData.alreadySavedFields[j].ICF_SEQ_NO[0];
                        break;
                    }
                }
            }
        }
        let jss = that.allData.fieldsArray.filter((d) => {
            return (d.field_id != "" && d.value != null);
        });
        if (jss.length > 0) {
            let somedata = [];
            for (let i = 0; i < jss.length; i++) {
                if (jss[i].prm_key == undefined) {
                    somedata.push({
                        new: {
                            IGP_CONFIG_FIELD: {
                                IID_REQUEST_ID: obj,
                                ICF_CONFIG_FIELD: jss[i].field_id,
                                ICF_CONFIG_VALUE: jss[i].value
                            }
                        }
                    });
                }
                else {
                    somedata.push({
                        old: {
                            IGP_CONFIG_FIELD: {
                                ICF_SEQ_NO: jss[i].prm_key
                            }
                        },
                        new: {
                            IGP_CONFIG_FIELD: {
                                ICF_SEQ_NO: jss[i].prm_key,
                                IID_REQUEST_ID: obj,
                                ICF_CONFIG_FIELD: jss[i].field_id,
                                ICF_CONFIG_VALUE: jss[i].value
                            }
                        }
                    });
                }
            }
            if (somedata.length > 0) {
                var parameters = [];
                that.callSubCodeBasic(parameters, somedata, "UpdateIgpConfigField", 'http://schemas.cordys.com/igp', 'inserFields', funcKey);
            }
        }
        else {
            if (funcKey == 'Submit') {
                that._toastrMsg("Data Submitted Successfully.");
                that._sendMail(obj);
            }
            else if (funcKey == 'Save') {
                that._getPartsData(that.allData.requestNumber);
                that._toastrMsg("Data Saved Successfully.");
            }
        }
    }
    _sendMail(obj) {
        let that = this;
        var parameters = [];
        let dataObj = {
            UIDNumber: obj
        };
        that.callSubCodeBasic(parameters, dataObj, "IGP_EmaiToSectorHOD", 'http://schemas.cordys.com/default', 'sendMail', 'Submit');
    }
    _onFieldChange(field, index, item) {
        console.log("changed field: ", field);
        let that = this;
        debugger;
        let count = 0;
        if (field == "") {
            if (item.prm_key != undefined) {
                let dataObj = {
                    tuple: {
                        old: {
                            IGP_CONFIG_FIELD: {
                                ICF_SEQ_NO: item.prm_key
                            }
                        }
                    }
                };
                // delete field
                var parameters = [];
                that.fieldsSubCode(parameters, "UpdateIgpConfigField", dataObj, "http://schemas.cordys.com/igp", '_deleteSingleField', field, index, item);
            }
            else {
                item.field_id = "";
                item.field_name = "";
                item.value = null;
            }
        }
        else {
            var valueArr = that.allData.fieldsArray.map(function (item1) { return item1.field_id; }).filter((d) => { return d != ""; });
            var isDuplicate = valueArr.some(function (item, idx) {
                return valueArr.indexOf(item) != idx;
            });
            console.log("isDuplicate: ", isDuplicate);
            if (isDuplicate == true) {
                if (that.allData.fieldsArray[index].prm_key != undefined) {
                    that._toastrErrorMsg("This field is already selected.");
                    let temp = that.allData.configValues.filter((d) => {
                        return d.LMF_DESC == item.field_name;
                    });
                    if (temp.length > 0) {
                        console.log("temp: ", temp[0]);
                        let elementId = 'row4' + index;
                        document.getElementById(elementId)["value"] = temp[0].LMF_ID;
                        that.allData.fieldsArray[index].field_id = temp[0].LMF_ID;
                    }
                    count = -1;
                }
                else {
                    that._toastrErrorMsg("This field is already selected.");
                    item.field_id = "";
                    item.field_name = "";
                    item.value = null;
                    let elementId = 'row4' + index;
                    // var e =document.getElementById(elementId)["value"];
                    document.getElementById(elementId)["value"] = "";
                }
            }
            // }
            for (let j = 0; j < that.allData.fieldsArray.length; j++) {
                /////// new code /////
                if (count != -1) {
                    if (index == j) {
                        if (that.allData.fieldsArray[j].field_name != "") {
                            if (that.allData.fieldsArray[j].prm_key != undefined) {
                                let dataObj = {
                                    tuple: {
                                        old: {
                                            IGP_CONFIG_FIELD: {
                                                ICF_SEQ_NO: that.allData.fieldsArray[j].prm_key
                                            }
                                        },
                                        new: {
                                            IGP_CONFIG_FIELD: {
                                                IID_REQUEST_ID: that.allData.reqNum,
                                                ICF_CONFIG_FIELD: that.allData.fieldsArray[j].field_id,
                                                ICF_CONFIG_VALUE: that.allData.fieldsArray[j].value
                                            }
                                        }
                                    }
                                };
                                // delete and update
                                var parameters = [];
                                that.fieldsSubCode(parameters, "UpdateIgpConfigField", dataObj, "http://schemas.cordys.com/igp", '_updateDeleteFields', field, index, item);
                                // that.heroService.ajax("UpdateIgpConfigField", "http://schemas.cordys.com/igp", {
                                //   tuple: dataObj.tuple
                                // }).then((respOnce) => {
                                //   console.log("check config response: ", respOnce);
                                //   that._getConfigData1(that.allData.reqNum);
                                // },
                                //   (err) => {
                                //     that.toastr.error("Error occured while saving config data. Please contact administrator.");
                                //   });
                            }
                        }
                    }
                }
            }
        }
        console.log("updted configValues ", that.allData.configValues);
    }
    _getConfigData() {
        let that = this;
        var parameters = [];
        let dataObj = {};
        that.callSubCodeBasic(parameters, dataObj, "GetIdeaConfig", "http://schemas.cordys.com/igp", '_getIdeaConfigData', '');
    }
    _getConfigData1(reqId) {
        let that = this;
        var parameters = [];
        let dataObj = {};
        that.callSubCodeBasic(parameters, dataObj, "GetIdeaConfig", "http://schemas.cordys.com/igp", '_getIdeaConfig1Data', '');
    }
    _getSavedFieldValues(allDd, reqId) {
        let that = this;
        var parameters = [];
        let dataObj = {
            requestID: reqId
        };
        that.callSubCodeBasic(parameters, dataObj, "GetConfigOnUID", "http://schemas.cordys.com/igp", '_getSavedFieldData', '');
    }
    _getSavedDocs(reqId) {
        let that = this;
        var parameters = [];
        let dataObj = {
            UID: reqId
        };
        that.callSubCodeBasic(parameters, dataObj, "GetIdeaDocOnUID", "http://schemas.cordys.com/igp", '_getIdeaDocOnUID', '');
    }
    _getPartsData(reqId) {
        let that = this;
        var parameters = [];
        let dataObj = {
            UID: reqId
        };
        that.callSubCodeBasic(parameters, dataObj, "GetAddedParts", "http://schemas.cordys.com/igp", '_getAddedParts', '');
    }
    _getSavedModelApp(reqId) {
        let that = this;
        let params = {
            UID: reqId
        };
        var parameters = [];
        that.callSubCodeBasic(parameters, params, 'GetIGPModelOnUID', "http://schemas.cordys.com/igp", '_getSavedModels', '');
    }
    static PartsParam(data) {
        let dataObj = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].old != undefined) {
                dataObj.push({
                    "old": {
                        "IGP_PART_DETAILS": {
                            "PRT_SEQ_NO": data[i].old.IGP_PART_DETAILS.PRT_SEQ_NO
                        }
                    },
                    "new": {
                        "IGP_PART_DETAILS": {
                            "PRT_SEQ_NO": data[i].new.IGP_PART_DETAILS.PRT_SEQ_NO,
                            "IID_REQUEST_ID": data[i].new.IGP_PART_DETAILS.IID_REQUEST_ID,
                            "PRT_PART_DESC": data[i].new.IGP_PART_DETAILS.PRT_PART_DESC,
                            "PRT_PART_TYPE": data[i].new.IGP_PART_DETAILS.PRT_PART_TYPE
                        }
                    }
                });
            }
            else {
                dataObj.push({
                    "new": {
                        "IGP_PART_DETAILS": {
                            "IID_REQUEST_ID": data[i].new.IGP_PART_DETAILS.IID_REQUEST_ID,
                            "PRT_PART_DESC": data[i].new.IGP_PART_DETAILS.PRT_PART_DESC,
                            "PRT_PART_TYPE": data[i].new.IGP_PART_DETAILS.PRT_PART_TYPE
                        }
                    }
                });
            }
        }
        return dataObj;
    }
    static DeleteFileParams(data) {
        let dataObj = [];
        dataObj.push({
            // "tuple": {
            "old": {
                "IGP_DOCS": {
                    "IGD_SNO": data.tuple.old.IGP_DOCS.IGD_SNO
                }
            }
            // }
        });
        return dataObj;
    }
    static DeletePartsParam(data) {
        let dataObj = [];
        dataObj.push({
            // "tuple": {
            "old": {
                "IGP_PART_DETAILS": {
                    "PRT_SEQ_NO": data.tuple.old.IGP_PART_DETAILS.PRT_SEQ_NO
                }
            }
            // }
        });
        return dataObj;
    }
    static DeleteModelsParam(data) {
        let dataObj = [];
        dataObj.push({
            // "tuple": {
            "old": {
                "IGP_MODEL_APP": {
                    "IMA_SEQ_NO": data.tuple.old.IGP_MODEL_APP.IMA_SEQ_NO
                }
            }
            // }
        });
        return dataObj;
    }
    static modelParams(data) {
        let dataObj = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].old != undefined) {
                dataObj.push({
                    "old": {
                        "IGP_MODEL_APP": {
                            "IMA_SEQ_NO": data[i].old.IGP_MODEL_APP.IMA_SEQ_NO
                        }
                    },
                    "new": {
                        "IGP_MODEL_APP": {
                            "IMA_SEQ_NO": data[i].new.IGP_MODEL_APP.IMA_SEQ_NO,
                            "IID_REQUEST_ID": data[i].new.IGP_MODEL_APP.IID_REQUEST_ID,
                            "IMA_MODEL_APPLICABILITY": data[i].new.IGP_MODEL_APP.IMA_MODEL_APPLICABILITY
                        }
                    }
                });
            }
            else {
                dataObj.push({
                    "new": {
                        "IGP_MODEL_APP": {
                            "IID_REQUEST_ID": data[i].new.IGP_MODEL_APP.IID_REQUEST_ID,
                            "IMA_MODEL_APPLICABILITY": data[i].new.IGP_MODEL_APP.IMA_MODEL_APPLICABILITY
                        }
                    }
                });
            }
        }
        ;
        let temp;
        temp = {
            dataObj
        };
        return dataObj;
    }
    static DocsParam(data) {
        var parameters = [];
        parameters["FileName"] = data.FileName;
        parameters["FileContent"] = data.FileContent;
        parameters["requestID"] = data.requestID;
        return parameters;
    }
    static singleFieldDeleteParam(data) {
        let dataObj = [];
        dataObj.push({
            // "tuple": {
            "old": {
                "IGP_CONFIG_FIELD": {
                    "ICF_SEQ_NO": data[0].tuple.old.IGP_CONFIG_FIELD.ICF_SEQ_NO
                }
            }
            // }
        });
        // let temp: any;
        // temp = {
        //   dataObj
        // }
        return dataObj;
    }
    static _updateDeleteParams(data) {
        let dataObj = [];
        dataObj.push({
            // "tuple": {
            "old": {
                "IGP_CONFIG_FIELD": {
                    "ICF_SEQ_NO": data[0].tuple.old.IGP_CONFIG_FIELD.ICF_SEQ_NO
                }
            },
            "new": {
                "IGP_CONFIG_FIELD": {
                    "IID_REQUEST_ID": data[0].tuple.old.IGP_CONFIG_FIELD.IID_REQUEST_ID,
                    "ICF_CONFIG_FIELD": data[0].tuple.old.IGP_CONFIG_FIELD.ICF_CONFIG_FIELD,
                    "ICF_CONFIG_VALUE": data[0].tuple.old.IGP_CONFIG_FIELD.ICF_CONFIG_VALUE
                }
            }
            // }
        });
        // let temp: any;
        // temp = {
        //   dataObj
        // }
        return dataObj;
    }
    static requestIDparam(data) {
        var parameters = [];
        parameters["requestID"] = data.requestID;
        return parameters;
    }
    static fieldsDataparam(data) {
        let dataObj = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].old != undefined) {
                dataObj.push({
                    "old": {
                        "IGP_CONFIG_FIELD": {
                            "ICF_SEQ_NO": data[i].old.IGP_CONFIG_FIELD.ICF_SEQ_NO
                        }
                    },
                    "new": {
                        "IGP_CONFIG_FIELD": {
                            "ICF_SEQ_NO": data[i].new.IGP_CONFIG_FIELD.ICF_SEQ_NO,
                            "IID_REQUEST_ID": data[i].new.IGP_CONFIG_FIELD.IID_REQUEST_ID,
                            "ICF_CONFIG_FIELD": data[i].new.IGP_CONFIG_FIELD.ICF_CONFIG_FIELD,
                            "ICF_CONFIG_VALUE": data[i].new.IGP_CONFIG_FIELD.ICF_CONFIG_VALUE
                        }
                    }
                });
            }
            else {
                dataObj.push({
                    "new": {
                        "IGP_CONFIG_FIELD": {
                            "IID_REQUEST_ID": data[i].new.IGP_CONFIG_FIELD.IID_REQUEST_ID,
                            "ICF_CONFIG_FIELD": data[i].new.IGP_CONFIG_FIELD.ICF_CONFIG_FIELD,
                            "ICF_CONFIG_VALUE": data[i].new.IGP_CONFIG_FIELD.ICF_CONFIG_VALUE
                        }
                    }
                });
            }
        }
        return dataObj;
    }
    callSubCodeBasic(parameters, params, method, namespace, key, funcKey) {
        let that = this;
        if (key == 'insertParts') {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.PartsParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == 'sendMail') {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.UIDParam(params.UIDNumber, 'sendMail');
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "insertDocs") {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.DocsParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getAddedParts") {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.UIDParam(params.UID, '_getAddedParts');
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getDeletedParts") {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.UIDParam(params.UID, '_getDeletedParts');
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getIdeaDocOnUID") {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.UIDParam(params.UID, '_getIdeaDocOnUID');
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "deleteFiles") {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.DeleteFileParams(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "insertModels") {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.modelParams(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getSavedModels") {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.UIDParam(params.UID, '_getSavedModels');
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getIdeaConfigData") {
            debugger;
            parameters[method + " xmlns='" + namespace + "'"] = {};
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getIdeaConfig1Data") {
            debugger;
            parameters[method + " xmlns='" + namespace + "'"] = {};
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getSavedFieldData") {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.requestIDparam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "inserFields") {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.fieldsDataparam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "deleteParts") {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.DeletePartsParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "deleteModels") {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.DeleteModelsParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        that.loadingController.create({
            spinner: 'bubbles',
            message: 'Please wait...'
        }).then((loadEl) => {
            loadEl.present();
            that.heroService.testService1(parameters, function (err, response) {
                loadEl.dismiss();
                if (response) {
                    var s = response['SOAP:Envelope'];
                    var a;
                    var b;
                    if (key == 'sendMail') {
                        a = s['SOAP:Body'][0].IGP_EmaiToSectorHODResponse;
                        if (a != undefined) {
                            that._toastrMsg("E-mail Sent Successfully.");
                            that.allData.requestNumber = undefined;
                            // that.allData.ideaTitle = undefined;
                            that.allData.ideaDesc = undefined;
                            that.allData.approxSavings = undefined;
                            that.allData.approxWeight = undefined;
                            that.allData.ideaResource = undefined;
                            that.allData.idaCat = null;
                            that.allData.otherCat = undefined;
                            that.allData.sector = null;
                            that.allData.vehiclePlt = null;
                            that.allData.mobileNumber = undefined;
                            that.allData.appSys = undefined;
                            that.allData.filesArray = [];
                            // that.allData.partsAddArray = [];
                            // that.allData.partsModArray = [];
                            // that.allData.modelAppArray = [];
                            that.allData.fieldsArray = [
                                {
                                    index: 0,
                                    field_name: "",
                                    field_id: "",
                                    value: null
                                }
                            ];
                            that.allData.partsAddArray = [
                                {
                                    index: 0,
                                    part_name: "",
                                    part_type: "ADD"
                                }
                            ];
                            that.allData.partsModArray = [
                                {
                                    index: 0,
                                    part_name: "",
                                    part_type: "MOD"
                                }
                            ];
                            that.allData.modelAppArray = [
                                {
                                    index: 0,
                                    model_name: ""
                                }
                            ];
                            // that.allData.configValues = [];
                            // that.modPartName = undefined;
                            // that.addModelName = undefined;
                            // that.addPartName = undefined;
                            that.allData.vehPlatformsArray = [];
                            that.allData.systemArray = [];
                        }
                        else {
                            that.heroService._toastrErrorMsg("Error occured while sending e-mail. Please contact administrator.");
                        }
                    }
                    else {
                        if (key == 'insertParts') {
                            a = s['SOAP:Body'][0].UpdateIgpPartDetailsResponse;
                            if (a != undefined) {
                                // that._insertInModelApp(that.allData.requestNumber, funcKey);
                                that._insertInDocs(that.allData.requestNumber, funcKey);
                            }
                            else {
                                // that._insertInModelApp(that.allData.requestNumber, funcKey);
                                that._insertInDocs(that.allData.requestNumber, funcKey);
                                that._toastrErrorMsg("Error occured while saving parts. Please contact administrator.");
                            }
                        }
                        else {
                            if (key == "insertDocs") {
                                a = s['SOAP:Body'][0].UploadDocumentResponse;
                                if (a != undefined) {
                                    that._insertInModelApp(that.allData.requestNumber, funcKey);
                                }
                                else {
                                    that._insertInModelApp(that.allData.requestNumber, funcKey);
                                    that._toastrMsg("Error occured while saving files. Please contact administrator.");
                                }
                            }
                            else if (key == "_getAddedParts") {
                                a = s['SOAP:Body'][0].GetAddedPartsResponse;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "IGP_PART_DETAILS");
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that._doFurtherParts(that.heroService.otoa(obj), 'ADD');
                                    }
                                }
                                var parameters = [];
                                let dataObj = {
                                    UID: that.allData.requestNumber
                                };
                                that.callSubCodeBasic(parameters, dataObj, "GetDeletedParts", "http://schemas.cordys.com/igp", '_getDeletedParts', '');
                            }
                            else if (key == "_getDeletedParts") {
                                a = s['SOAP:Body'][0].GetDeletedPartsResponse;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "IGP_PART_DETAILS");
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that._doFurtherParts(that.heroService.otoa(obj), 'MOD');
                                    }
                                }
                                that._getSavedModelApp(that.allData.requestNumber);
                            }
                            else if (key == "_getIdeaDocOnUID") {
                                a = s['SOAP:Body'][0].GetIdeaDocOnUIDResponse;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "IGP_DOCS");
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that._doFurtherParts(that.heroService.otoa(obj), 'DOCS');
                                    }
                                }
                            }
                            else if (key == "deleteFiles") {
                                a = s['SOAP:Body'][0].UpdateIgpDocsResponse;
                                if (a != undefined) {
                                    that._toastrMsg("File Deleted Successfully.");
                                    that._getSavedDocs(that.allData.requestNumber);
                                }
                            }
                            else if (key == "insertModels") {
                                // debugger
                                a = s['SOAP:Body'][0].UpdateIgpModelAppResponse;
                                if (a != undefined) {
                                    // that._insertInDocs(that.allData.requestNumber, funcKey);
                                    that._insertInfields(that.allData.requestNumber, funcKey);
                                }
                                else {
                                    that._insertInfields(that.allData.requestNumber, funcKey);
                                    that._toastrMsg("Error occured while saving model applicabilities. Please contact administrator.");
                                    // that._insertInDocs(that.allData.requestNumber, funcKey);
                                }
                            }
                            else if (key == "_getSavedModels") {
                                a = s['SOAP:Body'][0].GetIGPModelOnUIDResponse;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "IGP_MODEL_APP");
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that._doFurtherParts(that.heroService.otoa(obj), 'Model');
                                    }
                                }
                                that._getSavedDocs(that.allData.requestNumber);
                            }
                            else if (key == "_getIdeaConfigData") {
                                a = s['SOAP:Body'][0].GetIdeaConfigResponse;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "LOV_MASTER_AUTO");
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that.allData.configValues = that.heroService.otoa(obj);
                                    }
                                }
                                that._getSectorList();
                            }
                            else if (key == "_getIdeaConfig1Data") {
                                a = s['SOAP:Body'][0].GetIdeaConfigResponse;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "LOV_MASTER_AUTO");
                                    // if (obj != undefined)
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that.allDd = [];
                                        that.allDd = that.heroService.otoa(obj);
                                        that._getSavedFieldValues(that.allDd, that.allData.requestNumber);
                                    }
                                }
                            }
                            else if (key == "_getSavedFieldData") {
                                a = s['SOAP:Body'][0].GetConfigOnUIDResponse;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "IGP_CONFIG_FIELD");
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that._doFurtherParts(that.heroService.otoa(obj), 'Fields');
                                    }
                                }
                                that._getConfigData1(that.allData.requestNumber);
                            }
                            else if (key == "inserFields") {
                                debugger;
                                a = s['SOAP:Body'][0].UpdateIgpConfigFieldResponse;
                                if (a != undefined) {
                                    if (funcKey == 'Submit') {
                                        that._toastrMsg("Data Submitted Successfully.");
                                        that._sendMail(that.allData.requestNumber);
                                    }
                                    else if (funcKey == 'Save') {
                                        that._getPartsData(that.allData.requestNumber);
                                        that._toastrMsg("Data Saved Successfully.");
                                    }
                                }
                                else {
                                    if (funcKey == 'Submit') {
                                        that._toastrMsg("Data Submitted Successfully.");
                                        that._sendMail(that.allData.requestNumber);
                                    }
                                    else if (funcKey == 'Save') {
                                        that._getPartsData(that.allData.requestNumber);
                                        that._toastrMsg("Data Saved Successfully.");
                                    }
                                    that._toastrMsg("Error occured while saving fields. Please contact administrator.");
                                }
                            }
                            else if (key == "deleteParts") {
                                a = s['SOAP:Body'][0].UpdateIgpPartDetailsResponse;
                                if (a != undefined) {
                                    that._toastrMsg("Part Deleted Successfully.");
                                    that._getPartsData(that.allData.requestNumber);
                                }
                                else {
                                    that.heroService._toastrErrorMsg("Error occured while deleting part. Please contact administrator.");
                                }
                            }
                            else if (key == "deleteModels") {
                                a = s['SOAP:Body'][0].UpdateIgpModelAppResponse;
                                if (a != undefined) {
                                    that._toastrMsg("Model Deleted Successfully.");
                                    that._getSavedModelApp(that.allData.requestNumber);
                                }
                                else {
                                    that.heroService._toastrErrorMsg("Error occured while deleting model. Please contact administrator.");
                                }
                            }
                        }
                    }
                }
                else {
                    console.log("error found in err tag: ", err);
                    console.log("no response cought");
                    console.log("not getting response becz of err: ", err);
                }
            });
        });
    }
    fieldsSubCode(parameters, method, params, namespace, key, field, index, item) {
        let that = this;
        if (key == '_deleteSingleField') {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1.singleFieldDeleteParam(params);
            ;
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == '_updateDeleteFields') {
            parameters[method + " xmlns='" + namespace + "'"] = Tab1Page_1._updateDeleteParams(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        that.loadingController.create({
            spinner: 'bubbles',
            message: 'Please wait...'
        }).then((loadEl) => {
            loadEl.present();
            that.heroService.testService1(parameters, function (err, response) {
                loadEl.dismiss();
                debugger;
                if (response) {
                    let s = response['SOAP:Envelope'];
                    let faultString = s['SOAP:Body'][0]['SOAP:Fault'];
                    if (faultString != undefined) {
                        if (faultString[0].faultstring[0]._ == "Unable to bind the artifact to a SAML assertion.") {
                            that.heroService._toastrErrorMsg("Token is expired. Please logout and login to access the app.");
                            return;
                        }
                        console.log("fault string: ", faultString[0].faultstring[0]._);
                    }
                    if (key == '_deleteSingleField') {
                        let a = s['SOAP:Body'][0].UpdateIgpConfigFieldResponse;
                        if (a != undefined) {
                            item.field_id = "";
                            item.field_name = "";
                            item.value = null;
                            that._getConfigData1(that.allData.requestNumber);
                        }
                    }
                    else if (key == '_updateDeleteFields') {
                        let a = s['SOAP:Body'][0].UpdateIgpConfigFieldResponse;
                        if (a != undefined) {
                            that._getConfigData1(that.allData.requestNumber);
                        }
                    }
                }
                else {
                    console.log("error found in err tag: ", err);
                    console.log("no response cought");
                    console.log("not getting response becz of err: ", err);
                }
            });
        });
    }
    _doFurtherParts(obj, key) {
        let that = this;
        ///// for added parts
        if (key == "ADD") {
            that.allData.partsAddArray = [];
            that.allData.alreadySavedPartsCount = that.heroService.otoa(obj).length;
            that.allData.alreadySavedParts = that.heroService.otoa(obj);
            debugger;
            for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
                that.allData.partsAddArray.push({
                    part_name: that.heroService.otoa(obj)[j].PRT_PART_DESC[0],
                    part_type: that.heroService.otoa(obj)[j].PRT_PART_TYPE[0],
                    prm_key: that.heroService.otoa(obj)[j].PRT_SEQ_NO[0]
                });
            }
        }
        else if (key == "MOD") {
            that.allData.partsModArray = [];
            that.allData.alreadyDelPartsCount = that.heroService.otoa(obj).length;
            that.allData.alreadyDelParts = that.heroService.otoa(obj);
            for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
                that.allData.partsModArray.push({
                    part_name: that.heroService.otoa(obj)[j].PRT_PART_DESC[0],
                    part_type: that.heroService.otoa(obj)[j].PRT_PART_TYPE[0],
                    prm_key: that.heroService.otoa(obj)[j].PRT_SEQ_NO[0]
                });
            }
        }
        else if (key == "Model") {
            that.allData.modelAppArray = [];
            that.allData.alreadySavedModelCount = that.heroService.otoa(obj).length;
            that.allData.alreadySavedModel = that.heroService.otoa(obj);
            for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
                that.allData.modelAppArray.push({
                    model_name: that.heroService.otoa(obj)[j].IMA_MODEL_APPLICABILITY[0],
                    prm_key: that.heroService.otoa(obj)[j].IMA_SEQ_NO[0]
                });
            }
        }
        else if (key == "DOCS") {
            that.allData.filesArray = [];
            that.allData.alreadySavedDocsCount = that.heroService.otoa(obj).length;
            that.allData.alreadySavedDocs = that.heroService.otoa(obj);
            for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
                that.allData.filesArray.push({
                    file_name: that.heroService.otoa(obj)[j].IGD_DOC_NAME[0],
                    file_path: that.heroService.otoa(obj)[j].IGD_DOC_PATH[0],
                    prm_key: that.heroService.otoa(obj)[j].IGD_SNO[0]
                });
            }
        }
        else if (key == "Fields") {
            debugger;
            that.allData.fieldsArray = [];
            that.allData.alreadySavedFieldsCount = that.heroService.otoa(obj).length;
            that.allData.alreadySavedFields = that.heroService.otoa(obj);
            for (let i = 0; i < that.allDd.length; i++) {
                for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
                    if (that.allDd[i].LMF_ID[0] == that.heroService.otoa(obj)[j].ICF_CONFIG_FIELD[0]) {
                        that.allDd[i].selectedFlag = true;
                        that.allDd[i].ICF_CONFIG_VALUE = that.heroService.otoa(obj)[j].ICF_CONFIG_VALUE[0];
                        that.allDd[i].prm_key = that.heroService.otoa(obj)[j].ICF_SEQ_NO[0];
                    }
                }
                // for (let k = 0; k < that.allData.fieldsArray.length; k++) {
                //   if (that.allData.fieldsArray[k].field_id == "") {
                if (that.heroService.otoa(that.allDd)[i].selectedFlag == true) {
                    that.allData.fieldsArray.push({
                        field_name: that.heroService.otoa(that.allDd)[i].LMF_DESC[0],
                        field_id: that.heroService.otoa(that.allDd)[i].LMF_ID[0],
                        value: that.heroService.otoa(that.allDd)[i].ICF_CONFIG_VALUE,
                        prm_key: that.heroService.otoa(that.allDd)[i].prm_key
                    });
                    // that.allData.fieldsArray[k].field_name = that.heroService.otoa(that.allDd)[i].LMF_DESC;
                    // that.allData.fieldsArray[k].field_id = that.heroService.otoa(that.allDd)[i].LMF_ID;
                    // that.allData.fieldsArray[k].value = that.heroService.otoa(that.allDd)[i].ICF_CONFIG_VALUE;
                    // that.allData.fieldsArray[k].prm_key = that.heroService.otoa(that.allDd)[i].prm_key;
                    // break;
                }
                //   }
                // }
            }
            that.allData.configValues = that.allDd;
        }
    }
    _toastrMsg(msg) {
        this.heroService._toastrMsg(msg);
    }
    _toastrErrorMsg(msg) {
        this.heroService._toastrErrorMsg(msg);
    }
};
Tab1Page.ctorParameters = () => [
    { type: _services_hero_service__WEBPACK_IMPORTED_MODULE_2__.HeroService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.LoadingController }
];
Tab1Page.propDecorators = {
    rows: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChildren, args: ["row",] }],
    rows1: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChildren, args: ["row1",] }],
    rows3: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChildren, args: ["row3",] }]
};
Tab1Page = Tab1Page_1 = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-tab1',
        template: _raw_loader_tab1_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_tab1_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], Tab1Page);



/***/ }),

/***/ 99474:
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.page.scss ***!
  \*************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".form-group {\n  padding: 8px;\n  border: 1px solid #b2c0c6;\n  border-radius: 5px;\n  margin: 8px;\n  width: 100%;\n}\n\n.form-group > label {\n  position: absolute;\n  top: -1px;\n  left: 20px;\n  background-color: white;\n  padding-left: 5px;\n  padding-right: 5px;\n  color: #2c5364;\n}\n\n.form-group > input {\n  border: none;\n}\n\n.form-group > textarea {\n  border: none;\n}\n\n.bottmDiv {\n  position: fixed;\n  bottom: 0px;\n  width: 100%;\n  background: white;\n}\n\nion-item {\n  padding-bottom: 8px;\n}\n\nion-select {\n  max-width: 100%;\n  width: 100%;\n}\n\n.form-control[readonly] {\n  background-color: #ffffff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRhYjEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtBQUNKOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFFSjs7QUFDQTtFQUNJLFlBQUE7QUFFSjs7QUFBQTtFQUNJLFlBQUE7QUFHSjs7QUFLQTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0FBRko7O0FBS0E7RUFDSSxtQkFBQTtBQUZKOztBQUtBO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUFGSjs7QUFNQTtFQUNJLHlCQUFBO0FBSEoiLCJmaWxlIjoidGFiMS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9ybS1ncm91cCB7XG4gICAgcGFkZGluZzogOHB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNiMmMwYzY7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIG1hcmdpbjogOHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuLmZvcm0tZ3JvdXAgPiBsYWJlbCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogLTFweDtcbiAgICBsZWZ0OiAyMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIHBhZGRpbmctbGVmdDogNXB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICBjb2xvcjogIzJjNTM2NDtcbn1cblxuLmZvcm0tZ3JvdXAgPiBpbnB1dCB7XG4gICAgYm9yZGVyOiBub25lO1xufVxuLmZvcm0tZ3JvdXAgPiB0ZXh0YXJlYSB7XG4gICAgYm9yZGVyOiBub25lO1xufVxuLy8gLnRvcERpdiB7XG4vLyAgICAgcG9zaXRpb246IGZpeGVkO1xuLy8gICAgIHRvcDogMHB4O1xuLy8gICAgIHdpZHRoOiAxMDAlO1xuLy8gICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuLy8gfVxuLmJvdHRtRGl2IHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgYm90dG9tOiAwcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbmlvbi1pdGVtIHtcbiAgICBwYWRkaW5nLWJvdHRvbTogOHB4O1xufVxuXG5pb24tc2VsZWN0IHtcbiAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgLy8gbWFyZ2luLWxlZnQ6IDMzJTtcbn1cblxuLmZvcm0tY29udHJvbFtyZWFkb25seV0ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gICAgLy8gb3BhY2l0eTogMTtcbn1cbiJdfQ== */");

/***/ }),

/***/ 5683:
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html ***!
  \***************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<input id=\"uploadBtn\"\n  accept=\"image/*, .pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, .csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel\"\n  type=\"file\" multiple [(ngModel)]=\"fileTextValueFinal\" name=\"file[]\" [ngModelOptions]=\"{standalone: true} \"\n  (change)=\"_fileBrowseHandlerFinal($event.target)\" style=\"position:absolute; top:-100px;\">\n<ion-header [translucent]=\"true\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\" *ngIf=\"allData.showSearchMenu == true\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title mode=\"ios\">\n      New IDEA Generation\n    </ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button (click)=\"heroService.logout()\">\n        <ion-icon slot=\"icon-only\" name=\"log-out-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n  <ion-toolbar class=\"bar-light bar-subheader\" mode=\"ios\">\n    <ion-segment [(ngModel)]=\"section\" (ionChange)=\"segmentChanged($event)\" mode=\"ios\">\n      <ion-segment-button value=\"desc\">\n        <ion-label>Description</ion-label>\n      </ion-segment-button>\n      <ion-segment-button value=\"add\">\n        <ion-label>Add File & Parts</ion-label>\n      </ion-segment-button>\n      <ion-segment-button value=\"idea\">\n        <ion-label>IDEA Details</ion-label>\n      </ion-segment-button>\n    </ion-segment>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"false\" class=\"ion-no-padding\">\n\n  <div [ngSwitch]=\"section\">\n    <form style=\"padding-top: 20px;\" *ngSwitchCase=\"'desc'\">\n      <ion-list lines=\"none\">\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Request Number</label>\n            <input type=\"text\" class=\"form-control input-lg\" [(ngModel)]=\"allData.requestNumber\" name=\"requestNumber\"\n              disabled />\n          </div>\n        </ion-item>\n        <!-- <ion-item>\n          <div class=\"form-group\">\n            <label>IDEA Title<span style=\"color: red;\">*</span></label>\n            <input type=\"text\" class=\"form-control input-lg\" [(ngModel)]=\"allData.ideaTitle\" name=\"ideaTitle\"\n              placeholder=\"Enter IDEA Title\" />\n          </div>\n        </ion-item> -->\n        <ion-item>\n          <div class=\"form-group\">\n            <label>IDEA Description<span style=\"color: red;\">*</span></label>\n            <textarea rows=\"3\" type=\"text\" class=\"form-control input-lg\" [(ngModel)]=\"allData.ideaDesc\" name=\"ideaDesc\"\n              placeholder=\"Enter IDEA Description\"></textarea>\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Approx. Savings (RS./Veh)</label>\n            <!-- (keypress)=\"return (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode == 8 || event.keyCode == 190 || event.keyCode == 110) ? true : false\" -->\n            <!-- (keyup)=\"allData.approxSavings = checkValidation(allData.approxSavings, $event)\" -->\n            <input type=\"number\" class=\"form-control input-lg\" [(ngModel)]=\"allData.approxSavings\" name=\"approxSavings\"\n              placeholder=\"Enter Approx. Savings (RS./Veh)\" />\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Approx. Weight Reduction</label>\n            <input type=\"text\" class=\"form-control input-lg\" [(ngModel)]=\"allData.approxWeight\" name=\"approxWeight\"\n              placeholder=\"Approx. Weight Reduction\" />\n          </div>\n        </ion-item>\n\n      </ion-list>\n    </form>\n    <form style=\"padding-top: 20px;\" *ngSwitchCase=\"'add'\">\n      <ion-list lines=\"none\">\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Attach Files</label>\n            <ion-list lines=\"inset\" class=\"ion-no-padding\">\n              <ng-container *ngIf=\"allData.filesArray.length > 0\">\n                <ion-item *ngFor=\"let file of allData.filesArray; let i = index;\">\n                  <ion-row>\n                    <ion-col size=\"10\" class=\"ion-no-padding\">\n                      <ion-label>{{file.file_name}}</ion-label>\n                    </ion-col>\n                    <ion-col size=\"1\" class=\"ion-no-padding\">\n                      <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_deleteFile(i)\">\n                        <ion-icon slot=\"icon-only\" name=\"close-outline\" style=\"color: red;\"></ion-icon>\n                      </ion-button>\n                    </ion-col>\n                    <ion-col size=\"1\" class=\"ion-no-padding\">\n                      <ion-button *ngIf=\"file.prm_key != undefined\" class=\"ion-no-padding\" size=\"small\" fill=\"clear\"\n                        (click)=\"_downloadFile(file)\">\n                        <ion-icon slot=\"icon-only\" name=\"cloud-download-outline\"></ion-icon>\n                      </ion-button>\n                    </ion-col>\n                  </ion-row>\n                </ion-item>\n              </ng-container>\n              <ion-item class=\"ion-no-padding\">\n                <input type=\"text\" class=\"form-control input-lg\" placeholder=\"Attach Files\"\n                  style=\"border: none;box-shadow: none;\" readonly />\n                <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_openModalXlsx(uploadFileModal)\">\n                  <ion-icon slot=\"icon-only\" name=\"cloud-upload-outline\"></ion-icon>\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Add Part Number</label>\n            <ion-list lines=\"inset\" class=\"ion-no-padding\">\n              <ion-item *ngFor=\"let part of allData.partsAddArray; let i = index;\">\n                <input type=\"text\" #row class=\"form-control input-lg\" placeholder=\"Enter New Part Number & Description\"\n                  style=\"border: none;box-shadow: none; padding: 0px;\" [(ngModel)]=\"part.part_name\"\n                  [ngModelOptions]=\"{standalone: true}\" />\n                <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_deletePart('ADD', i)\"\n                  *ngIf=\"part.part_name != ''\">\n                  <ion-icon slot=\"icon-only\" name=\"close-outline\" style=\"color: red;\"></ion-icon>\n                </ion-button>\n                <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_addPartRow('ADD')\">\n                  <ion-icon slot=\"icon-only\" name=\"add-outline\"></ion-icon>\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item>\n        <!-- <ion-item>\n          <div class=\"form-group\">\n            <label>Add Part Number</label>\n            <ion-list lines=\"inset\" class=\"ion-no-padding\">\n              <ng-container *ngIf=\"allData.partsAddArray.length > 0\">\n                <ion-item *ngFor=\"let part of allData.partsAddArray; let i = index;\">\n                  <ion-label>{{part.part_name}}</ion-label>\n                  <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_deletePart('ADD', i)\">\n                    <ion-icon slot=\"icon-only\" name=\"close-outline\" style=\"color: red;\"></ion-icon>\n                  </ion-button>\n                </ion-item>\n              </ng-container>\n              <ion-item class=\"ion-no-padding\">\n                <input type=\"text\" class=\"form-control input-lg\" placeholder=\"Enter New Part Number & Description\"\n                  style=\"border: none;box-shadow: none;\" [(ngModel)]=\"addPartName\" name=\"addPartName\" />\n                <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_addPartRow('ADD')\">\n                  <ion-icon slot=\"icon-only\" name=\"add-outline\"></ion-icon>\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item> -->\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Delete Part Number</label>\n            <ion-list lines=\"inset\" class=\"ion-no-padding\">\n              <ion-item *ngFor=\"let part of allData.partsModArray; let i = index;\">\n                <input type=\"text\" #row1 class=\"form-control input-lg\" placeholder=\"Enter New Part Number & Description\"\n                  style=\"border: none;box-shadow: none; padding: 0px;\" [(ngModel)]=\"part.part_name\"\n                  [ngModelOptions]=\"{standalone: true}\" />\n                <ion-button *ngIf=\"part.part_name != ''\" class=\"ion-no-padding\" size=\"small\" fill=\"clear\"\n                  (click)=\"_deletePart('MOD', i)\">\n                  <ion-icon slot=\"icon-only\" name=\"close-outline\" style=\"color: red;\"></ion-icon>\n                </ion-button>\n                <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_addPartRow('MOD')\">\n                  <ion-icon slot=\"icon-only\" name=\"add-outline\"></ion-icon>\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item>\n        <!-- <ion-item>\n          <div class=\"form-group\">\n            <label>Delete Part Number</label>\n            <ion-list lines=\"inset\" class=\"ion-no-padding\">\n              <ng-container *ngIf=\"allData.partsModArray.length > 0\">\n                <ion-item *ngFor=\"let part of allData.partsModArray; let i = index;\">\n                  <ion-label>{{part.part_name}}</ion-label>\n                  <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_deletePart('MOD', i)\">\n                    <ion-icon slot=\"icon-only\" name=\"close-outline\" style=\"color: red;\"></ion-icon>\n                  </ion-button>\n                </ion-item>\n              </ng-container>\n              <ion-item class=\"ion-no-padding\">\n                <input type=\"text\" class=\"form-control input-lg\" placeholder=\"Enter New Part Number & Description\"\n                  style=\"border: none;box-shadow: none;\" [(ngModel)]=\"modPartName\" name=\"modPartName\" />\n                <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_addPartRow('MOD')\">\n                  <ion-icon slot=\"icon-only\" name=\"add-outline\"></ion-icon>\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item> -->\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Add Model Applicability</label>\n            <ion-list lines=\"inset\" class=\"ion-no-padding\">\n              <ion-item *ngFor=\"let model of allData.modelAppArray; let i = index;\">\n                <input type=\"text\" #row3 class=\"form-control input-lg\" placeholder=\"Add Model\"\n                  style=\"border: none;box-shadow: none;padding: 0px;\" [(ngModel)]=\"model.model_name\"\n                  [ngModelOptions]=\"{standalone: true}\" />\n                <ion-button *ngIf=\"model.model_name != ''\" class=\"ion-no-padding\" size=\"small\" fill=\"clear\"\n                  (click)=\"_deletePart('Model', i)\">\n                  <ion-icon slot=\"icon-only\" name=\"close-outline\" style=\"color: red;\"></ion-icon>\n                </ion-button>\n                <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_addPartRow('Model')\">\n                  <ion-icon slot=\"icon-only\" name=\"add-outline\"></ion-icon>\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item>\n        <!-- <ion-item>\n          <div class=\"form-group\">\n            <label>Add Model Applicability</label>\n            <ion-list lines=\"inset\" class=\"ion-no-padding\">\n              <ng-container *ngIf=\"allData.modelAppArray.length > 0\">\n                <ion-item *ngFor=\"let model of allData.modelAppArray; let i = index;\">\n                  <ion-label>{{model.model_name}}</ion-label>\n                  <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_deletePart('Model', i)\">\n                    <ion-icon slot=\"icon-only\" name=\"close-outline\" style=\"color: red;\"></ion-icon>\n                  </ion-button>\n                </ion-item>\n              </ng-container>\n              <ion-item class=\"ion-no-padding\">\n                <input type=\"text\" class=\"form-control input-lg\" placeholder=\"Add Model\"\n                  style=\"border: none;box-shadow: none;\" [(ngModel)]=\"addModelName\" name=\"addModelName\" />\n                <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_addPartRow('Model')\">\n                  <ion-icon slot=\"icon-only\" name=\"add-outline\"></ion-icon>\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item> -->\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Additional Fields</label>\n            <ion-list lines=\"inset\" class=\"ion-no-padding\">\n              <ion-item *ngFor=\"let field of allData.fieldsArray; let i = index;\" class=\"ion-no-padding\">\n                <ion-row>\n                  <ion-col size=\"6\">\n                    <select class=\"form-select form-select-sm\" #row4 [id]=\"'row4'+i\"\n                      aria-label=\".form-select-sm example\" [(ngModel)]=\"field.field_id\"\n                      [ngModelOptions]=\"{standalone: true}\" (change)=\"_onFieldChange(field.field_id, i, field)\"\n                      style=\"box-shadow: none;\">\n                      <option selected value=\"\">Select Field</option>\n                      <option *ngFor=\"let item of allData.configValues;\" [value]=\"item.LMF_ID\">{{item.LMF_DESC}}\n                      </option>\n                    </select>\n                  </ion-col>\n                  <ion-col size=\"5\">\n                    <input class=\"form-control form-control-sm\" type=\"text\" [value]=\"field.value\" id=\"field.value\"\n                      [(ngModel)]=\"field.value\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Enter Value\"\n                      style=\"box-shadow: none;\">\n                  </ion-col>\n                  <ion-col size=\"1\" class=\"ion-no-padding\">\n                    <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_addPartRow('Field')\">\n                      <ion-icon slot=\"icon-only\" name=\"add-outline\"></ion-icon>\n                    </ion-button>\n                  </ion-col>\n                </ion-row>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item>\n      </ion-list>\n    </form>\n    <form style=\"padding-top: 20px;\" *ngSwitchCase=\"'idea'\">\n      <ion-list lines=\"none\">\n        <ion-item>\n          <div class=\"form-group\">\n            <label>IDEA Resource</label>\n            <input type=\"text\" class=\"form-control input-lg\" placeholder=\"Enter IDEA Resource\"\n              [(ngModel)]=\"allData.ideaResource\" name=\"ideaResource\" />\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>IDEA Category</label>\n            <ion-select mode=\"ios\" interface=\"popover\" placeholder=\"Select IDEA Category\" [(ngModel)]=\"allData.idaCat\"\n              name=\"idaCat\" (ionChange)=\"onChangeCat(allData.idaCat)\">\n              <ion-select-option *ngFor=\"let item of allData.ideacategoryArray\" value=\"{{item.LMF_ID}}\">\n                {{item.LMF_DESC}}</ion-select-option>\n            </ion-select>\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>If Category is Other</label>\n            <input type=\"text\" class=\"form-control input-lg\" placeholder=\"Enter If Category is Others\"\n              [(ngModel)]=\"allData.otherCat\" name=\"otherCat\" [disabled]=\"!allData.catBtn\" />\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Sector<span style=\"color: red;\">*</span></label>\n            <ion-select mode=\"ios\" interface=\"popover\" placeholder=\"Select Sector\" [(ngModel)]=\"allData.sector\"\n              name=\"sector\" (ionChange)=\"callthis(allData.sector)\">\n              <ion-select-option *ngFor=\"let item of allData.sectorsArray\" value=\"{{item.LMF_DESC}}\">{{item.LMF_DESC}}\n              </ion-select-option>\n            </ion-select>\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Vehicle Platform<span style=\"color: red;\">*</span></label>\n            <ion-select mode=\"ios\" interface=\"popover\" [(ngModel)]=\"allData.vehiclePlt\" name=\"vehiclePlt\"\n              [disabled]=\"allData.vehPlatformsArray.length == 0\" placeholder=\"Select Vehicle Platform\">\n              <ion-select-option *ngFor=\"let item of allData.vehPlatformsArray\" value=\"{{item.LMF_ID}}\">\n                {{item.LMF_DESC}}</ion-select-option>\n            </ion-select>\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Mobile Number<span style=\"color: red;\">*</span></label>\n            <!-- onKeyPress=\"if(this.value.length==10) return false;\" -->\n            <input type=\"number\" class=\"form-control input-lg\" placeholder=\"Enter Mobile Number\"\n              [(ngModel)]=\"allData.mobileNumber\" name=\"mobileNumber\"\n              onKeyPress=\"if(this.value.length==10) return false;\"\n              (keyup)=\"allData.mobileNumber = mobValidation(allData.mobileNumber)\" autocomplete=\"off\" />\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Applicable System</label>\n            <ion-select mode=\"ios\" interface=\"popover\" [(ngModel)]=\"allData.appSys\" name=\"appSys\"\n              [disabled]=\"allData.systemArray.length == 0\" placeholder=\"Select Applicable System\">\n              <ion-select-option *ngFor=\"let item of allData.systemArray\" value=\"{{item.LMF_ID}}\">{{item.LMF_DESC}}\n              </ion-select-option>\n            </ion-select>\n          </div>\n        </ion-item>\n      </ion-list>\n    </form>\n  </div>\n</ion-content>\n\n<ion-footer class=\"ion-no-border\" mode=\"ios\">\n  <ion-toolbar color=\"primary\">\n    <ion-row class=\"ion-no-padding\" *ngIf=\"allData.showSaveBtn == true\">\n      <ion-col class=\"ion-no-padding ion-text-center\" style=\"border-right: 1px solid #fff;\">\n        <ion-button size=\"small\" (click)=\"_save()\"\n          [disabled]=\"\n            (allData.ideaDesc == undefined || allData.ideaDesc == '') || \n            (allData.sector == undefined || allData.sector == '') || \n            (allData.vehiclePlt == undefined || allData.vehiclePlt == '') || (allData.mobileNumber == undefined || allData.mobileNumber == '')\">\n          SAVE</ion-button>\n      </ion-col>\n      <ion-col class=\"ion-no-padding ion-text-center\" style=\"border-left: 1px solid #fff;\">\n        <ion-button size=\"small\" (click)=\"_submit()\"\n          [disabled]=\"\n          (allData.ideaDesc == undefined || allData.ideaDesc == '') || \n          (allData.sector == undefined || allData.sector == '') || \n            (allData.vehiclePlt == undefined || allData.vehiclePlt == '') || (allData.mobileNumber == undefined || allData.mobileNumber == '')\">SUBMIT\n        </ion-button>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-no-padding\" *ngIf=\"allData.showSaveBtn == false\">\n      <ion-col class=\"ion-no-padding ion-text-center\">\n        <ion-button size=\"small\" (click)=\"_submit()\"\n          [disabled]=\"\n          (allData.ideaDesc == undefined || allData.ideaDesc == '') || \n          (allData.sector == undefined || allData.sector == '') || \n            (allData.vehiclePlt == undefined || allData.vehiclePlt == '') || (allData.mobileNumber == undefined || allData.mobileNumber == '')\">SUBMIT\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>\n\n<ng-template #uploadFileModal let-modal>\n  <div class=\"modal-header text-center\"\n    class=\"dcardHeader ideaHeaderColor text-center text-white text-capitalize text-bold-700 font-medium-1 align-middle\">\n    <h4 class=\"modal-title w-100 text-white \" id=\"modal-basic-title \">Upload File</h4>\n    <i class=\"fa fa-times \" aria-hidden=\"true \" title=\"Close \"\n      style=\"margin-top: -20px;cursor: pointer;margin-right: 10px; float: right; \" (click)=\"_cancel() \"></i>\n  </div>\n  <div class=\"modal-body\">\n    <div class=\"container\">\n      <div class=\"row \">\n        <div class=\"col-lg-12 \">\n          <div class=\"row \" style=\"display: block;\">\n            <!-- <div class=\"form-group\"> -->\n            <div class=\"uploadContainer w-100\">\n              <input\n                accept=\"image/*, .pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, .csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel\"\n                class='fileupldInput' type=\"file\" [(ngModel)]=\"fileTextValueFinal\"\n                [ngModelOptions]=\"{standalone: true} \" (change)=\"_fileBrowseHandlerFinal($event.target)\">\n            </div>\n            <!-- _fileBrowseHandlerFinal($event.target.files) -->\n            <p class=\"text-left \" style=\"font-size: 0.8rem; \">\n              Supported File Format :jpeg, jpg, png, doc, docx, pdf, xlsx, xls.\n            </p>\n            <!-- </div> -->\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</ng-template>");

/***/ })

}]);
//# sourceMappingURL=src_app_tab1_tab1_module_ts-es2015.js.map