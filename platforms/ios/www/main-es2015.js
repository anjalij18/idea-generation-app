(self["webpackChunkIdeaApp"] = self["webpackChunkIdeaApp"] || []).push([["main"],{

/***/ 98255:
/*!*******************************************************!*\
  !*** ./$_lazy_route_resources/ lazy namespace object ***!
  \*******************************************************/
/***/ (function(module) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 98255;
module.exports = webpackEmptyAsyncContext;

/***/ }),

/***/ 90158:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": function() { return /* binding */ AppRoutingModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 39895);



const routes = [
    // {
    //   path: '',
    //   loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
    // },
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    {
        path: 'login',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_login_login_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./pages/login/login.module */ 21053)).then(m => m.LoginPageModule)
    },
    {
        path: 'idea',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_idea_idea_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./pages/idea/idea.module */ 9296)).then(m => m.IdeaPageModule)
    },
    {
        path: 'idea-details',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_idea-details_idea-details_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./pages/idea-details/idea-details.module */ 45850)).then(m => m.IdeaDetailsPageModule)
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.NgModule)({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__.PreloadAllModules })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule]
    })
], AppRoutingModule);



/***/ }),

/***/ 55041:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": function() { return /* binding */ AppComponent; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./app.component.html */ 91106);
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component.scss */ 43069);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ 51524);
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ 73494);








let AppComponent = class AppComponent {
    constructor(router, alertController, platform, splashScreen, statusBar) {
        // platform.ready().then(() => {
        this.router = router;
        this.alertController = alertController;
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.lastTimeBackPress = 0;
        this.timePeriodToExit = 2000;
        //   statusBar.styleDefault();
        //   statusBar.hide();
        //   this.splashScreen.hide();
        // });
        this.backButtonEvent();
    }
    ngOnInit() {
        if (localStorage.getItem("loggedinuser") != null && localStorage.getItem("Token") != null) {
            this.router.navigateByUrl('/idea/menu');
        }
        else {
            if (localStorage.getItem("loggedinuser") != null && localStorage.getItem("base64String") != null) {
                this.router.navigateByUrl('/idea/menu');
            }
            else {
                this.router.navigateByUrl('/login');
            }
        }
    }
    backButtonEvent() {
        this.platform.backButton.subscribeWithPriority(0, () => {
            navigator['app'].exitApp();
            // this.routerOutlets.forEach(async (outlet: IonRouterOutlet) => {
            //   if (this.router.url != '/tabs/tab1') {
            //     await this.router.navigate(['/tabs/tab1']);
            //   } else if (this.router.url === '/tabs/tab1') {
            //     if (new Date().getTime() - this.lastTimeBackPress >= this.timePeriodToExit) {
            //       this.lastTimeBackPress = new Date().getTime();
            //       this.presentAlertConfirm();
            //     } else {
            //       navigator['app'].exitApp();
            //     }
            //   }
            // });
        });
    }
    presentAlertConfirm() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                // header: 'Confirm!',
                message: 'Are you sure you want to exit the app?',
                buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => { }
                    }, {
                        text: 'Close App',
                        handler: () => {
                            navigator['app'].exitApp();
                        }
                    }]
            });
            yield alert.present();
        });
    }
};
AppComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.AlertController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.Platform },
    { type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_2__.SplashScreen },
    { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_3__.StatusBar }
];
AppComponent.propDecorators = {
    routerOutlets: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.ViewChildren, args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonRouterOutlet,] }]
};
AppComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], AppComponent);



/***/ }),

/***/ 36747:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": function() { return /* binding */ AppModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/platform-browser */ 39075);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/http/ngx */ 68589);
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ 51524);
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ 73494);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ 63301);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ 90158);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ 55041);
/* harmony import */ var _services_data_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/data.service */ 52468);
/* harmony import */ var _services_hero_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/hero.service */ 19405);
/* harmony import */ var _services_urls__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/urls */ 60881);














let AppModule = class AppModule {
};
AppModule = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.NgModule)({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_5__.AppComponent],
        entryComponents: [],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_11__.BrowserModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.IonicModule.forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__.AppRoutingModule,
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__.ModalModule.forRoot()
        ],
        providers: [
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_13__.RouteReuseStrategy, useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.IonicRouteStrategy },
            _services_urls__WEBPACK_IMPORTED_MODULE_8__.URLS,
            _services_hero_service__WEBPACK_IMPORTED_MODULE_7__.HeroService,
            _services_data_service__WEBPACK_IMPORTED_MODULE_6__.DataService,
            _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_0__.HTTP,
            _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_1__.SplashScreen,
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_2__.StatusBar
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__.AppComponent],
    })
], AppModule);



/***/ }),

/***/ 52468:
/*!******************************************!*\
  !*** ./src/app/services/data.service.ts ***!
  \******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DataService": function() { return /* binding */ DataService; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 26215);



let DataService = class DataService {
    constructor() {
        this.dataSource = new rxjs__WEBPACK_IMPORTED_MODULE_0__.BehaviorSubject("");
        this.detailsDataSource = new rxjs__WEBPACK_IMPORTED_MODULE_0__.BehaviorSubject("");
        this.currentData = this.dataSource.asObservable();
        this.currentData1 = this.detailsDataSource.asObservable();
    }
    changeData(data) {
        this.dataSource.next(data);
    }
    removeData() {
        this.dataSource.next("");
    }
    changeIdeaDetailsData(data) {
        this.detailsDataSource.next(data);
    }
};
DataService.ctorParameters = () => [];
DataService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], DataService);



/***/ }),

/***/ 19405:
/*!******************************************!*\
  !*** ./src/app/services/hero.service.ts ***!
  \******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HeroService": function() { return /* binding */ HeroService; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/http/ngx */ 68589);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _services_urls__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/urls */ 60881);
/* harmony import */ var xml2js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! xml2js */ 49277);
/* harmony import */ var xml2js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(xml2js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var jstoxml__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jstoxml */ 22126);
/* harmony import */ var jstoxml__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jstoxml__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 39895);
var HeroService_1;







// import * as js2xmlparser from "js2xmlparser";
// var js2xmlparser = require("js2xmlparser");

// declare var $: any;
let HeroService = HeroService_1 = class HeroService {
    constructor(urls, http, toastController, router, navCtrl, loadingController) {
        this.urls = urls;
        this.http = http;
        this.toastController = toastController;
        this.router = router;
        this.navCtrl = navCtrl;
        this.loadingController = loadingController;
        this.globalUser = {};
        this.envelopeBuilder_ = null;
        console.log('Hello HttpSoapProvider Provider');
        // this.headers = new HttpHeaders({ 'Content-Type': 'text/xml;charset="utf-8"' })
    }
    globalGet(key) {
        return key ? this.globalUser[key] : this.globalUser;
    }
    globalSet(key, data) {
        this.globalUser[key] = data;
    }
    toXml123(parameters) {
        var xml = "";
        var parameter;
        switch (typeof (parameters)) {
            case "string":
                xml += parameters.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
                break;
            case "number":
            case "boolean":
                xml += parameters.toString();
                break;
            case "object":
                if (parameters.constructor.toString().indexOf("function Date()") > -1) {
                    let year = parameters.getFullYear().toString();
                    let month = ("0" + (parameters.getMonth() + 1).toString()).slice(-2);
                    let date = ("0" + parameters.getDate().toString()).slice(-2);
                    let hours = ("0" + parameters.getHours().toString()).slice(-2);
                    let minutes = ("0" + parameters.getMinutes().toString()).slice(-2);
                    let seconds = ("0" + parameters.getSeconds().toString()).slice(-2);
                    let milliseconds = parameters.getMilliseconds().toString();
                    let tzOffsetMinutes = Math.abs(parameters.getTimezoneOffset());
                    let tzOffsetHours = 0;
                    while (tzOffsetMinutes >= 60) {
                        tzOffsetHours++;
                        tzOffsetMinutes -= 60;
                    }
                    let tzMinutes = ("0" + tzOffsetMinutes.toString()).slice(-2);
                    let tzHours = ("0" + tzOffsetHours.toString()).slice(-2);
                    let timezone = ((parameters.getTimezoneOffset() < 0) ? "-" : "+") + tzHours + ":" + tzMinutes;
                    xml += year + "-" + month + "-" + date + "T" + hours + ":" + minutes + ":" + seconds + "." + milliseconds + timezone;
                }
                else if (parameters.constructor.toString().indexOf("function Array()") > -1) { // Array
                    for (parameter in parameters) {
                        if (parameters.hasOwnProperty(parameter)) {
                            if (!isNaN(parameter)) { // linear array
                                (/function\s+(\w*)\s*\(/ig).exec(parameters[parameter].constructor.toString());
                                var type = RegExp.$1;
                                // debugger
                                switch (type) {
                                    case "":
                                        type = typeof (parameters[parameter]);
                                        break;
                                    case "Object":
                                        type = "tuple";
                                        break;
                                    case "String":
                                        type = "string";
                                        break;
                                    case "Number":
                                        type = "int";
                                        break;
                                    case "Boolean":
                                        type = "bool";
                                        break;
                                    case "Date":
                                        type = "DateTime";
                                        break;
                                }
                                xml += this.toElement(type, parameters[parameter]);
                            }
                            else { // associative array
                                xml += this.toElement(parameter, parameters[parameter]);
                            }
                        }
                    }
                }
                else { // Object or custom function
                    for (parameter in parameters) {
                        if (parameters.hasOwnProperty(parameter)) {
                            xml += this.toElement(parameter, parameters[parameter]);
                        }
                    }
                }
                break;
            default:
                throw new Error("SoapService error: type '" + typeof (parameters) + "' is not supported");
        }
        return xml;
    }
    static stripTagAttributes(tagNamePotentiallyWithAttributes) {
        tagNamePotentiallyWithAttributes = tagNamePotentiallyWithAttributes + ' ';
        return tagNamePotentiallyWithAttributes.slice(0, tagNamePotentiallyWithAttributes.indexOf(' '));
    }
    toElement(tagNamePotentiallyWithAttributes, parameters) {
        var elementContent = this.toXml123(parameters);
        if ("" == elementContent) {
            return "<" + tagNamePotentiallyWithAttributes + "/>";
        }
        else {
            return "<" + tagNamePotentiallyWithAttributes + ">" + elementContent + "</" + HeroService_1.stripTagAttributes(tagNamePotentiallyWithAttributes) + ">";
        }
    }
    set envelopeBuilder(envelopeBuilder) {
        this.envelopeBuilder_ = envelopeBuilder;
    }
    set envelopeBuilder2(envelopeBuilder2) {
        this.envelopeBuilder_ = envelopeBuilder2;
    }
    testService(input, cb) {
        let that = this;
        var request = this.toXml123(input);
        var envelopedRequest = null != this.envelopeBuilder_ ? this.envelopeBuilder_(request) : request;
        console.log(envelopedRequest);
        /////////// New code ////////////////
        let headers = {};
        let soapurl;
        // let finalUser = that.globalGet('option');
        let finalUser = localStorage.getItem('option');
        if (finalUser == 'Supplier') {
            headers = {
                'Content-Type': 'text/xml;charset="utf-8"'
            };
            soapurl = this.urls._baseURL;
        }
        else if (finalUser == 'MnM') {
            let str = localStorage.getItem("base64String");
            headers = {
                'Authorization': 'Basic ' + str,
                'Content-Type': 'text/xml;charset="utf-8"'
            };
            soapurl = this.urls._authURL;
        }
        // that.http.setSSLCertMode('pinned').then((res) => {
        let xml = envelopedRequest;
        that.http.setDataSerializer('utf8');
        that.http.post(soapurl, xml, headers)
            .then((data) => {
            debugger;
            xml2js__WEBPACK_IMPORTED_MODULE_2__.parseString(data.data, function (err, result) {
                if (result == undefined) {
                    cb(err, result);
                }
                else {
                    cb(err, result);
                }
            });
        }).catch((err) => {
            debugger;
            xml2js__WEBPACK_IMPORTED_MODULE_2__.parseString(err.error, function (err, result) {
                if (result == undefined) {
                    cb(err, result);
                }
                else {
                    cb(err, result);
                }
            });
        });
        // });
    }
    testService1(input, cb) {
        let that = this;
        // debugger
        var request = this.toXml123(input);
        var envelopedRequest = null != this.envelopeBuilder_ ? this.envelopeBuilder_(request) : request;
        // function groupChildren(obj) {
        //   for(key in obj) {
        //     if (typeof obj[key] === 'object') {
        //       groupChildren(obj[key]);
        //     } else {
        //       obj['$'] = obj['$'] || {};
        //       obj['$'][key] = obj[key];
        //       delete obj[key];
        //     }
        //   }
        //   return obj;
        // }
        // var obj = {
        //   Level1: {
        //     attribute: 'value',
        //     Level2: {
        //       attribute1: '05/29/2020',
        //       attribute2: '10',
        //       attribute3: 'Pizza'
        //     }
        //   }
        // };
        // var builder = new xml2js.Builder();
        // var xml123 = builder.buildObject(groupChildren(obj));
        // console.log(xml123)
        console.log(envelopedRequest);
        /////////// New code ////////////////
        let headers = {};
        let soapurl;
        let token;
        // let finalUser = that.globalGet('option');
        let finalUser = localStorage.getItem('option');
        console.log("finalUser: " + finalUser);
        if (finalUser == 'Supplier') {
            headers = {
                'Content-Type': 'text/xml;charset="utf-8"'
            };
            token = localStorage.getItem("Token");
            soapurl = this.urls._baseURL + '&SAMLart=' + token;
        }
        else if (finalUser == 'MnM') {
            let str = localStorage.getItem("base64String");
            headers = {
                'Authorization': 'Basic ' + str,
                'Content-Type': 'text/xml;charset="utf-8"'
            };
            soapurl = this.urls._authURL;
        }
        console.log("soapurl: " + soapurl);
        console.log("token: " + token);
        // that.http.setSSLCertMode('pinned').then((res) => {
        // debugger
        // let headers = { 'Content-Type': 'text/xml;charset="utf-8"' };
        let xml = envelopedRequest;
        that.http.setDataSerializer('utf8');
        that.http.post(soapurl, xml, headers)
            .then((data) => {
            debugger;
            xml2js__WEBPACK_IMPORTED_MODULE_2__.parseString(data.data, function (err, result) {
                if (result == undefined) {
                    cb(err, result);
                }
                else {
                    cb(err, result);
                }
            });
        }).catch((err) => {
            debugger;
            xml2js__WEBPACK_IMPORTED_MODULE_2__.parseString(err.error, function (err, result) {
                if (result == undefined) {
                    cb(err, result);
                }
                else {
                    cb(err, result);
                }
            });
        });
        // });
    }
    testService123(input, cb) {
        let that = this;
        debugger;
        var request = (0,jstoxml__WEBPACK_IMPORTED_MODULE_3__.toXML)(input);
        var envelopedRequest = null != this.envelopeBuilder_ ? this.envelopeBuilder_(request) : request;
        console.log(envelopedRequest);
        /////////// New code ////////////////
        let headers = {};
        let soapurl;
        let token;
        // let finalUser = that.globalGet('option');
        let finalUser = localStorage.getItem('option');
        console.log("finalUser: " + finalUser);
        if (finalUser == 'Supplier') {
            headers = {
                'Content-Type': 'text/xml;charset="utf-8"'
            };
            token = localStorage.getItem("Token");
            soapurl = this.urls._baseURL + '&SAMLart=' + token;
        }
        else if (finalUser == 'MnM') {
            let str = localStorage.getItem("base64String");
            headers = {
                'Authorization': 'Basic ' + str,
                'Content-Type': 'text/xml;charset="utf-8"'
            };
            soapurl = this.urls._authURL;
        }
        console.log("soapurl: " + soapurl);
        console.log("token: " + token);
        let xml = envelopedRequest;
        that.http.setDataSerializer('utf8');
        that.http.post(soapurl, xml, headers)
            .then((data) => {
            debugger;
            xml2js__WEBPACK_IMPORTED_MODULE_2__.parseString(data.data, function (err, result) {
                if (result == undefined) {
                    cb(err, result);
                }
                else {
                    cb(err, result);
                }
            });
        }).catch((err) => {
            debugger;
            xml2js__WEBPACK_IMPORTED_MODULE_2__.parseString(err.error, function (err, result) {
                if (result == undefined) {
                    cb(err, result);
                }
                else {
                    cb(err, result);
                }
            });
        });
    }
    _toastrMsg(msg) {
        return this.toastController.create({
            message: msg,
            duration: 2000,
            color: 'dark'
        }).then((toastEl) => {
            toastEl.present();
        });
    }
    _toastrErrorMsg(msg) {
        return this.toastController.create({
            message: msg,
            duration: 3000,
            color: 'danger',
            position: 'middle'
        }).then((toastEl) => {
            toastEl.present();
        });
    }
    logout() {
        let that = this;
        localStorage.clear();
        localStorage.setItem('option', 'MnM');
        that.loadingController.create({
            message: "Logging out...",
            spinner: "bubbles"
        }).then((loadEl) => {
            loadEl.present();
            setTimeout(() => {
                loadEl.dismiss();
                // that.router.navigateByUrl("/login");
                that.navCtrl.navigateRoot(['/login']);
            }, 1000);
        });
    }
    otoa(data) {
        return Array.isArray(data) ? data : [data];
    }
};
HeroService.ctorParameters = () => [
    { type: _services_urls__WEBPACK_IMPORTED_MODULE_1__.URLS },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_0__.HTTP },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.ToastController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.NavController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.LoadingController }
];
HeroService = HeroService_1 = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Injectable)({
        providedIn: 'root'
    })
], HeroService);



/***/ }),

/***/ 60881:
/*!**********************************!*\
  !*** ./src/app/services/urls.ts ***!
  \**********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "URLS": function() { return /* binding */ URLS; }
/* harmony export */ });
class URLS {
    constructor() {
        // _baseURL: string = "http://43.242.214.148:81/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=devmahindra,cn=cordys,cn=defaultInst,o=ADNATEITSOLUTIONS.COM" //dev url;
        this._baseURL = "https://cordys2.mahindra.com:8443/home/Mahindra/com.eibus.web.soap.Gateway.wcp?organization=o=Mahindra,cn=cordys,cn=BOP4,o=corp.mahindra.com"; // production url;
        this._authURL = "https://cordys2.mahindra.com/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=Mahindra,cn=cordys,cn=BOP4,o=corp.mahindra.com"; // for other authentication
    }
}


/***/ }),

/***/ 92340:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": function() { return /* binding */ environment; }
/* harmony export */ });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ 14431:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ 24608);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.module */ 36747);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ 92340);




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.production) {
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.enableProdMode)();
}
(0,_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__.platformBrowserDynamic)().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule)
    .catch(err => console.log(err));


/***/ }),

/***/ 50863:
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/ lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \******************************************************************************************************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var map = {
	"./ion-action-sheet.entry.js": [
		47321,
		"common",
		"node_modules_ionic_core_dist_esm_ion-action-sheet_entry_js"
	],
	"./ion-alert.entry.js": [
		36108,
		"common",
		"node_modules_ionic_core_dist_esm_ion-alert_entry_js"
	],
	"./ion-app_8.entry.js": [
		31489,
		"common",
		"node_modules_ionic_core_dist_esm_ion-app_8_entry_js"
	],
	"./ion-avatar_3.entry.js": [
		10305,
		"common",
		"node_modules_ionic_core_dist_esm_ion-avatar_3_entry_js"
	],
	"./ion-back-button.entry.js": [
		15830,
		"common",
		"node_modules_ionic_core_dist_esm_ion-back-button_entry_js"
	],
	"./ion-backdrop.entry.js": [
		37757,
		"node_modules_ionic_core_dist_esm_ion-backdrop_entry_js"
	],
	"./ion-button_2.entry.js": [
		30392,
		"common",
		"node_modules_ionic_core_dist_esm_ion-button_2_entry_js"
	],
	"./ion-card_5.entry.js": [
		66911,
		"common",
		"node_modules_ionic_core_dist_esm_ion-card_5_entry_js"
	],
	"./ion-checkbox.entry.js": [
		30937,
		"common",
		"node_modules_ionic_core_dist_esm_ion-checkbox_entry_js"
	],
	"./ion-chip.entry.js": [
		78695,
		"common",
		"node_modules_ionic_core_dist_esm_ion-chip_entry_js"
	],
	"./ion-col_3.entry.js": [
		16034,
		"node_modules_ionic_core_dist_esm_ion-col_3_entry_js"
	],
	"./ion-datetime_3.entry.js": [
		68837,
		"common",
		"node_modules_ionic_core_dist_esm_ion-datetime_3_entry_js"
	],
	"./ion-fab_3.entry.js": [
		34195,
		"common",
		"node_modules_ionic_core_dist_esm_ion-fab_3_entry_js"
	],
	"./ion-img.entry.js": [
		41709,
		"node_modules_ionic_core_dist_esm_ion-img_entry_js"
	],
	"./ion-infinite-scroll_2.entry.js": [
		33087,
		"node_modules_ionic_core_dist_esm_ion-infinite-scroll_2_entry_js"
	],
	"./ion-input.entry.js": [
		84513,
		"common",
		"node_modules_ionic_core_dist_esm_ion-input_entry_js"
	],
	"./ion-item-option_3.entry.js": [
		58056,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item-option_3_entry_js"
	],
	"./ion-item_8.entry.js": [
		10862,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item_8_entry_js"
	],
	"./ion-loading.entry.js": [
		7509,
		"common",
		"node_modules_ionic_core_dist_esm_ion-loading_entry_js"
	],
	"./ion-menu_3.entry.js": [
		86030,
		"common",
		"node_modules_ionic_core_dist_esm_ion-menu_3_entry_js"
	],
	"./ion-modal.entry.js": [
		71855,
		"common",
		"node_modules_ionic_core_dist_esm_ion-modal_entry_js"
	],
	"./ion-nav_2.entry.js": [
		38708,
		"common",
		"node_modules_ionic_core_dist_esm_ion-nav_2_entry_js"
	],
	"./ion-popover.entry.js": [
		23527,
		"common",
		"node_modules_ionic_core_dist_esm_ion-popover_entry_js"
	],
	"./ion-progress-bar.entry.js": [
		24694,
		"common",
		"node_modules_ionic_core_dist_esm_ion-progress-bar_entry_js"
	],
	"./ion-radio_2.entry.js": [
		19222,
		"common",
		"node_modules_ionic_core_dist_esm_ion-radio_2_entry_js"
	],
	"./ion-range.entry.js": [
		25277,
		"common",
		"node_modules_ionic_core_dist_esm_ion-range_entry_js"
	],
	"./ion-refresher_2.entry.js": [
		39921,
		"common",
		"node_modules_ionic_core_dist_esm_ion-refresher_2_entry_js"
	],
	"./ion-reorder_2.entry.js": [
		83122,
		"common",
		"node_modules_ionic_core_dist_esm_ion-reorder_2_entry_js"
	],
	"./ion-ripple-effect.entry.js": [
		51602,
		"node_modules_ionic_core_dist_esm_ion-ripple-effect_entry_js"
	],
	"./ion-route_4.entry.js": [
		45174,
		"common",
		"node_modules_ionic_core_dist_esm_ion-route_4_entry_js"
	],
	"./ion-searchbar.entry.js": [
		7895,
		"common",
		"node_modules_ionic_core_dist_esm_ion-searchbar_entry_js"
	],
	"./ion-segment_2.entry.js": [
		76164,
		"common",
		"node_modules_ionic_core_dist_esm_ion-segment_2_entry_js"
	],
	"./ion-select_3.entry.js": [
		20592,
		"common",
		"node_modules_ionic_core_dist_esm_ion-select_3_entry_js"
	],
	"./ion-slide_2.entry.js": [
		27162,
		"node_modules_ionic_core_dist_esm_ion-slide_2_entry_js"
	],
	"./ion-spinner.entry.js": [
		81374,
		"common",
		"node_modules_ionic_core_dist_esm_ion-spinner_entry_js"
	],
	"./ion-split-pane.entry.js": [
		97896,
		"node_modules_ionic_core_dist_esm_ion-split-pane_entry_js"
	],
	"./ion-tab-bar_2.entry.js": [
		25043,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab-bar_2_entry_js"
	],
	"./ion-tab_2.entry.js": [
		77802,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab_2_entry_js"
	],
	"./ion-text.entry.js": [
		29072,
		"common",
		"node_modules_ionic_core_dist_esm_ion-text_entry_js"
	],
	"./ion-textarea.entry.js": [
		32191,
		"common",
		"node_modules_ionic_core_dist_esm_ion-textarea_entry_js"
	],
	"./ion-toast.entry.js": [
		40801,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toast_entry_js"
	],
	"./ion-toggle.entry.js": [
		67110,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toggle_entry_js"
	],
	"./ion-virtual-scroll.entry.js": [
		10431,
		"node_modules_ionic_core_dist_esm_ion-virtual-scroll_entry_js"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function() { return Object.keys(map); };
webpackAsyncContext.id = 50863;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 43069:
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ 91106:
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\n  <ion-router-outlet></ion-router-outlet>\n</ion-app>\n");

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ "use strict";
/******/ 
/******/ var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
/******/ __webpack_require__.O(0, ["vendor"], function() { return __webpack_exec__(14431); });
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main-es2015.js.map