(self["webpackChunkIdeaApp"] = self["webpackChunkIdeaApp"] || []).push([["src_app_pages_login_login_module_ts"],{

/***/ 73403:
/*!*****************************************************!*\
  !*** ./src/app/pages/login/login-routing.module.ts ***!
  \*****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageRoutingModule": function() { return /* binding */ LoginPageRoutingModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.page */ 3058);




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_0__.LoginPage
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ 21053:
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageModule": function() { return /* binding */ LoginPageModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-routing.module */ 73403);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page */ 3058);







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _login_routing_module__WEBPACK_IMPORTED_MODULE_0__.LoginPageRoutingModule
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_1__.LoginPage]
    })
], LoginPageModule);



/***/ }),

/***/ 3058:
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPage": function() { return /* binding */ LoginPage; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./login.page.html */ 31021);
/* harmony import */ var _login_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page.scss */ 28781);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var src_app_services_hero_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/hero.service */ 19405);
/* harmony import */ var src_app_services_urls__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/urls */ 60881);
var LoginPage_1;








// import { SoapService } from 'src/app/services/soap.service';

let LoginPage = LoginPage_1 = class LoginPage {
    constructor(router, 
    // public soapService: SoapService,
    heroService, urls, loadingController, navCtrl) {
        this.router = router;
        this.heroService = heroService;
        this.urls = urls;
        this.loadingController = loadingController;
        this.navCtrl = navCtrl;
        this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormGroup({
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required])
        });
        this.whichUser = "option2";
        this._visiblePass = false;
        localStorage.setItem('option', 'MnM');
    }
    ngOnInit() {
    }
    static adminParam() {
        var parameters = [];
        parameters["sector"] = "";
        return parameters;
    }
    static caseParam(data) {
        var parameters = [];
        parameters["vendorCode"] = data;
        return parameters;
    }
    _checkIfAdmin() {
        let that = this;
        var method = 'CheckUserTypeWeb';
        var parameters = [];
        that.callSubCodeNew(parameters, method, 'admin');
    }
    _checkCaseSensetive() {
        let that = this;
        var method = 'GetSupplierDetail';
        var parameters = [];
        that.callSubCodeNew(parameters, method, 'caseSensetive');
    }
    changeRadio() {
        let that = this;
        console.log("checked value: ", that.whichUser);
        if (that.whichUser == 'option1') { // for supplier work as it is
            localStorage.setItem('option', 'Supplier');
        }
        else if (that.whichUser == 'option2') {
            localStorage.setItem('option', 'MnM');
        }
    }
    static userLogin(username, password) {
        var parameters = [];
        parameters["username"] = username;
        parameters["password"] = password;
        return parameters;
    }
    static checkParam(username) {
        var parameters = [];
        parameters["UM_USER_ID"] = username;
        return parameters;
    }
    _login() {
        let that = this;
        var method;
        debugger;
        let finalUser = localStorage.getItem('option');
        // let finalUser = that.heroService.globalGet('option');
        if (finalUser == 'Supplier') {
            method = 'GenerateSAML';
        }
        else if (finalUser == 'MnM') {
            // convert username and password into base64 format
            let str = btoa(that.loginForm.value.username + ':' + that.loginForm.value.password);
            localStorage.setItem("base64String", str);
            // method = 'CheckActiveUsersIGP'
            method = 'GetUserMasterObject';
        }
        var parameters = [];
        that.callSubCode(parameters, method, finalUser);
    }
    callSubCode(parameters, method, finalUser) {
        let that = this;
        if (finalUser == 'Supplier') {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = LoginPage_1.userLogin(that.loginForm.value.username, that.loginForm.value.password);
        }
        else {
            if (finalUser == 'MnM') {
                parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = LoginPage_1.checkParam(that.loginForm.value.username);
            }
        }
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        that.loadingController.create({
            spinner: 'bubbles',
            message: "Please wait..."
        }).then((loadEl) => {
            loadEl.present();
            that.heroService.testService(parameters, function (err, response) {
                loadEl.dismiss();
                debugger;
                if (response) {
                    let s = response['SOAP:Envelope'];
                    if (finalUser == 'Supplier') {
                        let a = s['SOAP:Body'][0].GenerateSAMLResponse;
                        let b;
                        if (a != undefined) {
                            b = a[0].tuple[0].old[0].generateSAML[0].generateSAML[0];
                            localStorage.setItem("Token", b);
                            localStorage.setItem("loggedinuser", that.loginForm.value.username);
                            ////////////// Anjali new code starts /////////////////
                            // that._checkIfAdmin();
                            that._checkCaseSensetive();
                            ////////////// Anjali new code ends /////////////////
                            // that.navCtrl.navigateRoot(['/idea/menu']);
                            // that.heroService._toastrMsg("Logged in successfully.");
                        }
                        else {
                            b = s['SOAP:Body'][0]["SOAP:Fault"][0].faultstring[0]._;
                            if (b != undefined)
                                // that.heroService._toastrErrorMsg("Error occured while login. Please contact administrator.");
                                that.heroService._toastrErrorMsg(b);
                            else
                                that.heroService._toastrErrorMsg("The username or password you entered is incorrect.");
                        }
                        console.log("login debugger:", b);
                    }
                    else if (finalUser == 'MnM') {
                        // let a = s['SOAP:Body'][0].CheckActiveUsersIGPResponse;
                        let a = s['SOAP:Body'][0].GetUserMasterObjectResponse;
                        let b;
                        if (a != undefined) {
                            b = $.cordys.json.findObjects(a[0], 'USER_MASTER');
                            if (b.length > 0) {
                                localStorage.setItem("loggedinuser", that.loginForm.value.username);
                                ////////////// Anjali new code starts /////////////////
                                that._checkIfAdmin();
                                ////////////// Anjali new code ends /////////////////
                                // that.navCtrl.navigateRoot(['/idea/menu']);
                                // that.heroService._toastrMsg("Logged in successfully.");
                            }
                        }
                        else {
                            b = s['SOAP:Body'][0]["SOAP:Fault"][0].faultstring[0]._;
                            if (b != undefined)
                                // that.heroService._toastrErrorMsg("Error occured while login. Please contact administrator.");
                                that.heroService._toastrErrorMsg(b);
                            else
                                that.heroService._toastrErrorMsg("The username or password you entered is incorrect.");
                        }
                        console.log("login debugger:", b);
                    }
                }
                else {
                    that.heroService._toastrErrorMsg("Error occured while logging. Please contact administrator.");
                }
            });
        });
    }
    callSubCodeNew(parameters, method, key) {
        let that = this;
        if (key == 'admin') {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = LoginPage_1.adminParam();
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == 'caseSensetive') {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = LoginPage_1.caseParam(that.loginForm.value.username);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        that.loadingController.create({
            spinner: 'bubbles',
            message: "Please wait..."
        }).then((loadEl) => {
            loadEl.present();
            that.heroService.testService1(parameters, function (err, response) {
                loadEl.dismiss();
                debugger;
                if (response) {
                    let s = response['SOAP:Envelope'];
                    debugger;
                    if (key == 'caseSensetive') {
                        let a = s['SOAP:Body'][0].GetSupplierDetailResponse;
                        if (a != undefined) {
                            let obj = $.cordys.json.findObjects(a[0], "SUPPLIER_MASTER");
                            console.log("check case obj in login screen: ", obj[0]);
                            debugger;
                            if (obj.length > 0) {
                                that._checkIfAdmin();
                            }
                            else {
                                that.heroService._toastrErrorMsg("Username or Password is incorrect.");
                                localStorage.clear();
                                that.loginForm.reset();
                                that.whichUser = "option2";
                                localStorage.setItem('option', 'MnM');
                            }
                        }
                    }
                    else if (key == 'admin') {
                        let a = s['SOAP:Body'][0].CheckUserTypeWebResponse;
                        if (a != undefined) {
                            let obj = $.cordys.json.findObjects(a[0], "CheckUserTypeWeb");
                            console.log("check admin obj in login screen: ", obj[0]);
                            debugger;
                            if (obj.length > 0) {
                                let role = obj[0].CheckUserTypeWeb[0];
                                if (role.includes("No IGP roles assigned")) {
                                    that.heroService._toastrErrorMsg("User do not have any access!");
                                    localStorage.clear();
                                    that.loginForm.reset();
                                    that.whichUser = "option2";
                                    localStorage.setItem('option', 'MnM');
                                }
                                else {
                                    that.navCtrl.navigateRoot(['/idea/menu']);
                                    that.heroService._toastrMsg("Logged in successfully.");
                                }
                            }
                        }
                    }
                }
                else {
                    that.heroService._toastrErrorMsg("Error occured while logging. Please contact administrator.");
                }
            });
        });
    }
    _showPassword() {
        let that = this;
        that._visiblePass = !that._visiblePass;
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: src_app_services_hero_service__WEBPACK_IMPORTED_MODULE_2__.HeroService },
    { type: src_app_services_urls__WEBPACK_IMPORTED_MODULE_3__.URLS },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.NavController }
];
LoginPage = LoginPage_1 = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-login',
        template: _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], LoginPage);



/***/ }),

/***/ 28781:
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#loginBg {\n  --background: url('loginBg.png');\n}\n\n#formBg {\n  height: 100%;\n  width: 100%;\n  background-image: url('simplebg.png');\n  background-repeat: no-repeat;\n  background-size: 100% 100%;\n}\n\n.sub_div {\n  position: absolute;\n  bottom: 0px;\n  width: 100%;\n}\n\n.formDiv {\n  padding-top: 30px;\n  padding-bottom: 30px;\n}\n\n.logo {\n  background-size: contain;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-color: transparent;\n  width: 50%;\n  height: 18%;\n  margin: 25% auto 7px auto;\n  text-align: center;\n}\n\n.logo img {\n  height: 200px;\n}\n\n.form-control {\n  border: none;\n  font-size: 1.25rem;\n}\n\ninput {\n  border: none;\n}\n\ninput:focus,\nselect:focus,\ntextarea:focus,\nbutton:focus {\n  outline: none;\n}\n\n.input-group {\n  border: 1.5px solid #2c5364;\n  border-radius: 5px;\n  padding: 0px;\n  background-color: white;\n}\n\n.iconC {\n  margin-top: 8px;\n  margin-left: 5px;\n  color: #2c5364;\n  font-size: 28px;\n}\n\n.iconR {\n  margin-top: 8px;\n  margin-right: 8px;\n  color: #2c5364;\n  font-size: 28px;\n}\n\n.radio-item {\n  display: inline-block;\n  position: relative;\n  padding: 0 6px;\n  margin: 10px 0 0;\n}\n\n.radio-item input[type=radio] {\n  display: none;\n}\n\n.radio-item label {\n  color: #2c5364;\n  font-weight: bold;\n  font-size: 1.25rem;\n}\n\n.radio-item label:before {\n  content: \" \";\n  display: inline-block;\n  position: relative;\n  top: 5px;\n  margin: 0 5px 0 0;\n  width: 20px;\n  height: 20px;\n  border-radius: 11px;\n  border: 2px solid #2c5364;\n  background-color: transparent;\n}\n\n.radio-item input[type=radio]:checked + label:after {\n  border-radius: 11px;\n  width: 12px;\n  height: 12px;\n  position: absolute;\n  top: 9px;\n  left: 10px;\n  content: \" \";\n  display: block;\n  background: #2c5364;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdDQUFBO0FBQ0o7O0FBRUE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHFDQUFBO0VBQ0EsNEJBQUE7RUFFQSwwQkFBQTtBQUFKOztBQUdBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtBQUFKOztBQUdBO0VBQ0ksaUJBQUE7RUFFQSxvQkFBQTtBQURKOztBQVFBO0VBQ0ksd0JBQUE7RUFDQSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0EsNkJBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUFMSjs7QUFPSTtFQUNJLGFBQUE7QUFMUjs7QUFTQTtFQUVJLFlBQUE7RUFFQSxrQkFBQTtBQVJKOztBQVVBO0VBQ0ksWUFBQTtBQVBKOztBQVVBOzs7O0VBSUksYUFBQTtBQVBKOztBQVVBO0VBRUksMkJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtBQVJKOztBQVdBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFSSjs7QUFVQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FBUEo7O0FBU0E7RUFDSSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FBTko7O0FBU0U7RUFDRSxhQUFBO0FBTko7O0FBU0U7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQU5KOztBQVNFO0VBQ0UsWUFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSw2QkFBQTtBQU5KOztBQVNFO0VBQ0UsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0FBTkoiLCJmaWxlIjoibG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2xvZ2luQmcge1xuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9sb2dpbkJnLnBuZ1wiKTtcbn1cblxuI2Zvcm1CZyB7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvc2ltcGxlYmcucG5nXCIpO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgLy8gYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMTAwJTtcbiAgICAvLyBwb3NpdGlvbjogZml4ZWQ7XG59XG4uc3ViX2RpdiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4uZm9ybURpdiB7XG4gICAgcGFkZGluZy10b3A6IDMwcHg7XG4gICAgLy8gcGFkZGluZy1ib3R0b206IDUwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDMwcHg7XG59XG4vLyAud2FybmluZyB7XG4vLyAgICAgcG9zaXRpb246IGFic29sdXRlO1xuLy8gICAgIGhlaWdodDogMTAwJTtcbi8vICAgICB3aWR0aDogMTAwJTtcbi8vIH1cbi5sb2dvIHtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBoZWlnaHQ6IDE4JTtcbiAgICBtYXJnaW46IDI1JSBhdXRvIDdweCBhdXRvO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcblxuICAgIGltZyB7XG4gICAgICAgIGhlaWdodDogMjAwcHg7XG4gICAgfVxufVxuXG4uZm9ybS1jb250cm9sIHtcbiAgICAvLyBwYWRkaW5nOiAyNXB4IDEwcHg7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIC8vIGJvcmRlcjogMS41cHggc29saWQgIzJjNTM2NDtcbiAgICBmb250LXNpemU6IDEuMjVyZW07XG59XG5pbnB1dCB7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIC8vIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xufVxuaW5wdXQ6Zm9jdXMsXG5zZWxlY3Q6Zm9jdXMsXG50ZXh0YXJlYTpmb2N1cyxcbmJ1dHRvbjpmb2N1cyB7XG4gICAgb3V0bGluZTogbm9uZTtcbn1cblxuLmlucHV0LWdyb3VwIHtcbiAgICAvLyBib3JkZXI6IDFweCBncmF5IHNvbGlkO1xuICAgIGJvcmRlcjogMS41cHggc29saWQgIzJjNTM2NDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgcGFkZGluZzogMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4uaWNvbkMge1xuICAgIG1hcmdpbi10b3A6IDhweDtcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xuICAgIGNvbG9yOiAjMmM1MzY0O1xuICAgIGZvbnQtc2l6ZTogMjhweDtcbn1cbi5pY29uUiB7XG4gICAgbWFyZ2luLXRvcDogOHB4O1xuICAgIG1hcmdpbi1yaWdodDogOHB4O1xuICAgIGNvbG9yOiAjMmM1MzY0O1xuICAgIGZvbnQtc2l6ZTogMjhweDtcbn1cbi5yYWRpby1pdGVtIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHBhZGRpbmc6IDAgNnB4O1xuICAgIG1hcmdpbjogMTBweCAwIDA7XG4gIH1cbiAgXG4gIC5yYWRpby1pdGVtIGlucHV0W3R5cGU9J3JhZGlvJ10ge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbiAgXG4gIC5yYWRpby1pdGVtIGxhYmVsIHtcbiAgICBjb2xvcjogIzJjNTM2NDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDEuMjVyZW07XG4gIH1cbiAgXG4gIC5yYWRpby1pdGVtIGxhYmVsOmJlZm9yZSB7XG4gICAgY29udGVudDogXCIgXCI7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6IDVweDtcbiAgICBtYXJnaW46IDAgNXB4IDAgMDtcbiAgICB3aWR0aDogMjBweDtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTFweDtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjMmM1MzY0O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICB9XG4gIFxuICAucmFkaW8taXRlbSBpbnB1dFt0eXBlPXJhZGlvXTpjaGVja2VkICsgbGFiZWw6YWZ0ZXIge1xuICAgIGJvcmRlci1yYWRpdXM6IDExcHg7XG4gICAgd2lkdGg6IDEycHg7XG4gICAgaGVpZ2h0OiAxMnB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDlweDtcbiAgICBsZWZ0OiAxMHB4O1xuICAgIGNvbnRlbnQ6IFwiIFwiO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGJhY2tncm91bmQ6ICMyYzUzNjQ7XG4gIH1cblxuIl19 */");

/***/ }),

/***/ 31021:
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
  \***********************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar color=\"primary\">\n    <ion-title style=\"text-align: center;\">Welcome To IDEA Generation</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content id=\"loginBg\">\n  <div class=\"warning\">\n    <div class=\"logo\">\n      <img src=\"assets/images/dummylogo.png\">\n    </div>\n    <div class=\"sub_div\">\n      <form [formGroup]=\"loginForm\" class=\"ion-padding\" id=\"formBg\">\n        <div class=\"formDiv\">\n          <div class=\"form-group input-group\">\n            <span>\n              <ion-icon class=\"iconC\" name=\"person-circle-outline\"></ion-icon>\n            </span>\n            <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\"\n              placeholder=\"Enter User Name\" formControlName=\"username\" style=\"box-shadow: none;\">\n          </div>\n          <br>\n          <div class=\"form-group input-group\">\n            <span>\n              <ion-icon class=\"iconC\" name=\"lock-closed-outline\"></ion-icon>\n            </span>\n            <input type=\"password\" *ngIf=\"_visiblePass == false\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"Enter Password\"\n              formControlName=\"password\" style=\"box-shadow: none;\">\n            <input type=\"text\" *ngIf=\"_visiblePass == true\" class=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"Enter Password\"\n              formControlName=\"password\" style=\"box-shadow: none;\">\n            <span>\n              <ion-icon class=\"iconR\" *ngIf=\"_visiblePass == false\" name=\"eye-off-outline\" (click)=\"_showPassword()\"></ion-icon>\n              <ion-icon class=\"iconR\" *ngIf=\"_visiblePass == true\" name=\"eye-outline\" (click)=\"_showPassword()\"></ion-icon>\n            </span>\n          </div>\n          <!--Check if supplier or mnm user-->\n          <div class=\"row ion-no-padding\">\n            <div class=\"col-6 ion-no-padding\">\n              <div class=\"form-check form-check-inline radio-item\">\n                <input type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio1\" value=\"option1\" [(ngModel)]=\"whichUser\"\n                  name=\"whichUser\" [ngModelOptions]=\"{standalone: true}\" (change)=\"changeRadio()\">\n                <label for=\"inlineRadio1\">Supplier</label>\n              </div>\n            </div>\n            <div class=\"col-6 ion-no-padding\">\n              <div class=\"form-check form-check-inline radio-item\">\n                <input type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio2\" value=\"option2\" [(ngModel)]=\"whichUser\"\n                  name=\"whichUser\" [ngModelOptions]=\"{standalone: true}\" (change)=\"changeRadio()\">\n                <label for=\"inlineRadio2\">M&M User</label>\n              </div>\n            </div>\n          </div>\n          <br>\n          <!--Check if supplier or mnm user-->\n          <ion-button expand=\"block\" color=\"primary\" size=\"large\" (click)=\"_login()\"\n            [disabled]=\"whichUser == undefined\">Login</ion-button>\n        </div>\n      </form>\n    </div>\n  </div>\n\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_login_login_module_ts-es2015.js.map