(self["webpackChunkIdeaApp"] = self["webpackChunkIdeaApp"] || []).push([["src_app_pages_idea-details_idea-details_module_ts"],{

/***/ 43604:
/*!*******************************************************************!*\
  !*** ./src/app/pages/idea-details/idea-details-routing.module.ts ***!
  \*******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IdeaDetailsPageRoutingModule": function() { return /* binding */ IdeaDetailsPageRoutingModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _idea_details_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./idea-details.page */ 10956);




const routes = [
    {
        path: '',
        component: _idea_details_page__WEBPACK_IMPORTED_MODULE_0__.IdeaDetailsPage
    }
];
let IdeaDetailsPageRoutingModule = class IdeaDetailsPageRoutingModule {
};
IdeaDetailsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], IdeaDetailsPageRoutingModule);



/***/ }),

/***/ 45850:
/*!***********************************************************!*\
  !*** ./src/app/pages/idea-details/idea-details.module.ts ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IdeaDetailsPageModule": function() { return /* binding */ IdeaDetailsPageModule; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _idea_details_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./idea-details-routing.module */ 43604);
/* harmony import */ var _idea_details_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./idea-details.page */ 10956);







let IdeaDetailsPageModule = class IdeaDetailsPageModule {
};
IdeaDetailsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _idea_details_routing_module__WEBPACK_IMPORTED_MODULE_0__.IdeaDetailsPageRoutingModule
        ],
        declarations: [_idea_details_page__WEBPACK_IMPORTED_MODULE_1__.IdeaDetailsPage]
    })
], IdeaDetailsPageModule);



/***/ }),

/***/ 10956:
/*!*********************************************************!*\
  !*** ./src/app/pages/idea-details/idea-details.page.ts ***!
  \*********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IdeaDetailsPage": function() { return /* binding */ IdeaDetailsPage; }
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_idea_details_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./idea-details.page.html */ 21995);
/* harmony import */ var _idea_details_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./idea-details.page.scss */ 28030);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/data.service */ 52468);
/* harmony import */ var src_app_services_hero_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/hero.service */ 19405);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 39895);
var IdeaDetailsPage_1;








let IdeaDetailsPage = IdeaDetailsPage_1 = class IdeaDetailsPage {
    constructor(dataService, loadingController, heroService, router) {
        this.dataService = dataService;
        this.loadingController = loadingController;
        this.heroService = heroService;
        this.router = router;
        this.allData = {};
        this.section = "desc";
        this.allData.detailedData = {};
        this.allData.showEditContainer = false;
        this.allData.filesArray = [];
        this.allData.systemArray = [];
        this.allData.vehPlatformsArray = [];
        this.allData.sectorsArray = [];
        this.allData.ideacategoryArray = [];
        // this.allData.partsAddArray = [];
        // this.allData.partsModArray = []
        // this.allData.modelAppArray = [];
        this.allData.partsAddArray = [
            {
                index: 0,
                part_name: "",
                part_type: "ADD"
            }
        ];
        this.allData.partsModArray = [
            {
                index: 0,
                part_name: "",
                part_type: "MOD"
            }
        ];
        this.allData.modelAppArray = [
            {
                index: 0,
                model_name: ""
            }
        ];
        this.allData.configValues = [];
        this.allData.fieldsArray = [
            {
                index: 0,
                field_name: "",
                field_id: "",
                value: null
            }
        ];
        this.allData.catBtn = false;
        this.dataService.currentData1.subscribe((data) => {
            let data1 = data;
            // debugger
            this.allData.detailedData = {
                IID_IDEA_DESCRIPTION: (data1.IID_IDEA_DESCRIPTION[0].$ ? undefined : data1.IID_IDEA_DESCRIPTION[0]),
                IID_APP_SAVINGS: (data1.IID_APP_SAVINGS[0].$ ? undefined : data1.IID_APP_SAVINGS[0]),
                IID_IDEA_RESOURCE: (data1.IID_IDEA_RESOURCE[0].$ ? undefined : data1.IID_IDEA_RESOURCE[0]),
                IID_APP_WT_REDUCTION: (data1.IID_APP_WT_REDUCTION[0].$ ? undefined : data1.IID_APP_WT_REDUCTION[0]),
                IID_IDEA_CAT: (data1.IID_IDEA_CAT[0].$ ? undefined : data1.IID_IDEA_CAT[0]),
                IGPCAT: (data1.IGPCAT[0].$ ? undefined : data1.IGPCAT[0]),
                IID_IDEA_CAT_OTH: (data1.IID_IDEA_CAT_OTH[0].$ ? undefined : data1.IID_IDEA_CAT_OTH[0]),
                VECHPLATF: (data1.VECHPLATF[0].$ ? undefined : data1.VECHPLATF[0]),
                APPLSYS: (data1.APPLSYS[0].$ ? undefined : data1.APPLSYS[0]),
                IID_MOBILE_NUMBER: (data1.IID_MOBILE_NUMBER[0].$ ? undefined : data1.IID_MOBILE_NUMBER[0]),
                IID_IDEA_GEN_TOKEN_NO: data1.IID_IDEA_GEN_TOKEN_NO[0],
                IID_IDEA_GEN_NAME: data1.IID_IDEA_GEN_NAME[0],
                IID_IDEA_GEN_EMAIL: data1.IID_IDEA_GEN_EMAIL[0],
                IID_CREATED_ON: data1.IID_CREATED_ON[0],
                VECPNUMBER: (data1.VECPNUMBER[0].$ ? undefined : data1.VECPNUMBER[0]),
                IID_REQUEST_ID: data1.IID_REQUEST_ID[0],
                IID_STATUS: data1.IID_STATUS[0],
                IID_VEHICLE_PLAT: data1.IID_VEHICLE_PLAT[0],
                IID_SECTOR: data1.IID_SECTOR[0],
                IID_APP_SYSTEM: (data1.IID_APP_SYSTEM[0].$ ? undefined : data1.IID_APP_SYSTEM[0])
            };
            this.allData.reqNum = this.allData.detailedData.IID_REQUEST_ID;
            console.log("check details data: ", this.allData.detailedData);
        });
    }
    ngOnInit() {
        let that = this;
        that.allData.alreadySavedPartsCount = 0;
        that.allData.alreadySavedDocsCount = 0;
        that.allData.alreadySavedFieldsCount = 0;
        that.allData.alreadyDelPartsCount = 0;
        that.allData.alreadySavedModelCount = 0;
        that.allData.alreadySavedDocs = [];
        that.allData.alreadySavedParts = [];
        that.allData.alreadyDelParts = [];
        that.allData.alreadySavedModel = [];
        that.allData.alreadySavedFields = [];
        that.allData.allDd = [];
        this._checkIfAdmin();
    }
    segmentChanged(ev) {
        console.log('Segment changed', ev);
        console.log('Segment changed to', ev.detail.value);
        this.section = ev.detail.value;
    }
    onChangeCat(value) {
        console.log("selected idea: ", value);
        let that = this;
        if (value == 'Other') {
            // if (value == 'VAVE_LEVER_OTHER') { // checking with id
            that.allData.catBtn = true;
        }
        else {
            that.allData.catBtn = false;
        }
    }
    _openModalXlsx() {
        $("#uploadBtnEdit").click();
    }
    _fileBrowseHandlerFinal(files) {
        this._prepareFilesListFinal(files.files);
    }
    _prepareFilesListFinal(files) {
        let that = this;
        // that.allData.base64ContentFinal = "";
        // that.allData.UploadFile = "Choose a file or drag it here";
        // that._setupReader(files[0]);
        debugger;
        that.allData.filesArray.push({
            file_name: "",
            file_path: ""
        });
        for (let i = 0; i < files.length; i++) {
            that.allData.base64ContentFinal = "";
            that.allData.UploadFile = "Choose a file or drag it here";
            that._setupReader(files[i]);
        }
    }
    _showInfo() {
    }
    _setupReader(file) {
        let that = this;
        var reader = new FileReader();
        reader.onload = function (e) {
            var temp = reader.result;
            that.allData.base64ContentFinal = String(temp).split(",")[1];
            that.allData.UploadFile = file.name;
            for (let i = 0; i < that.allData.filesArray.length; i++) {
                if (file.name == that.allData.filesArray[i].file_name) {
                    that.heroService._toastrErrorMsg("This file is already attached.");
                    break;
                }
                if (that.allData.filesArray[i].file_name == "") {
                    that.allData.filesArray[i].file_name = file.name;
                    that.allData.filesArray[i].file_path = that.allData.base64ContentFinal;
                    break;
                }
            }
            that._cancel();
        };
        reader.readAsDataURL(file);
    }
    mobValidation(val) {
        debugger;
        let num = val.toString();
        if (num.length <= 10) {
            return val;
        }
        else {
            let val1 = num.substr(0, 10);
            return Number(val1);
        }
    }
    _cancel() {
        let that = this;
        // that.tempmodalRef.hide();
        that.allData.errorMsg = "";
        that.allData.fileName = "";
        that.allData.base64Content = "";
        that.allData.selectedFile = "Choose a file or drag it here";
    }
    static adminParam(user) {
        var parameters = [];
        parameters["sector"] = "";
        return parameters;
    }
    static adminParam1() {
        return {};
    }
    static dropdownParam(reqType) {
        var parameters = [];
        parameters["reqType"] = reqType;
        return parameters;
    }
    static savedDocsParam(uid) {
        var parameters = [];
        parameters["UID"] = uid;
        return parameters;
    }
    static dropdwParam(data) {
        var parameters = [];
        parameters["sector"] = data.sector;
        parameters["LMFID"] = data.LMFID;
        return parameters;
    }
    static dropdwParam1(data) {
        var parameters = [];
        parameters["LMFID"] = data.LMFID;
        return parameters;
    }
    static fieldsParam(data) {
        var parameters = [];
        parameters["requestID"] = data.requestID;
        return parameters;
    }
    static UIDParam(uidNum) {
        var parameters = [];
        parameters["UIDNumber"] = uidNum;
        return parameters;
    }
    callSubCode(parameters, method, params, key) {
        let that = this;
        if (key == 'sector') {
            parameters[method + ' xmlns="http://schemas.cordys.com/igp"'] = IdeaDetailsPage_1.dropdownParam(params.reqType);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == 'admin') {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.adminParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "ideaCat") {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.dropdownParam(params.reqType);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "savedDocs") {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.savedDocsParam(params.UID);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "savedAddParts") {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.savedDocsParam(params.UID);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "savedModParts") {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.savedDocsParam(params.UID);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "vehDropDw") {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.dropdwParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "appSysDropDw") {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.dropdwParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "idCatDropDw") {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.dropdwParam1(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "config") {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.adminParam1();
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "savedFields") {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.fieldsParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "sendMail") {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.UIDParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "VehiclePltArray") {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.dropdownParam(params.reqType);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "appSysArray") {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.dropdownParam(params.reqType);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "savedModels") {
            parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.savedDocsParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        that.loadingController.create({
            spinner: 'bubbles',
            message: 'Please wait...'
        }).then((loadEl) => {
            loadEl.present();
            that.heroService.testService1(parameters, function (err, response) {
                loadEl.dismiss();
                // debugger
                if (response) {
                    let s = response['SOAP:Envelope'];
                    if (key == 'sector') {
                        var a = s['SOAP:Body'][0].GetLOVObjectForIdeaGenResponse;
                        var b = a[0].tuple;
                        let daa = b.map((d) => { return d.old[0].LOV_MASTER_AUTO[0]; });
                        that.allData.sectorsArray = daa;
                        let method = 'GetLOVObjectForIdeaGen';
                        var parameters = [];
                        let params1 = {
                            reqType: "VAVE_LEVER"
                        };
                        that.callSubCode(parameters, method, params1, 'ideaCat');
                    }
                    else if (key == 'admin') {
                        let a = s['SOAP:Body'][0].CheckUserTypeWebResponse;
                        if (a != undefined) {
                            let obj = $.cordys.json.findObjects(a[0], "CheckUserTypeWeb");
                            console.log("check admin obj: ", obj[0]);
                            debugger;
                            // if (obj.length && obj[0]?.old?.CheckUserTypeWeb?.CheckUserTypeWeb) {
                            if (obj.length > 0) {
                                that._doFurther(obj[0]);
                            }
                        }
                    }
                    else if (key == 'ideaCat') {
                        var a = s['SOAP:Body'][0].GetLOVObjectForIdeaGenResponse;
                        var b = a[0].tuple;
                        let daa = b.map((d) => { return d.old[0].LOV_MASTER_AUTO[0]; });
                        that.allData.ideacategoryArray = daa;
                        that._getSavedDocs();
                    }
                    else if (key == 'savedDocs') {
                        let a = s['SOAP:Body'][0].GetIdeaDocOnUIDResponse;
                        if (a != undefined) {
                            let obj = $.cordys.json.findObjects(a[0], "IGP_DOCS");
                            console.log("check saved docs obj: ", obj);
                            if (that.heroService.otoa(obj).length > 0) {
                                that._doFurtherParts(that.heroService.otoa(obj), 'DOCS');
                            }
                        }
                        that._getPartsData();
                    }
                    else if (key == "savedAddParts") {
                        let a = s['SOAP:Body'][0].GetAddedPartsResponse;
                        if (a != undefined) {
                            let obj = $.cordys.json.findObjects(a[0], "IGP_PART_DETAILS");
                            console.log("check saved added parts obj: ", obj);
                            if (that.heroService.otoa(obj).length > 0) {
                                that._doFurtherParts(that.heroService.otoa(obj), 'ADD');
                            }
                        }
                        let params = {
                            UID: that.allData.reqNum
                        };
                        let parameters = [];
                        let method1 = 'GetDeletedParts';
                        that.callSubCode(parameters, method1, params, 'savedModParts');
                    }
                    else if (key == "savedModParts") {
                        let a = s['SOAP:Body'][0].GetDeletedPartsResponse;
                        if (a != undefined) {
                            let obj = $.cordys.json.findObjects(a[0], "IGP_PART_DETAILS");
                            console.log("check saved deleted parts obj: ", obj);
                            if (that.heroService.otoa(obj).length > 0) {
                                that._doFurtherParts(that.heroService.otoa(obj), 'MOD');
                            }
                        }
                        that._getConfigData();
                        // that._getDropDownData();
                    }
                    else if (key == "vehDropDw") {
                        let a = s['SOAP:Body'][0].GetModelAffDescResponse;
                        if (a != undefined) {
                            let obj = $.cordys.json.findObjects(a[0], "LOV_MASTER_AUTO");
                            console.log("check veh dropdown obj: ", obj);
                            that.allData.vehiclePlt = that.heroService.otoa(obj)[0].LMF_ID[0];
                        }
                        let parameters = [];
                        if (that.allData.detailedData.IID_APP_SYSTEM != undefined) {
                            let params1 = {
                                sector: that.allData.detailedData.IID_SECTOR,
                                LMFID: that.allData.detailedData.IID_APP_SYSTEM
                            };
                            that.callSubCode(parameters, 'GetAppSyssDesc', params1, 'appSysDropDw');
                        }
                        else {
                            if (that.allData.detailedData.IID_IDEA_CAT != undefined) {
                                let params1 = {
                                    LMFID: that.allData.detailedData.IID_IDEA_CAT
                                };
                                that.callSubCode(parameters, 'GetIdeaCatDesc', params1, 'idCatDropDw');
                            }
                        }
                    }
                    else if (key == "appSysDropDw") {
                        // debugger
                        let a = s['SOAP:Body'][0].GetAppSyssDescResponse;
                        if (a != undefined) {
                            let obj = $.cordys.json.findObjects(a[0], "LOV_MASTER_AUTO");
                            console.log("check app sys dropdown obj: ", obj);
                            that.allData.appSys = that.heroService.otoa(obj)[0].LMF_ID[0];
                        }
                        if (that.allData.detailedData.IID_IDEA_CAT != undefined) {
                            let params1 = {
                                LMFID: that.allData.detailedData.IID_IDEA_CAT
                            };
                            that.callSubCode(parameters, 'GetIdeaCatDesc', params1, 'idCatDropDw');
                        }
                    }
                    else if (key == "idCatDropDw") {
                        // debugger
                        let a = s['SOAP:Body'][0].GetIdeaCatDescResponse;
                        if (a != undefined) {
                            let obj = $.cordys.json.findObjects(a[0], "LOV_MASTER_AUTO");
                            console.log("check idea cat dropdown obj: ", obj);
                            that.allData.idaCat = that.heroService.otoa(obj)[0].LMF_ID[0];
                        }
                    }
                    else if (key == "config") {
                        debugger;
                        let a = s['SOAP:Body'][0].GetIdeaConfigResponse;
                        if (a != undefined) {
                            that.allData.allDd = [];
                            let obj = $.cordys.json.findObjects(a[0], "LOV_MASTER_AUTO");
                            console.log("check config obj: ", obj);
                            that.allData.allDd = that.heroService.otoa(obj);
                            that.allData.configValues = that.heroService.otoa(obj);
                        }
                        that._getSavedFieldValues();
                    }
                    else if (key == "savedFields") {
                        debugger;
                        let a = s['SOAP:Body'][0].GetConfigOnUIDResponse;
                        if (a != undefined) {
                            let obj = $.cordys.json.findObjects(a[0], "IGP_CONFIG_FIELD");
                            console.log("check saved fields obj: ", obj);
                            if (that.heroService.otoa(obj).length > 0) {
                                that._doFurtherParts(that.heroService.otoa(obj), 'Fields');
                            }
                            else {
                                that.allData.fieldsArray = [
                                    {
                                        index: 0,
                                        field_name: "",
                                        field_id: "",
                                        value: null
                                    }
                                ];
                            }
                        }
                        that._getSavedModelApp();
                    }
                    else if (key == "sendMail") {
                        let a = s['SOAP:Body'][0].IGP_EmaiToSectorHODResponse;
                        if (a != undefined) {
                            that._toastrMsg("E-mail Sent Successfully.");
                        }
                    }
                    else if (key == "VehiclePltArray") {
                        debugger;
                        let a = s['SOAP:Body'][0].GetLOVObjectForIdeaGenResponse;
                        if (a != undefined) {
                            if (a[0].tuple != undefined) {
                                var b = a[0].tuple;
                                let daa = b.map((d) => { return d.old[0].LOV_MASTER_AUTO[0]; });
                                that.allData.vehiclePlt = undefined;
                                that.allData.vehPlatformsArray = daa;
                            }
                        }
                        let method = 'GetLOVObjectForIdeaGen';
                        let parameters = [];
                        let params1 = {
                            reqType: "SYSTEM###" + that.allData.sector
                        };
                        that.callSubCode(parameters, method, params1, 'appSysArray');
                    }
                    else if (key == "appSysArray") {
                        let a = s['SOAP:Body'][0].GetLOVObjectForIdeaGenResponse;
                        if (a != undefined) {
                            if (a[0].tuple != undefined) {
                                let b = a[0].tuple;
                                let daa = b.map((d) => { return d.old[0].LOV_MASTER_AUTO[0]; });
                                that.allData.appSys = undefined;
                                that.allData.systemArray = daa;
                            }
                        }
                        that._getDropDownData();
                    }
                    else if (key == 'savedModels') {
                        debugger;
                        let a = s['SOAP:Body'][0].GetIGPModelOnUIDResponse;
                        if (a != undefined) {
                            let obj = $.cordys.json.findObjects(a[0], "IGP_MODEL_APP");
                            console.log("check saved models obj: ", obj);
                            if (that.heroService.otoa(obj).length > 0) {
                                that._doFurtherParts(that.heroService.otoa(obj), 'Model');
                            }
                        }
                    }
                }
                else {
                    console.log("error found in err tag: ", err);
                    console.log("no response cought");
                    console.log("not getting response becz of err: ", err);
                }
            });
        });
    }
    _addPartRow(_tYpe) {
        let that = this;
        debugger;
        if (_tYpe == "ADD") {
            let temp = that.allData.partsAddArray.filter((d) => {
                return d.part_name == "";
            });
            if (temp.length == 0) {
                that.allData.partsAddArray.push({
                    index: that.allData.partsAddArray.length,
                    part_name: "",
                    part_type: "ADD"
                });
                setTimeout(() => {
                    that.rows.last.nativeElement.focus();
                }, 0);
            }
        }
        else if (_tYpe == "MOD") {
            let temp = that.allData.partsModArray.filter((d) => {
                return d.part_name == "";
            });
            if (temp.length == 0) {
                that.allData.partsModArray.push({
                    index: that.allData.partsModArray.length,
                    part_name: "",
                    part_type: "MOD"
                });
                setTimeout(() => {
                    that.rows1.last.nativeElement.focus();
                }, 0);
            }
        }
        else if (_tYpe == "Model") {
            let temp = that.allData.modelAppArray.filter((d) => {
                return d.model_name == "";
            });
            if (temp.length == 0) {
                that.allData.modelAppArray.push({
                    index: that.allData.modelAppArray.length,
                    model_name: ""
                });
                setTimeout(() => {
                    that.rows3.last.nativeElement.focus();
                }, 0);
            }
        }
        else if (_tYpe == "Field") {
            let rtml = that.allData.fieldsArray;
            rtml.push({
                field_id: "",
                field_name: "",
                value: null
            });
            that.allData.fieldsArray = rtml;
        }
    }
    _checkIfAdmin() {
        let that = this;
        var method = 'CheckUserTypeWeb';
        var parameters = [];
        let params = {
            sector: ""
        };
        that.callSubCode(parameters, method, params, 'admin');
    }
    // adminString: any;
    _doFurther(obj) {
        debugger;
        let that = this;
        // let role = obj[0]?.old?.CheckUserTypeWeb?.CheckUserTypeWeb;
        let role = obj.CheckUserTypeWeb[0];
        if ((role).includes("ADMIN")) {
            // that.adminString = role;
            let tempStr = role.split("ADMIN");
            console.log(tempStr[0]);
            if ((tempStr[0]).includes(that.allData.detailedData.IID_SECTOR)) {
                that.allData.isAdmin = true;
                that._getSectorList();
            }
            else {
                if (that.allData.detailedData.IID_STATUS == 'Saved') {
                    that.allData.isAdmin = true;
                    that._getSectorList();
                }
                else {
                    that.allData.isAdmin = false;
                    that._getSavedDocs();
                }
            }
        }
        else if (role == "MnM User" && that.allData.detailedData.IID_STATUS == 'Saved') {
            that.allData.isAdmin = true;
            that._getSectorList();
        }
        else {
            that.allData.isAdmin = false;
            that._getSavedDocs();
        }
    }
    _getSectorList() {
        let that = this;
        let params = {
            reqType: "SECTOR"
        };
        var method = 'GetLOVObjectForIdeaGen';
        var parameters = [];
        that.callSubCode(parameters, method, params, 'sector');
    }
    callthis(value) {
        debugger;
        console.log("im here: ", value);
        let that = this;
        let params = {
            reqType: "MODEL_AFFECTED###" + value
        };
        var method = 'GetLOVObjectForIdeaGen';
        var parameters = [];
        that.callSubCode(parameters, method, params, 'VehiclePltArray');
    }
    _editDetails() {
        let that = this;
        if (this.allData.showEditContainer == true) {
            this.allData.showEditContainer = false;
        }
        else {
            debugger;
            this.allData.showEditContainer = true;
            // assign all the variable values
            that.allData.ideaDesc = that.allData.detailedData.IID_IDEA_DESCRIPTION;
            that.allData.approxSavings = that.allData.detailedData.IID_APP_SAVINGS;
            that.allData.approxWeight = that.allData.detailedData.IID_APP_WT_REDUCTION;
            that.allData.ideaResource = that.allData.detailedData.IID_IDEA_RESOURCE;
            that.allData.idaCat = that.allData.detailedData.IID_IDEA_CAT;
            that.allData.otherCat = that.allData.detailedData.IID_IDEA_CAT_OTH;
            that.allData.sector = that.allData.detailedData.IID_SECTOR;
            that.allData.vehiclePlt = that.allData.detailedData.IID_VEHICLE_PLAT;
            that.allData.mobileNumber = that.allData.detailedData.IID_MOBILE_NUMBER;
            that.allData.appSys = that.allData.detailedData.IID_APP_SYSTEM;
            that.callthis(that.allData.sector);
        }
    }
    _onFieldChange(field, index, item) {
        console.log("changed field: ", field);
        let that = this;
        debugger;
        let count = 0;
        if (field == "") {
            if (item.prm_key != undefined) {
                let dataObj = {
                    tuple: {
                        old: {
                            IGP_CONFIG_FIELD: {
                                ICF_SEQ_NO: item.prm_key
                            }
                        }
                    }
                };
                // delete field
                var parameters = [];
                that.fieldsSubCode(parameters, "UpdateIgpConfigField", dataObj, "http://schemas.cordys.com/igp", '_deleteSingleField', field, index, item);
            }
            else {
                item.field_id = "";
                item.field_name = "";
                item.value = null;
            }
        }
        else {
            var valueArr = that.allData.fieldsArray.map(function (item1) { return item1.field_id; }).filter((d) => { return d != ""; });
            var isDuplicate = valueArr.some(function (item, idx) {
                return valueArr.indexOf(item) != idx;
            });
            console.log("isDuplicate: ", isDuplicate);
            if (isDuplicate == true) {
                if (that.allData.fieldsArray[index].prm_key != undefined) {
                    that.heroService._toastrErrorMsg("This field is already selected.");
                    let temp = that.allData.configValues.filter((d) => {
                        return d.LMF_DESC == item.field_name;
                    });
                    if (temp.length > 0) {
                        console.log("temp: ", temp[0]);
                        let elementId = 'row4' + index;
                        document.getElementById(elementId)["value"] = temp[0].LMF_ID;
                        that.allData.fieldsArray[index].field_id = temp[0].LMF_ID;
                    }
                    count = -1;
                }
                else {
                    that.heroService._toastrErrorMsg("This field is already selected.");
                    item.field_id = "";
                    item.field_name = "";
                    item.value = null;
                    let elementId = 'row4' + index;
                    // var e =document.getElementById(elementId)["value"];
                    document.getElementById(elementId)["value"] = "";
                }
            }
            // }
            for (let j = 0; j < that.allData.fieldsArray.length; j++) {
                /////// new code /////
                if (count != -1) {
                    if (index == j) {
                        if (that.allData.fieldsArray[j].field_name != "") {
                            if (that.allData.fieldsArray[j].prm_key != undefined) {
                                let dataObj = {
                                    tuple: {
                                        old: {
                                            IGP_CONFIG_FIELD: {
                                                ICF_SEQ_NO: that.allData.fieldsArray[j].prm_key
                                            }
                                        },
                                        new: {
                                            IGP_CONFIG_FIELD: {
                                                IID_REQUEST_ID: that.allData.reqNum,
                                                ICF_CONFIG_FIELD: that.allData.fieldsArray[j].field_id,
                                                ICF_CONFIG_VALUE: that.allData.fieldsArray[j].value
                                            }
                                        }
                                    }
                                };
                                // delete and update
                                var parameters = [];
                                that.fieldsSubCode(parameters, "UpdateIgpConfigField", dataObj, "http://schemas.cordys.com/igp", '_updateDeleteFields', field, index, item);
                            }
                        }
                    }
                }
            }
        }
        console.log("updted configValues ", that.allData.configValues);
    }
    static singleFieldDeleteParam(data) {
        let dataObj = [];
        dataObj.push({
            "old": {
                "IGP_CONFIG_FIELD": {
                    "ICF_SEQ_NO": data.tuple.old.IGP_CONFIG_FIELD.ICF_SEQ_NO
                }
            }
        });
        return dataObj;
    }
    static _updateDeleteParams(data) {
        let dataObj = [];
        dataObj.push({
            "old": {
                "IGP_CONFIG_FIELD": {
                    "ICF_SEQ_NO": data.tuple.old.IGP_CONFIG_FIELD.ICF_SEQ_NO
                }
            },
            "new": {
                "IGP_CONFIG_FIELD": {
                    "IID_REQUEST_ID": data.tuple.old.IGP_CONFIG_FIELD.IID_REQUEST_ID,
                    "ICF_CONFIG_FIELD": data.tuple.old.IGP_CONFIG_FIELD.ICF_CONFIG_FIELD,
                    "ICF_CONFIG_VALUE": data.tuple.old.IGP_CONFIG_FIELD.ICF_CONFIG_VALUE
                }
            }
        });
        return dataObj;
    }
    fieldsSubCode(parameters, method, params, namespace, key, field, index, item) {
        let that = this;
        if (key == '_deleteSingleField') {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.singleFieldDeleteParam(params);
            ;
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == '_updateDeleteFields') {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1._updateDeleteParams(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        that.loadingController.create({
            spinner: 'bubbles',
            message: 'Please wait...'
        }).then((loadEl) => {
            loadEl.present();
            that.heroService.testService1(parameters, function (err, response) {
                loadEl.dismiss();
                debugger;
                if (response) {
                    let s = response['SOAP:Envelope'];
                    let faultString = s['SOAP:Body'][0]['SOAP:Fault'];
                    if (faultString != undefined) {
                        if (faultString[0].faultstring[0]._ == "Unable to bind the artifact to a SAML assertion.") {
                            that.heroService._toastrErrorMsg("Token is expired. Please logout and login to access the app.");
                            return;
                        }
                        console.log("fault string: ", faultString[0].faultstring[0]._);
                    }
                    if (key == '_deleteSingleField') {
                        let a = s['SOAP:Body'][0].UpdateIgpConfigFieldResponse;
                        if (a != undefined) {
                            item.field_id = "";
                            item.field_name = "";
                            item.value = null;
                            that._getConfigData();
                        }
                    }
                    else if (key == '_updateDeleteFields') {
                        let a = s['SOAP:Body'][0].UpdateIgpConfigFieldResponse;
                        if (a != undefined) {
                            that._getConfigData();
                        }
                    }
                }
                else {
                    console.log("error found in err tag: ", err);
                    console.log("no response cought");
                    console.log("not getting response becz of err: ", err);
                }
            });
        });
    }
    static DeleteFileParams(data) {
        let dataObj = [];
        dataObj.push({
            "old": {
                "IGP_DOCS": {
                    "IGD_SNO": data.tuple.old.IGP_DOCS.IGD_SNO
                }
            }
        });
        return dataObj;
    }
    static DeletePartsParam(data) {
        let dataObj = [];
        dataObj.push({
            "old": {
                "IGP_PART_DETAILS": {
                    "PRT_SEQ_NO": data.tuple.old.IGP_PART_DETAILS.PRT_SEQ_NO
                }
            }
        });
        return dataObj;
    }
    static DeleteModelsParam(data) {
        let dataObj = [];
        dataObj.push({
            "old": {
                "IGP_MODEL_APP": {
                    "IMA_SEQ_NO": data.tuple.old.IGP_MODEL_APP.IMA_SEQ_NO
                }
            }
        });
        return dataObj;
    }
    _sendMail() {
        let that = this;
        let params = {
            UIDNumber: that.allData.reqNum
        };
        var method = 'IGP_EmaiToSectorHOD';
        var parameters = [];
        that.callSubCodeBasic(parameters, params, method, 'http://schemas.cordys.com/default', 'sendMail');
    }
    _getSavedDocs() {
        let that = this;
        let params = {
            UID: that.allData.reqNum
        };
        var method = 'GetIdeaDocOnUID';
        var parameters = [];
        that.callSubCode(parameters, method, params, 'savedDocs');
    }
    _getPartsData() {
        let that = this;
        let params = {
            UID: that.allData.reqNum
        };
        var method = 'GetAddedParts';
        var parameters = [];
        that.callSubCode(parameters, method, params, 'savedAddParts');
        // var method1: string = 'GetDeletedParts';
        // that.callSubCode(parameters, method1, params, 'savedModParts');
        // that.heroService.ajax("GetAddedParts", "http://schemas.cordys.com/igp", {
        //   UID: that.allData.requestNum
        // }).then((resp) => {
        //   console.log("check added parts: ", resp);
        //   let obj = $.cordys.json.findObjects(resp, "IGP_PART_DETAILS");
        //   if (obj != undefined) {
        //     
        //     that.allData.alreadySavedPartsCount = that.heroService.otoa(obj).length;
        //     that.allData.alreadySavedParts = that.heroService.otoa(obj);
        //     debugger
        //     if (that.heroService.otoa(obj).length <= that.allData.partsAddArray.length) {
        //       for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
        //         for (let i = 0; i < that.allData.partsAddArray.length; i++) {
        //           if (that.allData.partsAddArray[i].part_name == "") {
        //             that.allData.partsAddArray[i].part_name = that.heroService.otoa(obj)[j].PRT_PART_DESC;
        //             that.allData.partsAddArray[i].part_type = that.heroService.otoa(obj)[j].PRT_PART_TYPE
        //             break;
        //           }
        //         }
        //       }
        //     } else {
        //       for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
        //         if (that.allData.partsAddArray.length != that.heroService.otoa(obj).length) {
        //           that.allData.partsAddArray.push({
        //             index: that.allData.partsAddArray.length,
        //             part_name: "",
        //             part_type: "ADD"
        //           })
        //         }
        //       }
        //       if (that.allData.partsAddArray.length == that.heroService.otoa(obj).length) {
        //         for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
        //           for (let i = 0; i < that.allData.partsAddArray.length; i++) {
        //             if (that.allData.partsAddArray[i].part_name == "") {
        //               that.allData.partsAddArray[i].part_name = that.heroService.otoa(obj)[j].PRT_PART_DESC;
        //               that.allData.partsAddArray[i].part_type = that.heroService.otoa(obj)[j].PRT_PART_TYPE
        //               break;
        //             }
        //           }
        //         }
        //       }
        //     }
        //   }
        // },
        //   (err) => {
        //     that._toastrMsg("Error occured while fetching parts. Please contact administartor.");
        //   });
        // that.heroService.ajax("GetDeletedParts", "http://schemas.cordys.com/igp", {
        //   UID: that.allData.requestNum
        // }).then((resp) => {
        //   console.log("check deleted parts: ", resp);
        //   let obj = $.cordys.json.findObjects(resp, "IGP_PART_DETAILS");
        //   if (obj != undefined) {
        //     that.allData.alreadyDelPartsCount = that.heroService.otoa(obj).length;
        //     that.allData.alreadyDelParts = that.heroService.otoa(obj);
        //     if (that.heroService.otoa(obj).length <= that.allData.partsModArray.length) {
        //       for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
        //         for (let i = 0; i < that.allData.partsModArray.length; i++) {
        //           if (that.allData.partsModArray[i].part_name == "") {
        //             that.allData.partsModArray[i].part_name = that.heroService.otoa(obj)[j].PRT_PART_DESC;
        //             that.allData.partsModArray[i].part_type = that.heroService.otoa(obj)[j].PRT_PART_TYPE
        //             break;
        //           }
        //         }
        //       }
        //     } else {
        //       for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
        //         if (that.allData.partsModArray.length != that.heroService.otoa(obj).length) {
        //           that.allData.partsModArray.push({
        //             index: that.allData.partsModArray.length,
        //             part_name: "",
        //             part_type: "MOD"
        //           })
        //         }
        //       }
        //       if (that.allData.partsModArray.length == that.heroService.otoa(obj).length) {
        //         for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
        //           for (let i = 0; i < that.allData.partsModArray.length; i++) {
        //             if (that.allData.partsModArray[i].part_name == "") {
        //               that.allData.partsModArray[i].part_name = that.heroService.otoa(obj)[j].PRT_PART_DESC;
        //               that.allData.partsModArray[i].part_type = that.heroService.otoa(obj)[j].PRT_PART_TYPE
        //               break;
        //             }
        //           }
        //         }
        //       }
        //     }
        //   }
        // },
        //   (err) => {
        //     that._toastrMsg("Error occured while fetching parts. Please contact administartor.");
        //   });
    }
    _doFurtherParts(obj, key) {
        let that = this;
        ///// for added parts
        if (key == "ADD") {
            that.allData.partsAddArray = [];
            that.allData.alreadySavedPartsCount = that.heroService.otoa(obj).length;
            that.allData.alreadySavedParts = that.heroService.otoa(obj);
            debugger;
            for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
                that.allData.partsAddArray.push({
                    part_name: that.heroService.otoa(obj)[j].PRT_PART_DESC[0],
                    part_type: that.heroService.otoa(obj)[j].PRT_PART_TYPE[0],
                    prm_key: that.heroService.otoa(obj)[j].PRT_SEQ_NO[0]
                });
            }
        }
        else if (key == "MOD") {
            that.allData.partsModArray = [];
            that.allData.alreadyDelPartsCount = that.heroService.otoa(obj).length;
            that.allData.alreadyDelParts = that.heroService.otoa(obj);
            for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
                that.allData.partsModArray.push({
                    part_name: that.heroService.otoa(obj)[j].PRT_PART_DESC[0],
                    part_type: that.heroService.otoa(obj)[j].PRT_PART_TYPE[0],
                    prm_key: that.heroService.otoa(obj)[j].PRT_SEQ_NO[0]
                });
            }
        }
        else if (key == "Model") {
            that.allData.modelAppArray = [];
            that.allData.alreadySavedModelCount = that.heroService.otoa(obj).length;
            that.allData.alreadySavedModel = that.heroService.otoa(obj);
            for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
                that.allData.modelAppArray.push({
                    model_name: that.heroService.otoa(obj)[j].IMA_MODEL_APPLICABILITY[0],
                    prm_key: that.heroService.otoa(obj)[j].IMA_SEQ_NO[0]
                });
            }
        }
        else if (key == "DOCS") {
            that.allData.filesArray = [];
            that.allData.alreadySavedDocsCount = that.heroService.otoa(obj).length;
            that.allData.alreadySavedDocs = that.heroService.otoa(obj);
            for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
                that.allData.filesArray.push({
                    file_name: that.heroService.otoa(obj)[j].IGD_DOC_NAME[0],
                    file_path: that.heroService.otoa(obj)[j].IGD_DOC_PATH[0],
                    prm_key: that.heroService.otoa(obj)[j].IGD_SNO[0]
                });
            }
        }
        else if (key == "Fields") {
            debugger;
            that.allData.fieldsArray = [];
            that.allData.alreadySavedFieldsCount = that.heroService.otoa(obj).length;
            that.allData.alreadySavedFields = that.heroService.otoa(obj);
            for (let i = 0; i < that.allData.allDd.length; i++) {
                for (let j = 0; j < that.heroService.otoa(obj).length; j++) {
                    if (that.allData.allDd[i].LMF_ID[0] == that.heroService.otoa(obj)[j].ICF_CONFIG_FIELD[0]) {
                        that.allData.allDd[i].selectedFlag = true;
                        that.allData.allDd[i].ICF_CONFIG_VALUE = that.heroService.otoa(obj)[j].ICF_CONFIG_VALUE[0];
                        that.allData.allDd[i].prm_key = that.heroService.otoa(obj)[j].ICF_SEQ_NO[0];
                    }
                }
                if (that.heroService.otoa(that.allData.allDd)[i].selectedFlag == true) {
                    that.allData.fieldsArray.push({
                        field_name: that.heroService.otoa(that.allData.allDd)[i].LMF_DESC[0],
                        field_id: that.heroService.otoa(that.allData.allDd)[i].LMF_ID[0],
                        value: that.heroService.otoa(that.allData.allDd)[i].ICF_CONFIG_VALUE,
                        prm_key: that.heroService.otoa(that.allData.allDd)[i].prm_key
                    });
                }
            }
            that.allData.configValues = that.allData.allDd;
        }
    }
    _getDropDownData() {
        let that = this;
        let params = {
            sector: that.allData.detailedData.IID_SECTOR,
            LMFID: that.allData.detailedData.IID_VEHICLE_PLAT
        };
        var parameters = [];
        that.callSubCode(parameters, 'GetModelAffDesc', params, 'vehDropDw');
    }
    _getConfigData() {
        let that = this;
        var parameters = [];
        let dataObj = {};
        that.callSubCode(parameters, "GetIdeaConfig", dataObj, 'config');
        // that.callSubCodeBasic(parameters,dataObj, "GetIdeaConfig", "http://schemas.cordys.com/igp", '_getIdeaConfigData');
    }
    _getSavedFieldValues() {
        let that = this;
        let params = {
            requestID: that.allData.reqNum
        };
        var parameters = [];
        that.callSubCode(parameters, 'GetConfigOnUID', params, 'savedFields');
    }
    _getSavedModelApp() {
        let that = this;
        let params = {
            UID: that.allData.reqNum
        };
        var parameters = [];
        that.callSubCode(parameters, 'GetIGPModelOnUID', params.UID, 'savedModels');
    }
    _downloadFile(file) {
        console.log("check file object: ", file);
        var filPath = "http://43.242.214.148:81/home/devmahindra/" + file.file_path.split("shared/")[1];
        // var filPath = window.location.href.split("/com")[0] + "/" + file.file_path.split("shared/")[1];
        // http://43.242.214.148:81/home/devmahindra/MAHINDRA_UPLOADS/IGP/Doc_Uploads/Screenshot_20210716-183934_Idea App.jpg
        console.log("check file path: ", filPath);
        var dnldFile;
        dnldFile = document.createElement("A");
        dnldFile.href = filPath;
        dnldFile.download = filPath.substr(filPath.lastIndexOf('/') + 1).replace(/^.*[\\\/]/, "");
        console.log("check substracted file path: ", filPath.substr(filPath.lastIndexOf('/') + 1).replace(/^.*[\\\/]/, ""));
        console.log("dnldFile: ", dnldFile);
        document.body.appendChild(dnldFile);
        dnldFile.click();
        document.body.removeChild(dnldFile);
    }
    _toastrMsg(msg) {
        this.heroService._toastrMsg(msg);
    }
    _insertInPartsMaster(obj) {
        let that = this;
        if (that.allData.alreadySavedPartsCount > 0) {
            for (let i = 0; i < that.allData.partsAddArray.length; i++) {
                for (let j = 0; j < that.allData.alreadySavedParts.length; j++) {
                    if (that.allData.partsAddArray[i].part_name == that.allData.alreadySavedParts[j].PRT_PART_DESC[0]) {
                        that.allData.partsAddArray[i].prm_key = that.allData.alreadySavedParts[j].PRT_SEQ_NO[0];
                        break;
                    }
                }
            }
        }
        //////////// insert data in part master table
        let dataObj3 = [];
        let temp = that.allData.partsAddArray.filter((d) => {
            return d.part_name != "";
        });
        if (temp.length > 0) {
            for (let i = 0; i < temp.length; i++) {
                if (temp[i].prm_key == undefined) {
                    dataObj3.push({
                        new: {
                            IGP_PART_DETAILS: {
                                IID_REQUEST_ID: obj,
                                PRT_PART_DESC: temp[i].part_name,
                                PRT_PART_TYPE: temp[i].part_type
                            }
                        }
                    });
                }
                else {
                    dataObj3.push({
                        old: {
                            IGP_PART_DETAILS: {
                                PRT_SEQ_NO: temp[i].prm_key
                            }
                        },
                        new: {
                            IGP_PART_DETAILS: {
                                PRT_SEQ_NO: temp[i].prm_key,
                                IID_REQUEST_ID: obj,
                                PRT_PART_DESC: temp[i].part_name,
                                PRT_PART_TYPE: temp[i].part_type
                            }
                        }
                    });
                }
            }
        }
        if (that.allData.alreadyDelPartsCount > 0) {
            for (let i = 0; i < that.allData.partsModArray.length; i++) {
                for (let j = 0; j < that.allData.alreadyDelParts.length; j++) {
                    if (that.allData.partsModArray[i].part_name == that.allData.alreadyDelParts[j].PRT_PART_DESC[0]) {
                        //   tempPartsArray1.push(that.allData.partsModArray[i]);
                        that.allData.partsModArray[i].prm_key = that.allData.alreadyDelParts[j].PRT_SEQ_NO[0];
                        break;
                    }
                }
            }
        }
        let temp1 = that.allData.partsModArray.filter((d) => {
            return d.part_name != "";
        });
        if (temp1.length > 0) {
            for (let i = 0; i < temp1.length; i++) {
                if (temp1[i].prm_key == undefined) {
                    dataObj3.push({
                        new: {
                            IGP_PART_DETAILS: {
                                IID_REQUEST_ID: obj,
                                PRT_PART_DESC: temp1[i].part_name,
                                PRT_PART_TYPE: temp1[i].part_type
                            }
                        }
                    });
                }
                else {
                    dataObj3.push({
                        old: {
                            IGP_PART_DETAILS: {
                                PRT_SEQ_NO: temp1[i].prm_key
                            }
                        },
                        new: {
                            IGP_PART_DETAILS: {
                                PRT_SEQ_NO: temp1[i].prm_key,
                                IID_REQUEST_ID: obj,
                                PRT_PART_DESC: temp1[i].part_name,
                                PRT_PART_TYPE: temp1[i].part_type
                            }
                        }
                    });
                }
            }
        }
        if (dataObj3.length > 0) {
            var parameters = [];
            that.callSubCodeBasic(parameters, dataObj3, "UpdateIgpPartDetails", 'http://schemas.cordys.com/igp', 'insertParts');
        }
        else {
            that._insertInDocs(obj);
        }
    }
    _insertInModelApp(obj) {
        let that = this;
        if (that.allData.alreadySavedModelCount > 0) {
            for (let i = 0; i < that.allData.modelAppArray.length; i++) {
                for (let j = 0; j < that.allData.alreadySavedModel.length; j++) {
                    if (that.allData.modelAppArray[i].model_name == that.allData.alreadySavedModel[j].IMA_MODEL_APPLICABILITY[0]) {
                        that.allData.modelAppArray[i].prm_key = that.allData.alreadySavedModel[j].IMA_SEQ_NO[0];
                        break;
                    }
                }
            }
        }
        let dataObj3 = [];
        let temp = that.allData.modelAppArray.filter((d) => {
            return d.model_name != "";
        });
        if (temp.length > 0) {
            for (let i = 0; i < temp.length; i++) {
                if (temp[i].prm_key == undefined) {
                    dataObj3.push({
                        new: {
                            IGP_MODEL_APP: {
                                IID_REQUEST_ID: obj,
                                IMA_MODEL_APPLICABILITY: temp[i].model_name
                            }
                        }
                    });
                }
                else {
                    dataObj3.push({
                        old: {
                            IGP_MODEL_APP: {
                                IMA_SEQ_NO: temp[i].prm_key
                            }
                        },
                        new: {
                            IGP_MODEL_APP: {
                                IMA_SEQ_NO: temp[i].prm_key,
                                IID_REQUEST_ID: obj,
                                IMA_MODEL_APPLICABILITY: temp[i].model_name
                            }
                        }
                    });
                }
            }
        }
        if (dataObj3.length > 0) {
            var parameters = [];
            that.callSubCodeBasic(parameters, dataObj3, "UpdateIgpModelApp", 'http://schemas.cordys.com/igp', 'insertModels');
        }
        else {
            // that._insertInDocs(obj, funcKey);
            that._insertInfields(obj);
        }
    }
    _insertInDocs(obj) {
        let that = this;
        //////////// Insert data in docs table
        let temp11;
        let temp_file_name = "", temp_file_path = "";
        if (that.allData.alreadySavedDocsCount > 0) {
            let tempArray = [];
            for (let i = 0; i < that.allData.filesArray.length; i++) {
                if (that.allData.filesArray[i].file_path.split(":")[1] == undefined) {
                    tempArray.push(that.allData.filesArray[i]);
                }
            }
            temp11 = tempArray.filter((d) => {
                return d.file_name != "";
            });
            if (temp11.length > 0) {
                for (let i = 0; i < temp11.length; i++) {
                    temp_file_name += temp11[i].file_name + "###";
                    temp_file_path += temp11[i].file_path + "###";
                }
                var parameters = [];
                let dataObj = {
                    FileName: temp_file_name,
                    FileContent: temp_file_path,
                    requestID: obj
                };
                that.callSubCodeBasic(parameters, dataObj, "UploadDocument", 'http://schemas.cordys.com/igp', 'insertDocs');
            }
            else {
                that._insertInModelApp(obj);
            }
        }
        else {
            temp11 = that.allData.filesArray.filter((d) => {
                return d.file_name != "";
            });
            if (temp11.length > 0) {
                for (let i = 0; i < temp11.length; i++) {
                    temp_file_name += temp11[i].file_name + "###";
                    temp_file_path += temp11[i].file_path + "###";
                }
                var parameters = [];
                let dataObj = {
                    FileName: temp_file_name,
                    FileContent: temp_file_path,
                    requestID: obj
                };
                that.callSubCodeBasic(parameters, dataObj, "UploadDocument", 'http://schemas.cordys.com/igp', 'insertDocs');
            }
            else {
                that._insertInModelApp(obj);
            }
        }
    }
    _insertInfields(obj) {
        let that = this;
        /////// Insert data in fields config table
        debugger;
        if (that.allData.alreadySavedFieldsCount > 0) {
            for (let i = 0; i < that.allData.fieldsArray.length; i++) {
                for (let j = 0; j < that.allData.alreadySavedFields.length; j++) {
                    if (that.allData.fieldsArray[i].field_id == that.allData.alreadySavedFields[j].ICF_CONFIG_FIELD[0]) {
                        that.allData.fieldsArray[i].prm_key = that.allData.alreadySavedFields[j].ICF_SEQ_NO[0];
                        break;
                    }
                }
            }
        }
        let jss = that.allData.fieldsArray.filter((d) => {
            return (d.field_id != "" && d.value != null);
        });
        if (jss.length > 0) {
            let somedata = [];
            for (let i = 0; i < jss.length; i++) {
                if (jss[i].prm_key == undefined) {
                    somedata.push({
                        new: {
                            IGP_CONFIG_FIELD: {
                                IID_REQUEST_ID: obj,
                                ICF_CONFIG_FIELD: jss[i].field_id,
                                ICF_CONFIG_VALUE: jss[i].value
                            }
                        }
                    });
                }
                else {
                    somedata.push({
                        old: {
                            IGP_CONFIG_FIELD: {
                                ICF_SEQ_NO: jss[i].prm_key
                            }
                        },
                        new: {
                            IGP_CONFIG_FIELD: {
                                ICF_SEQ_NO: jss[i].prm_key,
                                IID_REQUEST_ID: obj,
                                ICF_CONFIG_FIELD: jss[i].field_id,
                                ICF_CONFIG_VALUE: jss[i].value
                            }
                        }
                    });
                }
            }
            if (somedata.length > 0) {
                var parameters = [];
                that.callSubCodeBasic(parameters, somedata, "UpdateIgpConfigField", 'http://schemas.cordys.com/igp', 'inserFields');
            }
        }
        else {
            // if (funcKey == 'Submit') {
            that._toastrMsg("Data Submitted Successfully.");
            that._sendMail();
            // } 
            // else if (funcKey == 'Save') {
            //   that._getPartsData(that.allData.reqNum);
            //   that._toastrMsg("Data Saved Successfully.");
            // }
        }
    }
    _deletePart(type, index) {
        let that = this;
        if (type == 'ADD') {
            for (let i = 0; i < that.allData.partsAddArray.length; i++) {
                if (index == i) {
                    if (that.allData.partsAddArray[i].prm_key != undefined) {
                        that._deletePartsService(that.allData.partsAddArray[i].prm_key);
                    }
                    else {
                        if (that.allData.partsAddArray.length > 1) {
                            that.allData.partsAddArray.splice(i, 1);
                        }
                        else {
                            that.allData.partsAddArray[i].part_name = '';
                            that.allData.partsAddArray[i].part_type = "ADD";
                        }
                    }
                }
            }
        }
        else if (type == 'MOD') {
            for (let i = 0; i < that.allData.partsModArray.length; i++) {
                if (index == i) {
                    if (that.allData.partsModArray[i].prm_key != undefined) {
                        that._deletePartsService(that.allData.partsModArray[i].prm_key);
                    }
                    else {
                        if (that.allData.partsModArray.length > 1) {
                            that.allData.partsModArray.splice(i, 1);
                        }
                        else {
                            that.allData.partsModArray[i].part_name = '';
                            that.allData.partsModArray[i].part_type = "MOD";
                        }
                    }
                }
            }
        }
        else if (type == 'Model') {
            for (let i = 0; i < that.allData.modelAppArray.length; i++) {
                if (index == i) {
                    if (that.allData.modelAppArray[i].prm_key != undefined) {
                        that._deleteModelService(that.allData.modelAppArray[i].prm_key);
                    }
                    else {
                        if (that.allData.modelAppArray.length > 1) {
                            that.allData.modelAppArray.splice(i, 1);
                        }
                        else {
                            that.allData.modelAppArray[i].model_name = '';
                        }
                    }
                }
            }
        }
    }
    _deletePartsService(prm_key) {
        let that = this;
        let dataObj3 = {
            tuple: {
                old: {
                    IGP_PART_DETAILS: {
                        PRT_SEQ_NO: prm_key
                    }
                }
            }
        };
        var parameters = [];
        that.callSubCodeBasic(parameters, dataObj3, "UpdateIgpPartDetails", 'http://schemas.cordys.com/igp', 'deleteParts');
    }
    _deleteModelService(prm_key) {
        let that = this;
        let dataObj3 = {
            tuple: {
                old: {
                    IGP_MODEL_APP: {
                        IMA_SEQ_NO: prm_key
                    }
                }
            }
        };
        var parameters = [];
        that.callSubCodeBasic(parameters, dataObj3, "UpdateIgpModelApp", 'http://schemas.cordys.com/igp', 'deleteModels');
    }
    _deleteFile(index) {
        let that = this;
        for (let i = 0; i < that.allData.filesArray.length; i++) {
            if (index == i) {
                // that.allData.filesArray.splice(i, 1);
                if (that.allData.filesArray[i].prm_key != undefined) {
                    that._deleteFileService(that.allData.filesArray[i].prm_key);
                }
                else {
                    that.allData.filesArray.splice(i, 1);
                }
            }
        }
    }
    _deleteFileService(prm_key) {
        let that = this;
        let dataObj3 = {
            tuple: {
                old: {
                    IGP_DOCS: {
                        IGD_SNO: prm_key
                    }
                }
            }
        };
        var parameters = [];
        that.callSubCodeBasic(parameters, dataObj3, "UpdateIgpDocs", 'http://schemas.cordys.com/igp', 'deleteFiles');
    }
    _submit() {
        let that = this;
        debugger;
        if (that.allData.ideaDesc == '' || that.allData.ideaDesc == undefined) {
            that.heroService._toastrErrorMsg("Please Enter IDEA Description field to proceed further.");
        }
        else if (that.allData.sector == '' || that.allData.sector == undefined) {
            that.heroService._toastrErrorMsg("Please Select IDEA Sector field to proceed further.");
        }
        else if (that.allData.vehiclePlt == '' || that.allData.vehiclePlt == undefined) {
            that.heroService._toastrErrorMsg("Please Select Vehicle Platform field to proceed further.");
        }
        else if (that.allData.mobileNumber == '' || that.allData.mobileNumber == undefined) {
            that.heroService._toastrErrorMsg("Please Select Mobile Number to proceed further.");
        }
        else if (String(that.allData.mobileNumber).length < 10) {
            that.heroService._toastrErrorMsg("Please Enter Valid Mobile Number to proceed further.");
        }
        else {
            let dataObj = {};
            dataObj = {
                tuple: {
                    old: {
                        IGP_IDEA_DETAILS: {
                            IID_REQUEST_ID: that.allData.reqNum
                        }
                    },
                    new: {
                        IGP_IDEA_DETAILS: {
                            IID_IDEA_DESCRIPTION: that.allData.ideaDesc,
                            IID_APP_SAVINGS: (that.allData.approxSavings ? that.allData.approxSavings : ""),
                            IID_IDEA_RESOURCE: (that.allData.ideaResource ? that.allData.ideaResource : ""),
                            IID_APP_WT_REDUCTION: (that.allData.approxWeight ? that.allData.approxWeight : ""),
                            IID_IDEA_CAT: (that.allData.idaCat ? that.allData.idaCat : ""),
                            IID_VEHICLE_PLAT: (that.allData.vehiclePlt ? that.allData.vehiclePlt : ""),
                            IID_MOBILE_NUMBER: (that.allData.mobileNumber ? that.allData.mobileNumber : ""),
                            IID_APP_SYSTEM: (that.allData.appSys ? that.allData.appSys : ""),
                            // IID_IDEA_GEN_TOKEN_NO: that.allData.userDetails.UM_USER_ID[0],
                            // IID_IDEA_GEN_NAME: that.allData.userDetails.UM_USER_NAME[0],
                            // IID_IDEA_GEN_EMAIL: that.allData.userDetails.UM_USER_EMAIL[0],
                            // IID_IDEA_DATE: moment(new Date()).format(),
                            IID_STATUS: "Submitted",
                            IID_IS_ACTIVE: "A",
                            // IID_IDEA_TITLE: that.allData.ideaTitle,
                            IID_SECTOR: (that.allData.sector ? that.allData.sector : ""),
                            IID_IDEA_CAT_OTH: (that.allData.otherCat ? that.allData.otherCat : "")
                        }
                    }
                }
            };
            var parameters = [];
            that.callSubCodeNew(parameters, dataObj);
        }
    }
    callSubCodeNew(parameters, params) {
        let that = this;
        parameters["UpdateIgpIdeaDetails xmlns='http://schemas.cordys.com/igp'"] = IdeaDetailsPage_1.saveParam(params);
        parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        that.loadingController.create({
            message: "Please wait...",
            spinner: 'bubbles'
        }).then((loadEl) => {
            loadEl.present();
            that.heroService.testService1(parameters, function (err, response) {
                loadEl.dismiss();
                if (response) {
                    var s = response['SOAP:Envelope'];
                    if (s['SOAP:Body'][0]["SOAP:Fault"] != undefined) {
                        if (s['SOAP:Body'][0]["SOAP:Fault"][0].faultstring[0]._ == "Error occurred while processing the request. Error from database server or driver.ORA-01438: value larger than specified precision allowed for this column\n.") {
                            that.heroService._toastrErrorMsg("Error occured while submitting data. Please provide (14+2) digits for Approx. Savings (RS./Veh).");
                        }
                        else {
                            that.heroService._toastrErrorMsg("Error occured while submitting data. Please contact administrator.");
                        }
                    }
                    else {
                        var a = s['SOAP:Body'][0].UpdateIgpIdeaDetailsResponse;
                        var b = a[0].tuple;
                        if (a == undefined || b == undefined) {
                            that._toastrMsg("Error occured while saving data. Please contact administrator.");
                        }
                        else {
                            debugger;
                            console.log("check submitted data: " + b);
                            if (b[0].new[0].IGP_IDEA_DETAILS[0].IID_REQUEST_ID[0] != undefined) {
                                that.allData.reqNum = b[0].new[0].IGP_IDEA_DETAILS[0].IID_REQUEST_ID[0];
                                that._insertInPartsMaster(that.allData.reqNum);
                            }
                        }
                    }
                }
                else {
                    console.log("error found in err tag: ", err);
                    console.log("no response cought");
                    console.log("not getting response becz of err: ", err);
                }
            });
        });
    }
    callSubCodeBasic(parameters, params, method, namespace, key) {
        let that = this;
        if (key == 'insertParts') {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.PartsParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == 'sendMail') {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.UIDParam1(params.UIDNumber, 'sendMail');
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "insertDocs") {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.DocsParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getAddedParts") {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.UIDParam1(params.UID, '_getAddedParts');
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getDeletedParts") {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.UIDParam1(params.UID, '_getDeletedParts');
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getIdeaDocOnUID") {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.UIDParam1(params.UID, '_getIdeaDocOnUID');
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "deleteFiles") {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.DeleteFileParams(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "insertModels") {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.modelParams(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getSavedModels") {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.UIDParam1(params.UID, '_getSavedModels');
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getIdeaConfigData") {
            debugger;
            parameters[method + " xmlns='" + namespace + "'"] = {};
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getIdeaConfig1Data") {
            debugger;
            parameters[method + " xmlns='" + namespace + "'"] = {};
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "_getSavedFieldData") {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.requestIDparam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "inserFields") {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.fieldsDataparam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "deleteParts") {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.DeletePartsParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        else if (key == "deleteModels") {
            parameters[method + " xmlns='" + namespace + "'"] = IdeaDetailsPage_1.DeleteModelsParam(params);
            parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
        }
        that.loadingController.create({
            spinner: 'bubbles',
            message: 'Please wait...'
        }).then((loadEl) => {
            loadEl.present();
            that.heroService.testService1(parameters, function (err, response) {
                loadEl.dismiss();
                if (response) {
                    var s = response['SOAP:Envelope'];
                    var a;
                    var b;
                    if (key == 'sendMail') {
                        a = s['SOAP:Body'][0].IGP_EmaiToSectorHODResponse;
                        if (a != undefined) {
                            that._toastrMsg("E-mail sSent Successfully.");
                            // that.allData.showEditContainer= false;
                            // that._checkIfAdmin();
                            that.router.navigateByUrl("/idea/menu/tabs/tab2");
                        }
                        else {
                            that.heroService._toastrErrorMsg("Error occured while sending e-mail. Please contact administrator.");
                        }
                    }
                    else {
                        if (key == 'insertParts') {
                            a = s['SOAP:Body'][0].UpdateIgpPartDetailsResponse;
                            if (a != undefined) {
                                // that._insertInModelApp(that.allData.reqNum, funcKey);
                                that._insertInDocs(that.allData.reqNum);
                            }
                            else {
                                // that._insertInModelApp(that.allData.reqNum, funcKey);
                                that._insertInDocs(that.allData.reqNum);
                                that.heroService._toastrErrorMsg("Error occured while saving parts. Please contact administrator.");
                            }
                        }
                        else {
                            if (key == "insertDocs") {
                                a = s['SOAP:Body'][0].UploadDocumentResponse;
                                if (a != undefined) {
                                    that._insertInModelApp(that.allData.reqNum);
                                }
                                else {
                                    that._insertInModelApp(that.allData.reqNum);
                                    that._toastrMsg("Error occured while saving files. Please contact administrator.");
                                }
                            }
                            else if (key == "_getAddedParts") {
                                a = s['SOAP:Body'][0].GetAddedPartsResponse;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "IGP_PART_DETAILS");
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that._doFurtherParts(that.heroService.otoa(obj), 'ADD');
                                    }
                                }
                                var parameters = [];
                                let dataObj = {
                                    UID: that.allData.reqNum
                                };
                                that.callSubCodeBasic(parameters, dataObj, "GetDeletedParts", "http://schemas.cordys.com/igp", '_getDeletedParts');
                            }
                            else if (key == "_getDeletedParts") {
                                a = s['SOAP:Body'][0].GetDeletedPartsResponse;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "IGP_PART_DETAILS");
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that._doFurtherParts(that.heroService.otoa(obj), 'MOD');
                                    }
                                }
                                that._getSavedModelApp();
                            }
                            else if (key == "_getIdeaDocOnUID") {
                                a = s['SOAP:Body'][0].GetIdeaDocOnUIDResponse;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "IGP_DOCS");
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that._doFurtherParts(that.heroService.otoa(obj), 'DOCS');
                                    }
                                }
                            }
                            else if (key == "deleteFiles") {
                                a = s['SOAP:Body'][0].UpdateIgpDocsResponse;
                                if (a != undefined) {
                                    that._toastrMsg("File Deleted Successfully.");
                                    that._getSavedDocs();
                                }
                            }
                            else if (key == "insertModels") {
                                debugger;
                                a = s['SOAP:Body'][0].UpdateIgpModelAppResponse;
                                if (a != undefined) {
                                    // that._insertInDocs(that.allData.reqNum, funcKey);
                                    that._insertInfields(that.allData.reqNum);
                                }
                                else {
                                    that._insertInfields(that.allData.reqNum);
                                    that._toastrMsg("Error occured while saving model applicabilities. Please contact administrator.");
                                    // that._insertInDocs(that.allData.reqNum, funcKey);
                                }
                            }
                            else if (key == "_getSavedModels") {
                                a = s['SOAP:Body'][0].GetIGPModelOnUIDResponse;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "IGP_MODEL_APP");
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that._doFurtherParts(that.heroService.otoa(obj), 'Model');
                                    }
                                }
                                that._getSavedDocs();
                            }
                            else if (key == "_getIdeaConfigData") {
                                a = s['SOAP:Body'][0].GetIdeaConfigResponse;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "LOV_MASTER_AUTO");
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that.allData.configValues = that.heroService.otoa(obj);
                                    }
                                }
                                that._getSectorList();
                            }
                            else if (key == "_getIdeaConfig1Data") {
                                a = s['SOAP:Body'][0].GetIdeaConfigResponse;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "LOV_MASTER_AUTO");
                                    // if (obj != undefined)
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that.allData.allDd = [];
                                        that.allData.allDd = that.heroService.otoa(obj);
                                        that._getSavedFieldValues();
                                    }
                                }
                            }
                            else if (key == "_getSavedFieldData") {
                                a = s['SOAP:Body'][0].GetConfigOnUIDResponse;
                                debugger;
                                if (a != undefined) {
                                    let obj = $.cordys.json.findObjects(a[0], "IGP_CONFIG_FIELD");
                                    if (that.heroService.otoa(obj).length > 0) {
                                        that._doFurtherParts(that.heroService.otoa(obj), 'Fields');
                                    }
                                }
                                that._getConfigData();
                            }
                            else if (key == "inserFields") {
                                debugger;
                                a = s['SOAP:Body'][0].UpdateIgpConfigFieldResponse;
                                if (a != undefined) {
                                    // if (funcKey == 'Submit') {
                                    that._toastrMsg("Data Submitted Successfully.");
                                    that._sendMail();
                                    // } else if (funcKey == 'Save') {
                                    //   that._getPartsData(that.allData.reqNum);
                                    //   that._toastrMsg("Data Saved Successfully.");
                                    // }
                                }
                                else {
                                    // if (funcKey == 'Submit') {
                                    that._toastrMsg("Data Submitted Successfully.");
                                    that._sendMail();
                                    // } else if (funcKey == 'Save') {
                                    //   that._getPartsData(that.allData.reqNum);
                                    //   that._toastrMsg("Data Saved Successfully.");
                                    // }
                                    that.heroService._toastrErrorMsg("Error occured while saving fields. Please contact administrator.");
                                }
                            }
                            else if (key == "deleteParts") {
                                a = s['SOAP:Body'][0].UpdateIgpPartDetailsResponse;
                                if (a != undefined) {
                                    that._toastrMsg("Part Deleted Successfully.");
                                    that._getPartsData();
                                }
                                else {
                                    that.heroService._toastrErrorMsg("Error occured while deleting part. Please contact administrator.");
                                }
                            }
                            else if (key == "deleteModels") {
                                a = s['SOAP:Body'][0].UpdateIgpModelAppResponse;
                                if (a != undefined) {
                                    that._toastrMsg("Model Deleted Successfully.");
                                    that._getSavedModelApp();
                                }
                                else {
                                    that.heroService._toastrErrorMsg("Error occured while deleting model. Please contact administrator.");
                                }
                            }
                        }
                    }
                }
                else {
                    console.log("error found in err tag: ", err);
                    console.log("no response cought");
                    console.log("not getting response becz of err: ", err);
                }
            });
        });
    }
    static saveParam(data) {
        let dataObj;
        dataObj = {
            "tuple": {
                "old": {
                    "IGP_IDEA_DETAILS": {
                        "IID_REQUEST_ID": data.tuple.old.IGP_IDEA_DETAILS.IID_REQUEST_ID
                    }
                },
                "new": {
                    "IGP_IDEA_DETAILS": {
                        "IID_IDEA_DESCRIPTION": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_DESCRIPTION,
                        "IID_APP_SAVINGS": data.tuple.new.IGP_IDEA_DETAILS.IID_APP_SAVINGS,
                        "IID_APP_WT_REDUCTION": data.tuple.new.IGP_IDEA_DETAILS.IID_APP_WT_REDUCTION,
                        "IID_IDEA_RESOURCE": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_RESOURCE,
                        "IID_IDEA_CAT": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_CAT,
                        "IID_VEHICLE_PLAT": data.tuple.new.IGP_IDEA_DETAILS.IID_VEHICLE_PLAT,
                        "IID_MOBILE_NUMBER": data.tuple.new.IGP_IDEA_DETAILS.IID_MOBILE_NUMBER,
                        "IID_APP_SYSTEM": data.tuple.new.IGP_IDEA_DETAILS.IID_APP_SYSTEM,
                        // "IID_IDEA_GEN_TOKEN_NO": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_GEN_TOKEN_NO,
                        // "IID_IDEA_GEN_NAME": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_GEN_NAME,
                        // "IID_IDEA_GEN_EMAIL": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_GEN_EMAIL,
                        // "IID_IDEA_DATE": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_DATE,
                        "IID_STATUS": data.tuple.new.IGP_IDEA_DETAILS.IID_STATUS,
                        "IID_IS_ACTIVE": data.tuple.new.IGP_IDEA_DETAILS.IID_IS_ACTIVE,
                        // "IID_IDEA_TITLE": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_TITLE,
                        "IID_SECTOR": data.tuple.new.IGP_IDEA_DETAILS.IID_SECTOR,
                        "IID_IDEA_CAT_OTH": data.tuple.new.IGP_IDEA_DETAILS.IID_IDEA_CAT_OTH
                    }
                }
            }
        };
        // }
        return dataObj;
        // return parameters;
    }
    static fieldsDataparam(data) {
        let dataObj = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].old != undefined) {
                dataObj.push({
                    "old": {
                        "IGP_CONFIG_FIELD": {
                            "ICF_SEQ_NO": data[i].old.IGP_CONFIG_FIELD.ICF_SEQ_NO
                        }
                    },
                    "new": {
                        "IGP_CONFIG_FIELD": {
                            "ICF_SEQ_NO": data[i].new.IGP_CONFIG_FIELD.ICF_SEQ_NO,
                            "IID_REQUEST_ID": data[i].new.IGP_CONFIG_FIELD.IID_REQUEST_ID,
                            "ICF_CONFIG_FIELD": data[i].new.IGP_CONFIG_FIELD.ICF_CONFIG_FIELD,
                            "ICF_CONFIG_VALUE": data[i].new.IGP_CONFIG_FIELD.ICF_CONFIG_VALUE
                        }
                    }
                });
            }
            else {
                dataObj.push({
                    "new": {
                        "IGP_CONFIG_FIELD": {
                            "IID_REQUEST_ID": data[i].new.IGP_CONFIG_FIELD.IID_REQUEST_ID,
                            "ICF_CONFIG_FIELD": data[i].new.IGP_CONFIG_FIELD.ICF_CONFIG_FIELD,
                            "ICF_CONFIG_VALUE": data[i].new.IGP_CONFIG_FIELD.ICF_CONFIG_VALUE
                        }
                    }
                });
            }
        }
        return dataObj;
    }
    static PartsParam(data) {
        let dataObj = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].old != undefined) {
                dataObj.push({
                    "old": {
                        "IGP_PART_DETAILS": {
                            "PRT_SEQ_NO": data[i].old.IGP_PART_DETAILS.PRT_SEQ_NO
                        }
                    },
                    "new": {
                        "IGP_PART_DETAILS": {
                            "PRT_SEQ_NO": data[i].new.IGP_PART_DETAILS.PRT_SEQ_NO,
                            "IID_REQUEST_ID": data[i].new.IGP_PART_DETAILS.IID_REQUEST_ID,
                            "PRT_PART_DESC": data[i].new.IGP_PART_DETAILS.PRT_PART_DESC,
                            "PRT_PART_TYPE": data[i].new.IGP_PART_DETAILS.PRT_PART_TYPE
                        }
                    }
                });
            }
            else {
                dataObj.push({
                    "new": {
                        "IGP_PART_DETAILS": {
                            "IID_REQUEST_ID": data[i].new.IGP_PART_DETAILS.IID_REQUEST_ID,
                            "PRT_PART_DESC": data[i].new.IGP_PART_DETAILS.PRT_PART_DESC,
                            "PRT_PART_TYPE": data[i].new.IGP_PART_DETAILS.PRT_PART_TYPE
                        }
                    }
                });
            }
        }
        return dataObj;
    }
    static DocsParam(data) {
        var parameters = [];
        parameters["FileName"] = data.FileName;
        parameters["FileContent"] = data.FileContent;
        parameters["requestID"] = data.requestID;
        return parameters;
    }
    static modelParams(data) {
        let dataObj = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].old != undefined) {
                dataObj.push({
                    "old": {
                        "IGP_MODEL_APP": {
                            "IMA_SEQ_NO": data[i].old.IGP_MODEL_APP.IMA_SEQ_NO
                        }
                    },
                    "new": {
                        "IGP_MODEL_APP": {
                            "IMA_SEQ_NO": data[i].new.IGP_MODEL_APP.IMA_SEQ_NO,
                            "IID_REQUEST_ID": data[i].new.IGP_MODEL_APP.IID_REQUEST_ID,
                            "IMA_MODEL_APPLICABILITY": data[i].new.IGP_MODEL_APP.IMA_MODEL_APPLICABILITY
                        }
                    }
                });
            }
            else {
                dataObj.push({
                    "new": {
                        "IGP_MODEL_APP": {
                            "IID_REQUEST_ID": data[i].new.IGP_MODEL_APP.IID_REQUEST_ID,
                            "IMA_MODEL_APPLICABILITY": data[i].new.IGP_MODEL_APP.IMA_MODEL_APPLICABILITY
                        }
                    }
                });
            }
        }
        ;
        let temp;
        temp = {
            dataObj
        };
        return dataObj;
    }
    static UIDParam1(uidNum, key) {
        var parameters = [];
        if (key == "sendMail") {
            parameters["UIDNumber"] = uidNum;
        }
        else {
            if (key == "_getAddedParts" || key == "_getIdeaDocOnUID" || key == "_getDeletedParts" || key == "_getSavedModels") {
                parameters["UID"] = uidNum;
            }
        }
        return parameters;
    }
    static requestIDparam(data) {
        var parameters = [];
        parameters["requestID"] = data.requestID;
        return parameters;
    }
};
IdeaDetailsPage.ctorParameters = () => [
    { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_2__.DataService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.LoadingController },
    { type: src_app_services_hero_service__WEBPACK_IMPORTED_MODULE_3__.HeroService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router }
];
IdeaDetailsPage.propDecorators = {
    rows: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ViewChildren, args: ["row",] }],
    rows1: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ViewChildren, args: ["row1",] }],
    rows3: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ViewChildren, args: ["row3",] }]
};
IdeaDetailsPage = IdeaDetailsPage_1 = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-idea-details',
        template: _raw_loader_idea_details_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_idea_details_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], IdeaDetailsPage);



/***/ }),

/***/ 28030:
/*!***********************************************************!*\
  !*** ./src/app/pages/idea-details/idea-details.page.scss ***!
  \***********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".h2Style {\n  color: #2c5364;\n  font-weight: 500;\n  font-size: 18px;\n  padding-bottom: 5px;\n}\n\n.pStyle {\n  color: black;\n  font-size: 16px;\n}\n\n.form-group {\n  padding: 8px;\n  border: 2px solid #b2c0c6;\n  border-radius: 5px;\n  margin: 8px;\n  width: 100%;\n}\n\n.form-group > label {\n  position: absolute;\n  top: -1px;\n  left: 20px;\n  background-color: white;\n  padding-left: 5px;\n  padding-right: 5px;\n  color: #2c5364;\n  font-size: 18px;\n  font-weight: 500;\n}\n\n.form-group > input {\n  border: none;\n}\n\n.form-group > textarea {\n  border: none;\n}\n\nion-row {\n  width: 100%;\n}\n\nion-item {\n  margin-right: -5px;\n}\n\nion-select {\n  max-width: 100%;\n  width: 100%;\n}\n\n.form-control[readonly] {\n  background-color: #ffffff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImlkZWEtZGV0YWlscy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxjQUFBO0VBQWdCLGdCQUFBO0VBQWlCLGVBQUE7RUFBZ0IsbUJBQUE7QUFJckQ7O0FBREE7RUFDSSxZQUFBO0VBQWMsZUFBQTtBQUtsQjs7QUFGQTtFQUNJLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7QUFLSjs7QUFIQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSx1QkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBTUo7O0FBSEE7RUFDSSxZQUFBO0FBTUo7O0FBSkE7RUFDSSxZQUFBO0FBT0o7O0FBTEE7RUFDSSxXQUFBO0FBUUo7O0FBTkE7RUFDSSxrQkFBQTtBQVNKOztBQVBBO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUFVSjs7QUFQQTtFQUNJLHlCQUFBO0FBVUoiLCJmaWxlIjoiaWRlYS1kZXRhaWxzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oMlN0eWxlIHtcbiAgICBjb2xvcjogIzJjNTM2NDsgZm9udC13ZWlnaHQ6IDUwMDtmb250LXNpemU6IDE4cHg7cGFkZGluZy1ib3R0b206IDVweDtcbn1cblxuLnBTdHlsZSB7XG4gICAgY29sb3I6IGJsYWNrOyBmb250LXNpemU6IDE2cHg7XG59XG5cbi5mb3JtLWdyb3VwIHtcbiAgICBwYWRkaW5nOiA4cHg7XG4gICAgYm9yZGVyOiAycHggc29saWQgI2IyYzBjNjtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgbWFyZ2luOiA4cHg7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4uZm9ybS1ncm91cCA+IGxhYmVsIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAtMXB4O1xuICAgIGxlZnQ6IDIwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gICAgcGFkZGluZy1yaWdodDogNXB4O1xuICAgIGNvbG9yOiAjMmM1MzY0O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xufVxuXG4uZm9ybS1ncm91cCA+IGlucHV0IHtcbiAgICBib3JkZXI6IG5vbmU7XG59XG4uZm9ybS1ncm91cCA+IHRleHRhcmVhIHtcbiAgICBib3JkZXI6IG5vbmU7XG59XG5pb24tcm93e1xuICAgIHdpZHRoOiAxMDAlO1xufVxuaW9uLWl0ZW17XG4gICAgbWFyZ2luLXJpZ2h0OiAtNXB4O1xufVxuaW9uLXNlbGVjdCB7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIC8vIG1hcmdpbi1sZWZ0OiAzMyU7XG59XG4uZm9ybS1jb250cm9sW3JlYWRvbmx5XSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICAvLyBvcGFjaXR5OiAxO1xufSJdfQ== */");

/***/ }),

/***/ 21995:
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/idea-details/idea-details.page.html ***!
  \*************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<input id=\"uploadBtnEdit\"\n  accept=\"image/*, .pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, .csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel\"\n  type=\"file\" multiple [(ngModel)]=\"fileTextValueFinal\" [ngModelOptions]=\"{standalone: true} \"\n  (change)=\"_fileBrowseHandlerFinal($event.target)\" style=\"position:absolute; top:-100px;\">\n<ion-header [translucent]=\"true\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/idea/menu/tabs/tab2\"></ion-back-button>\n    </ion-buttons>\n    <ion-title mode=\"md\">\n      {{allData.reqNum}}\n      <!-- IDEA Title -->\n    </ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button (click)=\"_showInfo()\">\n        <ion-icon slot=\"icon-only\" name=\"information-circle-outline\"></ion-icon>\n      </ion-button>\n      <ng-container *ngIf=\"allData.showEditContainer == false\">\n        <ion-button (click)=\"_editDetails()\" *ngIf=\"allData.isAdmin == true\">\n          <ion-icon slot=\"icon-only\" name=\"create-outline\"></ion-icon>\n        </ion-button>\n      </ng-container>\n      <ion-button (click)=\"_editDetails()\" *ngIf=\"allData.showEditContainer == true\">\n        <ion-icon slot=\"icon-only\" name=\"pencil\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n  <ion-toolbar class=\"bar-light bar-subheader\" mode=\"ios\" *ngIf=\"allData.showEditContainer == true\">\n    <ion-segment [(ngModel)]=\"section\" (ionChange)=\"segmentChanged($event)\" mode=\"ios\">\n      <ion-segment-button value=\"desc\">\n        <ion-label>Description</ion-label>\n      </ion-segment-button>\n      <ion-segment-button value=\"add\">\n        <ion-label>Add File & Parts</ion-label>\n      </ion-segment-button>\n      <ion-segment-button value=\"idea\">\n        <ion-label>IDEA Details</ion-label>\n      </ion-segment-button>\n    </ion-segment>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-no-padding\">\n  <ion-list lines=\"none\" *ngIf=\"allData.showEditContainer == false\">\n    <ion-item>\n      <ion-label>\n        <h2 class=\"h2Style\">IDEA Description</h2>\n        <p class=\"pStyle\">{{allData.detailedData.IID_IDEA_DESCRIPTION}}</p>\n      </ion-label>\n    </ion-item>\n    <ion-item style=\"padding-bottom: 8px;\">\n      <ion-label>\n        <h2 class=\"h2Style\">Approx. Savings (RS./Veh)</h2>\n        <p class=\"pStyle\">\n          {{allData.detailedData.IID_APP_SAVINGS ? allData.detailedData.IID_APP_SAVINGS : 'N/A'}}\n        </p>\n      </ion-label>\n    </ion-item>\n    <ion-item class=\"ion-no-padding\">\n      <div class=\"form-group\">\n        <label>Attach Files</label>\n        <ion-list lines=\"inset\" style=\"padding: 0px;\">\n          <ng-container *ngIf=\"allData.filesArray.length > 0\">\n            <ion-item *ngFor=\"let file of allData.filesArray; let i=index;\" style=\"padding: 0px;\">\n              <ion-label>{{file.file_name}}</ion-label>\n              <!-- <ion-icon slot=\"primary\" name=\"cloud-download-outline\"></ion-icon> -->\n              <ion-button *ngIf=\"file.prm_key != undefined\" class=\"ion-no-padding\" size=\"small\" fill=\"clear\"\n                (click)=\"_downloadFile(file)\">\n                <ion-icon slot=\"icon-only\" name=\"cloud-download-outline\"></ion-icon>\n              </ion-button>\n            </ion-item>\n          </ng-container>\n          <ng-container *ngIf=\"allData.filesArray.length == 0\">\n            <ion-item style=\"padding: 0px;\">\n              <!-- <ion-label>N/A</ion-label> -->\n              <ion-label>&nbsp;</ion-label>\n\n            </ion-item>\n          </ng-container>\n        </ion-list>\n      </div>\n    </ion-item>\n    <ion-item class=\"ion-no-padding\">\n      <div class=\"form-group\">\n        <label>Add Part Number</label>\n        <ion-list lines=\"inset\" style=\"padding: 0px;\">\n          <ng-container *ngIf=\"allData.partsAddArray.length > 0\">\n            <ion-item *ngFor=\"let part of allData.partsAddArray\" style=\"padding: 0px;\">\n              <ion-label>{{part.part_name}}</ion-label>\n            </ion-item>\n          </ng-container>\n          <ng-container *ngIf=\"allData.partsAddArray.length == 0\">\n            <ion-item style=\"padding: 0px;\">\n              <!-- <ion-label>N/A</ion-label> -->\n              <ion-label>&nbsp;</ion-label>\n            </ion-item>\n          </ng-container>\n        </ion-list>\n      </div>\n    </ion-item>\n    <ion-item class=\"ion-no-padding\">\n      <div class=\"form-group\">\n        <label>Delete Part Number</label>\n        <ion-list lines=\"inset\" style=\"padding: 0px;\">\n          <ng-container *ngIf=\"allData.partsModArray.length > 0\">\n            <ion-item *ngFor=\"let part of allData.partsModArray\" style=\"padding: 0px;\">\n              <ion-label>{{part.part_name}}</ion-label>\n            </ion-item>\n          </ng-container>\n          <ng-container *ngIf=\"allData.partsModArray.length == 0\">\n            <ion-item style=\"padding: 0px;\">\n              <!-- <ion-label>N/A</ion-label> -->\n              <ion-label>&nbsp;</ion-label>\n            </ion-item>\n          </ng-container>\n        </ion-list>\n      </div>\n    </ion-item>\n    <ion-item class=\"ion-no-padding\">\n      <div class=\"form-group\">\n        <label>Model Applicability</label>\n        <ion-list lines=\"inset\" style=\"padding: 0px;\">\n          <ng-container *ngIf=\"allData.modelAppArray.length > 0\">\n            <ion-item *ngFor=\"let model of allData.modelAppArray\" style=\"padding: 0px;\">\n              <ion-label>{{model.model_name}}</ion-label>\n            </ion-item>\n          </ng-container>\n          <ng-container *ngIf=\"allData.modelAppArray.length == 0\">\n            <ion-item style=\"padding: 0px;\">\n              <!-- <ion-label>N/A</ion-label> -->\n              <ion-label>&nbsp;</ion-label>\n            </ion-item>\n          </ng-container>\n        </ion-list>\n      </div>\n    </ion-item>\n    <ion-item class=\"ion-no-padding\">\n      <div class=\"form-group\">\n        <label>Additional Fields</label>\n        <ion-list lines=\"inset\" class=\"ion-no-padding\">\n          <ng-container *ngIf=\"allData.fieldsArray.length > 0\">\n            <ion-item *ngFor=\"let field of allData.fieldsArray; let i = index;\" class=\"ion-no-padding\">\n              <ion-row>\n                <ion-col size=\"6\">\n                  <select class=\"form-select form-select-sm\" #row4 [id]=\"'row4'+i\" aria-label=\".form-select-sm example\"\n                    [(ngModel)]=\"field.field_id\" [ngModelOptions]=\"{standalone: true}\" disabled>\n                    <option selected value=\"\">Select Field</option>\n                    <option *ngFor=\"let item of allData.configValues;\" [value]=\"item.LMF_ID\">{{item.LMF_DESC}}\n                    </option>\n                  </select>\n                </ion-col>\n                <ion-col size=\"6\">\n                  <input class=\"form-control form-control-sm\" type=\"text\" [value]=\"field.value\" id=\"field.value\"\n                    [(ngModel)]=\"field.value\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Enter Value\" disabled>\n                </ion-col>\n              </ion-row>\n            </ion-item>\n          </ng-container>\n          <ng-container *ngIf=\"allData.fieldsArray.length == 0\">\n            <ion-item style=\"padding: 0px;\">\n              <!-- <ion-label>N/A</ion-label> -->\n              <ion-label>&nbsp;</ion-label>\n            </ion-item>\n          </ng-container>\n        </ion-list>\n      </div>\n    </ion-item>\n    <ion-item>\n      <ion-label>\n        <h2 class=\"h2Style\">IDEA Resource</h2>\n        <p class=\"pStyle\">\n          {{allData.detailedData.IID_IDEA_RESOURCE ? allData.detailedData.IID_IDEA_RESOURCE : 'N/A'}}</p>\n      </ion-label>\n    </ion-item>\n    <ion-item style=\"padding-bottom: 8px;\">\n      <ion-label>\n        <h2 class=\"h2Style\">Approx. Weight Reduction</h2>\n        <p class=\"pStyle\">\n          {{allData.detailedData.IID_APP_WT_REDUCTION ? allData.detailedData.IID_APP_WT_REDUCTION : 'N/A'}}</p>\n      </ion-label>\n    </ion-item>\n\n    <ion-item class=\"ion-no-padding\">\n      <ion-row>\n        <ion-col size=\"6\" class=\"ion-no-padding\">\n          <ion-item>\n            <ion-label>\n              <h2 class=\"h2Style\">IDEA Category</h2>\n              <p class=\"pStyle\">\n                {{allData.detailedData.IGPCAT ? allData.detailedData.IGPCAT : 'N/A'}}</p>\n            </ion-label>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"6\" class=\"ion-no-padding\">\n          <ion-item>\n            <ion-label>\n              <h2 class=\"h2Style\">If Category is Others</h2>\n              <p class=\"pStyle\">\n                {{allData.detailedData.IID_IDEA_CAT_OTH ? allData.detailedData.IID_IDEA_CAT_OTH : 'N/A'}}</p>\n            </ion-label>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n    <ion-item class=\"ion-no-padding\">\n      <ion-row>\n        <ion-col size=\"6\" class=\"ion-no-padding\">\n          <ion-item>\n            <ion-label>\n              <h2 class=\"h2Style\">Vehicle Platform</h2>\n              <p class=\"pStyle\">{{allData.detailedData.VECHPLATF}}</p>\n            </ion-label>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"6\" class=\"ion-no-padding\">\n          <ion-item>\n            <ion-label>\n              <h2 class=\"h2Style\">Applicable System</h2>\n              <p class=\"pStyle\">\n                {{allData.detailedData.APPLSYS ? allData.detailedData.APPLSYS : 'N/A'}}</p>\n            </ion-label>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n\n    <ion-item>\n      <ion-label>\n        <h2 class=\"h2Style\">Mobile Number</h2>\n        <p class=\"pStyle\">\n          {{allData.detailedData.IID_MOBILE_NUMBER ? allData.detailedData.IID_MOBILE_NUMBER : 'N/A'}}</p>\n      </ion-label>\n    </ion-item>\n    <hr>\n    <ion-item>\n      <ion-label>\n        <h2 class=\"h2Style\">IDEA Generator Token</h2>\n        <p class=\"pStyle\">{{allData.detailedData.IID_IDEA_GEN_TOKEN_NO}}</p>\n      </ion-label>\n    </ion-item>\n    <ion-item>\n      <ion-label>\n        <h2 class=\"h2Style\">IDEA Generator Name</h2>\n        <p class=\"pStyle\">{{allData.detailedData.IID_IDEA_GEN_NAME}}</p>\n      </ion-label>\n    </ion-item>\n    <ion-item>\n      <ion-label>\n        <h2 class=\"h2Style\">IDEA Generator E-mail</h2>\n        <p class=\"pStyle\">{{allData.detailedData.IID_IDEA_GEN_EMAIL}}</p>\n      </ion-label>\n    </ion-item>\n    <ion-item class=\"ion-no-padding\">\n      <ion-row>\n        <ion-col size=\"6\" class=\"ion-no-padding\">\n          <ion-item>\n            <ion-label>\n              <h2 class=\"h2Style\">IDEA Date</h2>\n              <p class=\"pStyle\">{{allData.detailedData.IID_CREATED_ON | date:'mediumDate'}}</p>\n            </ion-label>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"6\" class=\"ion-no-padding ion-text-right\">\n          <ion-item>\n            <ion-label>\n              <h2 class=\"h2Style\">VECP Number</h2>\n              <p class=\"pStyle\">\n                {{allData.detailedData.VECPNUMBER ? allData.detailedData.VECPNUMBER : 'N/A'}}</p>\n            </ion-label>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list>\n\n\n  <!--Editable Form-->\n  <div [ngSwitch]=\"section\" *ngIf=\"allData.showEditContainer == true\">\n    <form style=\"padding-top: 20px;\" *ngSwitchCase=\"'desc'\">\n      <ion-list lines=\"none\">\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Request Number</label>\n            <input type=\"text\" class=\"form-control input-lg\" [(ngModel)]=\"allData.reqNum\" name=\"reqNum\" disabled />\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>IDEA Description<span style=\"color: red;\">*</span></label>\n            <textarea rows=\"3\" type=\"text\" class=\"form-control input-lg\" [(ngModel)]=\"allData.ideaDesc\"\n              name=\"ideaDesc\"></textarea>\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Approx. Savings (RS./Veh)</label>\n            <input type=\"number\" class=\"form-control input-lg\" [(ngModel)]=\"allData.approxSavings\"\n              name=\"approxSavings\" />\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Approx. Weight Reduction</label>\n            <input type=\"text\" class=\"form-control input-lg\" [(ngModel)]=\"allData.approxWeight\" name=\"approxWeight\" />\n          </div>\n        </ion-item>\n\n      </ion-list>\n    </form>\n    <form style=\"padding-top: 20px;\" *ngSwitchCase=\"'add'\">\n      <ion-list lines=\"none\">\n\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Attach Files</label>\n            <ion-list lines=\"inset\" class=\"ion-no-padding\">\n              <ng-container *ngIf=\"allData.filesArray.length > 0\">\n                <ion-item *ngFor=\"let file of allData.filesArray; let i = index;\">\n                  <ion-row>\n                    <ion-col size=\"10\" class=\"ion-no-padding\">\n                      <ion-label>{{file.file_name}}</ion-label>\n                    </ion-col>\n                    <ion-col size=\"1\" class=\"ion-no-padding\">\n                      <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_deleteFile(i)\">\n                        <ion-icon slot=\"icon-only\" name=\"close-outline\" style=\"color: red;\"></ion-icon>\n                      </ion-button>\n                    </ion-col>\n                    <ion-col size=\"1\" class=\"ion-no-padding\">\n                      <ion-button *ngIf=\"file.prm_key != undefined\" class=\"ion-no-padding\" size=\"small\" fill=\"clear\"\n                        (click)=\"_downloadFile(file)\">\n                        <ion-icon slot=\"icon-only\" name=\"cloud-download-outline\"></ion-icon>\n                      </ion-button>\n                    </ion-col>\n                  </ion-row>\n                </ion-item>\n              </ng-container>\n              <ion-item class=\"ion-no-padding\">\n                <input type=\"text\" class=\"form-control input-lg\" placeholder=\"Attach Files\"\n                  style=\"border: none; box-shadow: none;\" readonly />\n                <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_openModalXlsx()\">\n                  <ion-icon slot=\"icon-only\" name=\"cloud-upload-outline\"></ion-icon>\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Add Part Number</label>\n            <ion-list lines=\"inset\" style=\"padding: 0px;\">\n              <ion-item *ngFor=\"let part of allData.partsAddArray; let i = index;\" style=\"padding: 0px;\">\n                <input type=\"text\" #row class=\"form-control input-lg\" placeholder=\"Enter New Part Number & Description\"\n                  style=\"border: none; box-shadow: none; padding: 0px;\" [(ngModel)]=\"part.part_name\"\n                  [ngModelOptions]=\"{standalone: true}\" />\n                <ion-button class=\"ion-no-padding\" *ngIf=\"part.part_name != ''\" size=\"small\" fill=\"clear\"\n                  (click)=\"_deletePart('ADD', i)\">\n                  <ion-icon slot=\"icon-only\" name=\"close-outline\" style=\"color: red;\"></ion-icon>\n                </ion-button>\n                <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_addPartRow('ADD')\">\n                  <ion-icon slot=\"icon-only\" name=\"add-outline\"></ion-icon>\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item>\n        <!-- <ion-item>\n          <div class=\"form-group\">\n            <label>Add Part Number</label>\n            <ion-list lines=\"inset\" style=\"padding: 0px;\">\n              <ng-container *ngIf=\"allData.partsAddArray.length > 0\">\n                <ion-item *ngFor=\"let part of allData.partsAddArray; let i = index;\" style=\"padding: 0px;\">\n                  <ion-label>{{part.part_name}}</ion-label>\n                  <ion-button size=\"small\" fill=\"clear\" (click)=\"_deletePart('ADD', i)\">\n                    <ion-icon slot=\"icon-only\" name=\"close-outline\" style=\"color: red;\"></ion-icon>\n                  </ion-button>\n                </ion-item>\n              </ng-container>\n              <ion-item style=\"padding: 0px;\">\n                <input type=\"text\" class=\"form-control input-lg\" placeholder=\"Enter New Part Number & Description\"\n                  style=\"border: none; box-shadow: none;\" [(ngModel)]=\"addPartName\" name=\"addPartName\" />\n                <ion-button size=\"small\" fill=\"clear\" (click)=\"_addPartRow('ADD')\">\n                  <ion-icon slot=\"icon-only\" name=\"add-outline\"></ion-icon>\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item> -->\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Delete Part Number</label>\n            <ion-list lines=\"inset\" style=\"padding: 0px;\">\n              <ion-item *ngFor=\"let part of allData.partsModArray; let i = index;\" style=\"padding: 0px;\">\n                <input type=\"text\" #row1 class=\"form-control input-lg\" placeholder=\"Enter New Part Number & Description\"\n                  style=\"border: none; box-shadow: none;padding: 0px;\" [(ngModel)]=\"part.part_name\"\n                  [ngModelOptions]=\"{standalone: true}\" />\n                <ion-button *ngIf=\"part.part_name != ''\" class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_deletePart('MOD', i)\">\n                  <ion-icon slot=\"icon-only\" name=\"close-outline\" style=\"color: red;\"></ion-icon>\n                </ion-button>\n                <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_addPartRow('MOD')\">\n                  <ion-icon slot=\"icon-only\" name=\"add-outline\"></ion-icon>\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item>\n        <!-- <ion-item>\n          <div class=\"form-group\">\n            <label>Delete Part Number</label>\n            <ion-list lines=\"inset\" style=\"padding: 0px;\">\n              <ng-container *ngIf=\"allData.partsModArray.length > 0\">\n                <ion-item *ngFor=\"let part of allData.partsModArray; let i = index;\" style=\"padding: 0px;\">\n                  <ion-label>{{part.part_name}}</ion-label>\n                  <ion-button size=\"small\" fill=\"clear\" (click)=\"_deletePart('MOD', i)\">\n                    <ion-icon slot=\"icon-only\" name=\"close-outline\" style=\"color: red;\"></ion-icon>\n                  </ion-button>\n                </ion-item>\n              </ng-container>\n              <ion-item style=\"padding: 0px;\">\n                <input type=\"text\" class=\"form-control input-lg\" placeholder=\"Enter New Part Number & Description\"\n                  style=\"border: none; box-shadow: none;\" [(ngModel)]=\"modPartName\" name=\"modPartName\" />\n                <ion-button size=\"small\" fill=\"clear\" (click)=\"_addPartRow('MOD')\">\n                  <ion-icon slot=\"icon-only\" name=\"add-outline\"></ion-icon>\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item> -->\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Add Model Applicability</label>\n            <ion-list lines=\"inset\" style=\"padding: 0px;\">\n              <ion-item *ngFor=\"let model of allData.modelAppArray; let i = index;\" style=\"padding: 0px;\">\n                <input type=\"text\" #row3 class=\"form-control input-lg\" placeholder=\"Add Model\"\n                  style=\"border: none; box-shadow: none;padding: 0px;\" [(ngModel)]=\"model.model_name\"\n                  [ngModelOptions]=\"{standalone: true}\" />\n                <ion-button  class=\"ion-no-padding\" *ngIf=\"model.model_name != ''\" size=\"small\" fill=\"clear\" (click)=\"_deletePart('Model', i)\">\n                  <ion-icon slot=\"icon-only\" name=\"close-outline\" style=\"color: red;\"></ion-icon>\n                </ion-button>\n                <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_addPartRow('Model')\">\n                  <ion-icon slot=\"icon-only\" name=\"add-outline\"></ion-icon>\n                </ion-button>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Additional Fields</label>\n            <ion-list lines=\"inset\" class=\"ion-no-padding\">\n              <ion-item *ngFor=\"let field of allData.fieldsArray; let i = index;\" class=\"ion-no-padding\">\n                <ion-row>\n                  <ion-col size=\"6\">\n                    <select class=\"form-select form-select-sm\" #row4 [id]=\"'row4'+i\"\n                      aria-label=\".form-select-sm example\" [(ngModel)]=\"field.field_id\"\n                      [ngModelOptions]=\"{standalone: true}\" (change)=\"_onFieldChange(field.field_id, i, field)\"\n                      style=\" box-shadow: none;\">\n                      <option selected value=\"\">Select Field</option>\n                      <option *ngFor=\"let item of allData.configValues;\" [value]=\"item.LMF_ID\">{{item.LMF_DESC}}\n                      </option>\n                    </select>\n                  </ion-col>\n                  <ion-col size=\"5\">\n                    <input class=\"form-control form-control-sm\" type=\"text\" [value]=\"field.value\" id=\"field.value\"\n                      [(ngModel)]=\"field.value\" [ngModelOptions]=\"{standalone: true}\" placeholder=\"Enter Value\"\n                      style=\" box-shadow: none;\">\n                  </ion-col>\n                  <ion-col size=\"1\">\n                    <ion-button class=\"ion-no-padding\" size=\"small\" fill=\"clear\" (click)=\"_addPartRow('Field')\">\n                      <ion-icon slot=\"icon-only\" name=\"add-outline\"></ion-icon>\n                    </ion-button>\n                  </ion-col>\n                </ion-row>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-item>\n      </ion-list>\n    </form>\n    <form style=\"padding-top: 20px;\" *ngSwitchCase=\"'idea'\">\n      <ion-list lines=\"none\">\n        <ion-item>\n          <div class=\"form-group\">\n            <label>IDEA Resource</label>\n            <input type=\"text\" class=\"form-control input-lg\" placeholder=\"Enter IDEA Resource\"\n              [(ngModel)]=\"allData.ideaResource\" name=\"ideaResource\" />\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>IDEA Category</label>\n            <ion-select mode=\"ios\" interface=\"popover\" placeholder=\"Select IDEA Category\" [(ngModel)]=\"allData.idaCat\"\n              name=\"idaCat\" (ionChange)=\"onChangeCat(allData.idaCat)\">\n              <ion-select-option *ngFor=\"let item of allData.ideacategoryArray\" value=\"{{item.LMF_ID}}\">\n                {{item.LMF_DESC}}</ion-select-option>\n            </ion-select>\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>If Category is Other</label>\n            <input type=\"text\" class=\"form-control input-lg\" placeholder=\"Enter If Category is Others\"\n              [(ngModel)]=\"allData.otherCat\" name=\"otherCat\" [disabled]=\"!allData.catBtn\" />\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Sector<span style=\"color: red;\">*</span></label>\n            <ion-select mode=\"ios\" interface=\"popover\" placeholder=\"Select Sector\" [(ngModel)]=\"allData.sector\"\n              name=\"sector\" (ionChange)=\"callthis(allData.sector)\">\n              <ion-select-option *ngFor=\"let item of allData.sectorsArray\" value=\"{{item.LMF_DESC}}\">{{item.LMF_DESC}}\n              </ion-select-option>\n            </ion-select>\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Vehicle Platform<span style=\"color: red;\">*</span></label>\n            <ion-select mode=\"ios\" interface=\"popover\" [(ngModel)]=\"allData.vehiclePlt\" name=\"vehiclePlt\"\n              [disabled]=\"allData.sector.length == 0\" placeholder=\"Select Vehicle Platform\">\n              <ion-select-option *ngFor=\"let item of allData.vehPlatformsArray\" value=\"{{item.LMF_ID}}\">\n                {{item.LMF_DESC}}</ion-select-option>\n            </ion-select>\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Mobile Number<span style=\"color: red;\">*</span></label>\n            <input type=\"number\" class=\"form-control input-lg\" placeholder=\"Enter Mobile Number\"\n              [(ngModel)]=\"allData.mobileNumber\" name=\"mobileNumber\"\n              onKeyPress=\"if(this.value.length==10) return false;\"\n              (keyup)=\"allData.mobileNumber = mobValidation(allData.mobileNumber)\" autocomplete=\"off\" />\n          </div>\n        </ion-item>\n        <ion-item>\n          <div class=\"form-group\">\n            <label>Applicable System</label>\n            <ion-select mode=\"ios\" interface=\"popover\" [(ngModel)]=\"allData.appSys\" name=\"appSys\"\n              [disabled]=\"allData.sector.length == 0\" placeholder=\"Select Applicable System\">\n              <ion-select-option *ngFor=\"let item of allData.systemArray\" value=\"{{item.LMF_ID}}\">{{item.LMF_DESC}}\n              </ion-select-option>\n            </ion-select>\n          </div>\n        </ion-item>\n        <br />\n        <br />\n        <br />\n      </ion-list>\n    </form>\n  </div>\n</ion-content>\n<ion-footer class=\"ion-no-border\" mode=\"ios\" *ngIf=\"allData.showEditContainer == true\">\n  <ion-toolbar color=\"primary\">\n    <ion-row class=\"ion-no-padding\">\n      <ion-col class=\"ion-no-padding ion-text-center\" style=\"border-left: 1px solid #fff;\">\n        <ion-button size=\"small\" (click)=\"_submit()\" [disabled]=\"\n        (allData.ideaDesc == undefined || allData.ideaDesc == '') ||\n          (allData.mobileNumber == undefined || allData.mobileNumber == '') ||\n          (allData.sector == undefined || allData.sector == '') || \n            (allData.vehiclePlt == undefined || allData.vehiclePlt == '')\">SUBMIT</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_idea-details_idea-details_module_ts-es2015.js.map