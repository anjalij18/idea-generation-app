(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkIdeaApp"] = self["webpackChunkIdeaApp"] || []).push([["src_app_tabs_tabs_module_ts"], {
    /***/
    80530:
    /*!*********************************************!*\
      !*** ./src/app/tabs/tabs-routing.module.ts ***!
      \*********************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "TabsPageRoutingModule": function TabsPageRoutingModule() {
          return (
            /* binding */
            _TabsPageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _tabs_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./tabs.page */
      7942);

      var routes = [{
        path: 'tabs',
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_0__.TabsPage,
        children: [{
          path: 'tab1',
          loadChildren: function loadChildren() {
            return Promise.all(
            /*! import() */
            [__webpack_require__.e("common"), __webpack_require__.e("src_app_tab1_tab1_module_ts")]).then(__webpack_require__.bind(__webpack_require__,
            /*! ../tab1/tab1.module */
            2168)).then(function (m) {
              return m.Tab1PageModule;
            });
          }
        }, {
          path: 'tab2',
          loadChildren: function loadChildren() {
            return Promise.all(
            /*! import() */
            [__webpack_require__.e("common"), __webpack_require__.e("src_app_tab2_tab2_module_ts")]).then(__webpack_require__.bind(__webpack_require__,
            /*! ../tab2/tab2.module */
            14608)).then(function (m) {
              return m.Tab2PageModule;
            });
          }
        }, {
          path: '',
          redirectTo: 'tabs/tab1',
          pathMatch: 'full'
        }]
      }, {
        path: '',
        redirectTo: 'tabs/tab1',
        pathMatch: 'full'
      }];

      var _TabsPageRoutingModule = function TabsPageRoutingModule() {
        _classCallCheck(this, TabsPageRoutingModule);
      };

      _TabsPageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)]
      })], _TabsPageRoutingModule);
      /***/
    },

    /***/
    15564:
    /*!*************************************!*\
      !*** ./src/app/tabs/tabs.module.ts ***!
      \*************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "TabsPageModule": function TabsPageModule() {
          return (
            /* binding */
            _TabsPageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      38583);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _tabs_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./tabs-routing.module */
      80530);
      /* harmony import */


      var _tabs_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./tabs.page */
      7942);

      var _TabsPageModule = function TabsPageModule() {
        _classCallCheck(this, TabsPageModule);
      };

      _TabsPageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__.IonicModule, _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule, _tabs_routing_module__WEBPACK_IMPORTED_MODULE_0__.TabsPageRoutingModule],
        declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_1__.TabsPage]
      })], _TabsPageModule);
      /***/
    },

    /***/
    7942:
    /*!***********************************!*\
      !*** ./src/app/tabs/tabs.page.ts ***!
      \***********************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "TabsPage": function TabsPage() {
          return (
            /* binding */
            _TabsPage
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _raw_loader_tabs_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./tabs.page.html */
      97665);
      /* harmony import */


      var _tabs_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./tabs.page.scss */
      24427);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _services_hero_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../services/hero.service */
      19405);

      var TabsPage_1;

      var _TabsPage = TabsPage_1 = /*#__PURE__*/function () {
        function TabsPage(menuCtrl, router, heroService, loadingController) {
          _classCallCheck(this, TabsPage);

          this.menuCtrl = menuCtrl;
          this.router = router;
          this.heroService = heroService;
          this.loadingController = loadingController;
          this.showSearchtab = true;

          this._getRoles();
        }

        _createClass(TabsPage, [{
          key: "openSideMenu",
          value: function openSideMenu() {
            this.menuCtrl.open();
          }
        }, {
          key: "gets",
          value: function gets(tab) {
            debugger;

            if ("/tabs/" + tab.getSelected() != this.router.url) {
              this.router.navigateByUrl("idea/menu/tabs/" + tab.getSelected());
            }
          }
        }, {
          key: "_getRoles",
          value: function _getRoles() {
            var method = 'GetRoles';
            var parameters = [];
            this.callSubCode12(parameters, method, '', 'roles');
          }
        }, {
          key: "_doFurtherWithRoles",
          value: function _doFurtherWithRoles(obj) {
            var that = this;
            debugger;

            if (obj != undefined) {
              console.log("check roles123: ", obj);
              var mnmCount = that.heroService.otoa(obj[0].role).filter(function (d) {
                return d.description == "IGP_MnM";
              });
              console.log("check mnmCount: ", mnmCount);
              var supplierCount = that.heroService.otoa(obj[0].role).filter(function (d) {
                return d.description == "IGP_Supplier";
              });
              console.log("check supplierCount: ", supplierCount);

              if (mnmCount.length > 0) {
                that.showSearchtab = true;
              } else if (supplierCount.length > 0) {
                that.showSearchtab = false;
              }
            }
          }
        }, {
          key: "callSubCode12",
          value: function callSubCode12(parameters, method, params, key) {
            var that = this;

            if (key == "roles") {
              parameters[method + " xmlns='http://schemas.cordys.com/1.0/ldap'"] = TabsPage_1.rolesParam();
              parameters = {
                "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": {
                  "SOAP:Body": parameters
                }
              };
            }

            that.loadingController.create({
              spinner: 'bubbles',
              message: 'Please wait...'
            }).then(function (loadEl) {
              loadEl.present();
              that.heroService.testService1(parameters, function (err, response) {
                loadEl.dismiss();

                if (response) {
                  var s = response['SOAP:Envelope'];
                  var faultString = s['SOAP:Body'][0]['SOAP:Fault'];

                  if (faultString != undefined) {
                    if (faultString[0].faultstring[0]._ == "Unable to bind the artifact to a SAML assertion.") {
                      that.heroService._toastrErrorMsg("Token is expired. Please logout and login to access the app.");

                      return;
                    }

                    console.log("fault string: ", faultString[0].faultstring[0]._);
                  }

                  debugger;

                  if (key == "roles") {
                    var a = s['SOAP:Body'][0].GetRolesResponse;

                    if (a != undefined) {
                      console.log("check roles: ", a[0]);
                      var obj = $.cordys.json.findObjects(a[0], "user");

                      if (obj != undefined) {
                        that._doFurtherWithRoles(obj);
                      }
                    } else {
                      that.heroService._toastrErrorMsg("Error occured while fetching roles data. Please contact administrator.");
                    }
                  }
                } else {
                  console.log("error found in err tag: ", err);
                  console.log("no response cought");
                  console.log("not getting response becz of err: ", err);
                }
              });
            });
          }
        }], [{
          key: "rolesParam",
          value: function rolesParam() {
            var parameters = [];
            parameters['dn'] = "";
            return parameters;
          }
        }]);

        return TabsPage;
      }();

      _TabsPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.MenuController
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router
        }, {
          type: _services_hero_service__WEBPACK_IMPORTED_MODULE_2__.HeroService
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.LoadingController
        }];
      };

      _TabsPage = TabsPage_1 = (0, tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-tabs',
        template: _raw_loader_tabs_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_tabs_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _TabsPage);
      /***/
    },

    /***/
    24427:
    /*!*************************************!*\
      !*** ./src/app/tabs/tabs.page.scss ***!
      \*************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0YWJzLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    97665:
    /*!***************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tabs/tabs.page.html ***!
      \***************************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-tabs #tabs (ionTabsWillChange)=\"gets(tabs)\">\n\n  <ion-tab-bar slot=\"bottom\" color=\"secondary\">\n    <ion-tab-button tab=\"tab1\">\n      <ion-icon name=\"create-outline\" style=\"font-weight: bold;\"></ion-icon>\n      <ion-label style=\"font-weight: bold;\">Create IDEA</ion-label>\n    </ion-tab-button>\n    <ion-tab-button tab=\"tab2\" (click)=\"openSideMenu()\" *ngIf=\"showSearchtab == true\">\n      <ion-icon name=\"search-outline\" style=\"font-weight: bold;\"></ion-icon>\n      <ion-label style=\"font-weight: bold;\">Search</ion-label>\n    </ion-tab-button>\n  </ion-tab-bar>\n\n</ion-tabs>\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_tabs_tabs_module_ts-es5.js.map