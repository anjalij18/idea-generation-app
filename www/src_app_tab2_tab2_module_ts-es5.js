(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkIdeaApp"] = self["webpackChunkIdeaApp"] || []).push([["src_app_tab2_tab2_module_ts"], {
    /***/
    93092:
    /*!*********************************************!*\
      !*** ./src/app/tab2/tab2-routing.module.ts ***!
      \*********************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "Tab2PageRoutingModule": function Tab2PageRoutingModule() {
          return (
            /* binding */
            _Tab2PageRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _tab2_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./tab2.page */
      70442);

      var routes = [{
        path: '',
        component: _tab2_page__WEBPACK_IMPORTED_MODULE_0__.Tab2Page
      }];

      var _Tab2PageRoutingModule = function Tab2PageRoutingModule() {
        _classCallCheck(this, Tab2PageRoutingModule);
      };

      _Tab2PageRoutingModule = (0, tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
      })], _Tab2PageRoutingModule);
      /***/
    },

    /***/
    14608:
    /*!*************************************!*\
      !*** ./src/app/tab2/tab2.module.ts ***!
      \*************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "Tab2PageModule": function Tab2PageModule() {
          return (
            /* binding */
            _Tab2PageModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      80476);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common */
      38583);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/forms */
      3679);
      /* harmony import */


      var _tab2_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./tab2.page */
      70442);
      /* harmony import */


      var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../explore-container/explore-container.module */
      581);
      /* harmony import */


      var _tab2_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./tab2-routing.module */
      93092);

      var _Tab2PageModule = function Tab2PageModule() {
        _classCallCheck(this, Tab2PageModule);
      };

      _Tab2PageModule = (0, tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule, _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule, _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_1__.ExploreContainerComponentModule, _tab2_routing_module__WEBPACK_IMPORTED_MODULE_2__.Tab2PageRoutingModule],
        declarations: [_tab2_page__WEBPACK_IMPORTED_MODULE_0__.Tab2Page]
      })], _Tab2PageModule);
      /***/
    },

    /***/
    70442:
    /*!***********************************!*\
      !*** ./src/app/tab2/tab2.page.ts ***!
      \***********************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "Tab2Page": function Tab2Page() {
          return (
            /* binding */
            _Tab2Page
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! tslib */
      64762);
      /* harmony import */


      var _raw_loader_tab2_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! !raw-loader!./tab2.page.html */
      82477);
      /* harmony import */


      var _tab2_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./tab2.page.scss */
      92055);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/core */
      37716);
      /* harmony import */


      var _services_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../services/data.service */
      52468);
      /* harmony import */


      var _services_hero_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../services/hero.service */
      19405);
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! moment */
      16738);
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      39895);
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      80476);

      var _Tab2Page = /*#__PURE__*/function () {
        function Tab2Page(heroService, dataService, router, platform) {
          _classCallCheck(this, Tab2Page);

          this.heroService = heroService;
          this.dataService = dataService;
          this.router = router;
          this.platform = platform;
          this.allData = {};
          this.page = 0;
          debugger;
          this.allData.ideaListArray = [];
          this.allData.savedCount = 0;
          this.allData.submittedCount = 0;

          if (this.platform.is('ios')) {
            this.isIos = true;
            console.log("platform is ios");
          } else if (this.platform.is('android')) {
            this.isIos = false;
            console.log("platform is android");
          }
        } // ionViewDidEnter(){


        _createClass(Tab2Page, [{
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            var _this = this;

            debugger;
            this.allData.showHeader = false;
            this.dataService.currentData.subscribe(function (data) {
              var data1 = data;
              debugger;
              _this.allData.ideaListArray = data1; // if(this.allData.ideaListArray.length > 0) {
              // }

              _this.allData.showHeader = true;

              _this.heroService.otoa(_this.allData.ideaListArray).map(function (d) {
                // return d.IID_CREATED_ON[0] = moment(new Date(d.IID_CREATED_ON[0]), "DD-MM-YYYY").format("DD-MM-YYYY");
                return d.IID_CREATED_ON[0] = moment__WEBPACK_IMPORTED_MODULE_4__(new Date(d.IID_CREATED_ON[0])).format();
              });

              console.log("tab2 screen check data: ", _this.allData.ideaListArray);

              var saved = _this.allData.ideaListArray.filter(function (d) {
                return d.IID_STATUS[0] == 'Saved';
              });

              _this.allData.savedCount = saved.length;

              var submitted = _this.allData.ideaListArray.filter(function (d) {
                return d.IID_STATUS[0] == 'Submitted';
              });

              _this.allData.submittedCount = submitted.length;
            });
          }
        }, {
          key: "loadData",
          value: function loadData(infiniteScroll) {
            var that = this;
            that.page = that.page + 10;
            debugger;
            setTimeout(function () {
              // let baseURLp;
              // baseURLp = this.apiCall.mainUrl + "users/getCustomer?uid=" + that.islogin._id + "&pageNo=" + that.page + "&size=" + that.limit + '&search=' + that.searchKey_string + "&all=true";
              // that.ndata = [];
              // this.apiCall.getCustomersCall(baseURLp)
              //   .subscribe(data => {
              //     that.ndata = data;
              //     for (let i = 0; i < that.ndata.length; i++) {
              //       that.CustomerData.push(that.ndata[i]);
              //     }
              //     that.CustomerArraySearch = [];
              //     that.CustomerArraySearch = that.CustomerData;
              //   },
              //     err => {
              //       this.apiCall.stopLoading();
              //     });
              debugger;

              that._getSearchData(infiniteScroll); // infiniteScroll.disabled = true;

            }, 500);
          }
        }, {
          key: "_showDetails",
          value: function _showDetails(item) {
            console.log("check details obj: ", item);
            var that = this;
            that.dataService.changeIdeaDetailsData(item);
            that.router.navigateByUrl("/idea-details");
          }
        }, {
          key: "closeHeader",
          value: function closeHeader() {
            this.allData.showHeader = false;
          }
        }, {
          key: "_getSearchData",
          value: function _getSearchData(infiniteScroll) {
            var that = this;
            var params = that.heroService.globalGet('searchParams');
            console.log("params on tab2 from search screen: " + params); // let user;
            // if (that.allData.isAdmin == true) {
            //   if (that.allData.requestorId == '' || that.allData.requestorId == null || that.allData.requestorId == undefined) {
            //     user = "";
            //   } else {
            //     user = that.allData.requestorId;
            //   }
            // } else {
            //   user = localStorage.getItem("loggedinuser");
            // }

            var method = 'GetSearchData';
            var param = {
              cursor: {
                "id": 0,
                "position": that.page,
                "numRows": 10
              },
              reqID: params.reqID,
              ideaCat: params.ideaCat,
              ideaDesc: params.ideaDesc,
              applSystem: params.applSystem,
              intiator: params.intiator,
              vechPlatf: params.vechPlatf,
              fromDate: params.fromDate,
              toDate: params.toDate,
              ideaStatus: params.ideaStatus,
              sector: params.sector,
              fromReqID: params.fromReqID,
              toReqID: params.toReqID
            };
            that.callSubCode(method, param, infiniteScroll);
          }
        }, {
          key: "callSubCode",
          value: function callSubCode(method, params, infiniteScroll) {
            var that = this;
            debugger;
            var dataObj123 = {
              _name: "SOAP:Envelope",
              _attrs: {
                "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
              },
              _content: {
                "SOAP:Body": [{
                  _name: method,
                  _attrs: {
                    "xmlns": "http://schemas.cordys.com/igp"
                  },
                  _content: [{
                    _name: "cursor",
                    _attrs: {
                      "id": "0",
                      "position": params.cursor.position,
                      "numRows": "10"
                    }
                  }, {
                    "fromReqID": params.fromReqID
                  }, {
                    "reqID": params.reqID
                  }, {
                    "toReqID": params.toReqID
                  }, {
                    "ideaCat": params.ideaCat
                  }, {
                    "ideaDesc": params.ideaDesc
                  }, {
                    "applSystem": params.applSystem
                  }, {
                    "intiator": params.intiator
                  }, {
                    "vechPlatf": params.vechPlatf
                  }, {
                    "fromDate": params.fromDate
                  }, {
                    "toDate": params.toDate
                  }, {
                    "ideaStatus": params.ideaStatus
                  }, {
                    "sector": params.sector
                  }]
                }]
              }
            };
            that.heroService.testService123(dataObj123, function (err, response) {
              debugger;

              if (response) {
                var s = response['SOAP:Envelope'];
                var faultString = s['SOAP:Body'][0]['SOAP:Fault'];

                if (faultString != undefined) {
                  if (faultString[0].faultstring[0]._ == "Unable to bind the artifact to a SAML assertion.") {
                    that.heroService._toastrErrorMsg("Token is expired. Please logout and login to access the app.");

                    return;
                  }

                  console.log("fault string: ", faultString[0].faultstring[0]._);
                }

                var a = s['SOAP:Body'][0].GetSearchDataResponse;

                if (a != undefined) {
                  var obj = $.cordys.json.findObjects(a[0], "LOV_MASTER_AUTO");
                  console.log("check search obj: ", obj);
                  that.heroService.otoa(obj).map(function (d) {
                    return d.IID_CREATED_ON[0] = moment__WEBPACK_IMPORTED_MODULE_4__(new Date(d.IID_CREATED_ON[0])).format();
                  });

                  for (var i = 0; i < obj.length; i++) {
                    that.allData.ideaListArray.push(obj[i]);
                  }

                  infiniteScroll.target.complete(); // infiniteScroll.disabled = true;

                  that.allData.showHeader = true;
                  console.log("tab2 screen check data: ", that.allData.ideaListArray);
                  var saved = that.allData.ideaListArray.filter(function (d) {
                    return d.IID_STATUS[0] == 'Saved';
                  });
                  that.allData.savedCount = saved.length;
                  var submitted = that.allData.ideaListArray.filter(function (d) {
                    return d.IID_STATUS[0] == 'Submitted';
                  });
                  that.allData.submittedCount = submitted.length;
                }
              } else {
                console.log("error found in err tag: ", err);
              }
            }); // }
          }
        }]);

        return Tab2Page;
      }();

      _Tab2Page.ctorParameters = function () {
        return [{
          type: _services_hero_service__WEBPACK_IMPORTED_MODULE_3__.HeroService
        }, {
          type: _services_data_service__WEBPACK_IMPORTED_MODULE_2__.DataService
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.Platform
        }];
      };

      _Tab2Page.propDecorators = {
        infiniteScroll: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.ViewChild,
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonInfiniteScroll]
        }]
      };
      _Tab2Page = (0, tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([(0, _angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-tab2',
        template: _raw_loader_tab2_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_tab2_page_scss__WEBPACK_IMPORTED_MODULE_1__["default"]]
      })], _Tab2Page);
      /***/
    },

    /***/
    92055:
    /*!*************************************!*\
      !*** ./src/app/tab2/tab2.page.scss ***!
      \*************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".submittedCard {\n  background-color: #E8ECEE;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRhYjIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQUE7QUFDSiIsImZpbGUiOiJ0YWIyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zdWJtaXR0ZWRDYXJke1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNFOEVDRUU7XG59Il19 */";
      /***/
    },

    /***/
    82477:
    /*!***************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab2/tab2.page.html ***!
      \***************************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header [translucent]=\"true\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title mode=\"ios\">\n      Search IDEA\n    </ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button (click)=\"heroService.logout()\">\n        <ion-icon slot=\"icon-only\" name=\"log-out-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ng-container *ngIf=\"isIos == true\">\n  <ion-header *ngIf=\"allData.showHeader == true\" class=\"ion-no-padding\" mode=\"md\">\n    <ion-toolbar color=\"tertiary\" class=\"ion-no-padding\" style=\"height: 45px;padding: 15px 0px;\">\n      <p style=\"color: #2c5364;font-weight: 500; margin-left: 10px;margin-top: -33px; margin-bottom: 0px;\">Records\n        Found:\n        Saved {{allData.savedCount}} / Submitted {{allData.submittedCount}}</p>\n      <ion-icon name=\"close\" slot=\"primary\" color=\"primary\" (click)=\"closeHeader()\"\n        style=\"margin-top: -45px;margin-right: 10px; font-size:20px;font-weight: 500;\"></ion-icon>\n    </ion-toolbar>\n  </ion-header>\n</ng-container>\n<ng-container *ngIf=\"isIos == false\">\n  <ion-header *ngIf=\"allData.showHeader == true\" class=\"ion-no-padding\" mode=\"md\">\n    <ion-toolbar color=\"tertiary\" class=\"ion-no-padding\" style=\"height: 30px;\">\n      <p style=\"color: #2c5364;font-weight: 500; margin-left: 10px;margin-top: -25px; margin-bottom: 0px;\">Records\n        Found:\n        Saved {{allData.savedCount}} / Submitted {{allData.submittedCount}}</p>\n      <ion-icon name=\"close\" slot=\"primary\" color=\"primary\" (click)=\"closeHeader()\"\n        style=\"margin-top: -25px;margin-right: 10px; font-size:20px;font-weight: 500;\"></ion-icon>\n    </ion-toolbar>\n  </ion-header>\n</ng-container>\n<ion-content [fullscreen]=\"true\">\n\n  <ng-container *ngIf=\"allData.ideaListArray.length == 0\">\n    <ion-card style=\"border-radius: 15px;\">\n      <ion-card-content>\n        Oops...No IDEA Found!!\n      </ion-card-content>\n    </ion-card>\n  </ng-container>\n  <ng-container *ngIf=\"allData.ideaListArray.length > 0\">\n    <ion-card *ngFor=\"let item of allData.ideaListArray\" style=\"border-radius: 15px;\"\n      [ngClass]=\"{'submittedCard': item.IID_STATUS[0] === 'Submitted' }\">\n      <ion-row>\n        <ion-col size=\"8\" style=\"padding-top: 10px;\">\n          <ion-title class=\"ion-text-left\" color=\"primary\" mode=\"md\">{{item.IID_REQUEST_ID[0]}}</ion-title>\n\n        </ion-col>\n        <ion-col class=\"ion-no-padding ion-text-right\" size=\"4\">\n          <ion-button size=\"large\" fill=\"clear\" (click)=\"_showDetails(item)\" class=\"ion-no-padding\"\n            style=\"padding-right: 10px; margin-top: -5px;\">\n            <ion-icon slot=\"icon-only\" name=\"create-outline\" color=\"primary\"></ion-icon>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-card-content style=\"margin-top: -20px;\">\n        <ion-row>\n          <ion-col size=\"8\" style=\"padding-top: 10px;\">\n            <p style=\"font-size:15px\">{{item.IID_IDEA_DESCRIPTION[0]}}</p>\n          </ion-col>\n          <ion-col class=\"ion-no-padding ion-text-right\" size=\"4\">\n            <div style=\"position: absolute;bottom: 0px;width: 100%;\">\n              <ion-button fill=\"clear\" color=\"primary\" class=\"ion-no-padding\" style=\"font-size: 16px;\">\n                {{item.IID_CREATED_ON[0] | date:'mediumDate'}}\n              </ion-button>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n    </ion-card>\n  </ng-container>\n\n  <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\n    <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n\n\n  <!-- <app-explore-container name=\"Tab 2 page\"></app-explore-container> -->\n</ion-content>";
      /***/
    }
  }]);
})();
//# sourceMappingURL=src_app_tab2_tab2_module_ts-es5.js.map