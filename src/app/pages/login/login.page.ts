import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { HeroService } from 'src/app/services/hero.service';
// import { SoapService } from 'src/app/services/soap.service';
import { URLS } from 'src/app/services/urls';
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });
  whichUser: any = "option2";
  constructor(
    private router: Router,
    // public soapService: SoapService,
    public heroService: HeroService,
    public urls: URLS,
    private loadingController: LoadingController,
    public navCtrl: NavController
  ) {
    localStorage.setItem('option', 'MnM');
  }

  ngOnInit() {
  }

  private static adminParam(): {}[] {
    var parameters: {}[] = [];
    parameters["sector"] = "";
    return parameters;
  }
  private static caseParam(data): {}[] {
    var parameters: {}[] = [];
    parameters["vendorCode"] = data;
    return parameters;
  }

  _checkIfAdmin() {
    let that = this;
    var method: string = 'CheckUserTypeWeb';
    var parameters: {}[] = [];
    that.callSubCodeNew(parameters, method, 'admin');
  }
  _checkCaseSensetive() {
    let that = this;
    var method: string = 'GetSupplierDetail';
    var parameters: {}[] = [];
    that.callSubCodeNew(parameters, method, 'caseSensetive');
  }

  changeRadio() {
    let that = this;
    console.log("checked value: ", that.whichUser);
    if (that.whichUser == 'option1') { // for supplier work as it is
      localStorage.setItem('option', 'Supplier');
    } else if (that.whichUser == 'option2') {
      localStorage.setItem('option', 'MnM');
    }
  }
  private static userLogin(username, password): {}[] {
    var parameters: {}[] = [];
    parameters["username"] = username;
    parameters["password"] = password;
    return parameters;
  }
  private static checkParam(username): {}[] {
    var parameters: {}[] = [];
    parameters["UM_USER_ID"] = username;
    return parameters;
  }

  _login() {
    let that = this;
    var method: string;
    debugger
    let finalUser = localStorage.getItem('option');
    // let finalUser = that.heroService.globalGet('option');
    if (finalUser == 'Supplier') {
      method = 'GenerateSAML'
    } else if (finalUser == 'MnM') {
      // convert username and password into base64 format
      let str = btoa(that.loginForm.value.username + ':' + that.loginForm.value.password);
      localStorage.setItem("base64String", str);
      // method = 'CheckActiveUsersIGP'
      method = 'GetUserMasterObject'
    }
    var parameters: {}[] = [];
    that.callSubCode(parameters, method, finalUser);
  }

  callSubCode(parameters, method, finalUser) {
    let that = this;
    if (finalUser == 'Supplier') {
      parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = LoginPage.userLogin(that.loginForm.value.username, that.loginForm.value.password);
    } else {
      if (finalUser == 'MnM') {
        parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = LoginPage.checkParam(that.loginForm.value.username);
      }
    }
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    that.loadingController.create({
      spinner: 'bubbles',
      message: "Please wait..."
    }).then((loadEl) => {
      loadEl.present();

      that.heroService.testService(parameters, function (err, response) {
        loadEl.dismiss();
        debugger
        if (response) {
          let s = response['SOAP:Envelope'];
          if (finalUser == 'Supplier') {
            let a = s['SOAP:Body'][0].GenerateSAMLResponse;
            let b;
            if (a != undefined) {
              b = a[0].tuple[0].old[0].generateSAML[0].generateSAML[0];
              localStorage.setItem("Token", b);
              localStorage.setItem("loggedinuser", that.loginForm.value.username);
              ////////////// Anjali new code starts /////////////////
              // that._checkIfAdmin();

              that._checkCaseSensetive();
              ////////////// Anjali new code ends /////////////////

              // that.navCtrl.navigateRoot(['/idea/menu']);
              // that.heroService._toastrMsg("Logged in successfully.");
            } else {
              b = s['SOAP:Body'][0]["SOAP:Fault"][0].faultstring[0]._;
              if (b != undefined)
                // that.heroService._toastrErrorMsg("Error occured while login. Please contact administrator.");
                that.heroService._toastrErrorMsg(b);
              else
                that.heroService._toastrErrorMsg("The username or password you entered is incorrect.");
            }
            console.log("login debugger:", b)
          } else if (finalUser == 'MnM') {
            // let a = s['SOAP:Body'][0].CheckActiveUsersIGPResponse;
            let a = s['SOAP:Body'][0].GetUserMasterObjectResponse;
            let b;
            if (a != undefined) {
              b = $.cordys.json.findObjects(a[0], 'USER_MASTER');
              if (b.length > 0) {
                localStorage.setItem("loggedinuser", that.loginForm.value.username);
                ////////////// Anjali new code starts /////////////////
                that._checkIfAdmin();
                ////////////// Anjali new code ends /////////////////
                // that.navCtrl.navigateRoot(['/idea/menu']);
                // that.heroService._toastrMsg("Logged in successfully.");
              }
            } else {
              b = s['SOAP:Body'][0]["SOAP:Fault"][0].faultstring[0]._;
              if (b != undefined)
                // that.heroService._toastrErrorMsg("Error occured while login. Please contact administrator.");
                that.heroService._toastrErrorMsg(b);
              else
                that.heroService._toastrErrorMsg("The username or password you entered is incorrect.");
            }
            console.log("login debugger:", b)
          }
        } else {
          that.heroService._toastrErrorMsg("Error occured while logging. Please contact administrator.");
        }
      });
    });
  }

  callSubCodeNew(parameters, method, key) {
    let that = this;
    if (key == 'admin') {
      parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = LoginPage.adminParam();
      parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    } else if (key == 'caseSensetive') {
      parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = LoginPage.caseParam(that.loginForm.value.username);
      parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    }

    that.loadingController.create({
      spinner: 'bubbles',
      message: "Please wait..."
    }).then((loadEl) => {
      loadEl.present();
      that.heroService.testService1(parameters, function (err, response) {
        loadEl.dismiss();
        debugger
        if (response) {
          let s = response['SOAP:Envelope'];
          debugger
          if (key == 'caseSensetive') {
            let a = s['SOAP:Body'][0].GetSupplierDetailResponse;
            if (a != undefined) {
              let obj = $.cordys.json.findObjects(a[0], "SUPPLIER_MASTER");
              console.log("check case obj in login screen: ", obj[0]);
              debugger
              if (obj.length > 0) {
                that._checkIfAdmin();
              } else {
                that.heroService._toastrErrorMsg("Username or Password is incorrect.");
                localStorage.clear();
                that.loginForm.reset();
                that.whichUser = "option2";
                localStorage.setItem('option', 'MnM');
              }
            }
          } else if (key == 'admin') {
            let a = s['SOAP:Body'][0].CheckUserTypeWebResponse;
            if (a != undefined) {
              let obj = $.cordys.json.findObjects(a[0], "CheckUserTypeWeb");
              console.log("check admin obj in login screen: ", obj[0]);
              debugger
              if (obj.length > 0) {
                let role = obj[0].CheckUserTypeWeb[0];
                if (role.includes("No IGP roles assigned")) {
                  that.heroService._toastrErrorMsg("User do not have any access!");
                  localStorage.clear();
                  that.loginForm.reset();
                  that.whichUser = "option2";
                  localStorage.setItem('option', 'MnM');
                } else {
                  that.navCtrl.navigateRoot(['/idea/menu']);
                  that.heroService._toastrMsg("Logged in successfully.");
                }
              }
            }
          }

        } else {
          that.heroService._toastrErrorMsg("Error occured while logging. Please contact administrator.");
        }
      });
    });
  }

  _visiblePass: boolean = false;
  _showPassword(){
    let that = this;
    that._visiblePass = !that._visiblePass;
  }

}
