import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { HeroService } from 'src/app/services/hero.service';
import { LoadingController, MenuController } from '@ionic/angular';
import * as moment from 'moment';
import { DataService } from 'src/app/services/data.service';
declare var $: any;
@Component({
  selector: 'app-idea',
  templateUrl: './idea.page.html',
  styleUrls: ['./idea.page.scss'],
})
export class IdeaPage implements OnInit {
  selectedPath = '';
  allData: any = {};
  constructor(
    private router: Router,
    private heroService: HeroService,
    public loadingController: LoadingController,
    public dataService: DataService,
    private menuCtrl: MenuController
  ) {

    this.allData.vehPlatformsArray = [];
    this.allData.sectorsArray = [];
    this.allData.systemArray = [];
    this.allData.ideacategoryArray = [];
    this.allData.sector = undefined;
    this.allData.vehiclePlt = undefined;
    this.allData.appSys = undefined;
    this.allData.idaCat = undefined;
    this.allData.catBtn = false;
    this.router.events.subscribe((event: RouterEvent) => {
      if (event && event.url) {
        this.selectedPath = event.url;
      }
    });
    if (localStorage.getItem("loggedinuser") != null) {
      let user = localStorage.getItem("loggedinuser");
      let params = {
        UM_USER_ID: user
      }
      var method: string = 'GetUserMasterObject';
      var parameters: {}[] = [];
      this.callSubCode(parameters, method, params, 'userD', '');
    }
  }

  ngOnInit() {
    console.log("ngOnInit")
    let user = localStorage.getItem("loggedinuser");
    this.allData.requestorId = user;
    // this._checkIfAdmin();
    this._getSectorList();
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter")
    console.log("check url :", this.router.url);
    if (this.router.url == "/idea/menu/tabs/tab2") {
      this._getSearchData('initial');
    }
    //////////////////////
    // this._getSearchData('initial');
    //////////////////////
  }

  private static serviceParam(user): {}[] {
    var parameters: {}[] = [];
    parameters["UM_USER_ID"] = user;
    return parameters;
  }

  callSubCode(parameters, method, params, key, checkFrom) {
    let that = this;
    if (key == 'userD') {
      parameters[method + ' xmlns:SOAP="http://schemas.xmlsoap.org/soap/envelope/" xmlns="http://schemas.cordys.com/igp" preserveSpace="no" qAccess="0" qValues=""'] = IdeaPage.serviceParam(params.UM_USER_ID);
      parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    } else if (key == 'admin') {
      parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaPage.adminParam(params);
      parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    } else if (key == "search") {
      debugger;
      // parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaPage.searchParam(params);
      // parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
      let dataObj123 = {
        _name: "SOAP:Envelope",
        _attrs: {
          "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
        },
        _content: {
          "SOAP:Body": [
            {
              _name: method,
              _attrs: {
                "xmlns": "http://schemas.cordys.com/igp"
              },
              _content: [
                {
                  _name: "cursor",
                  _attrs: {
                    "id": "0",
                    "position": "0",
                    "numRows": "10"
                  },
                },
                {
                  "fromReqID": params.fromReqID
                },
                { "reqID": params.reqID },
                { "toReqID": params.toReqID },
                { "ideaCat": params.ideaCat },
                { "ideaDesc": params.ideaDesc },
                { "applSystem": params.applSystem },
                { "intiator": params.intiator },
                { "vechPlatf": params.vechPlatf },
                { "fromDate": params.fromDate },
                { "toDate": params.toDate },
                { "ideaStatus": params.ideaStatus },
                { "sector": params.sector }
              ]
            }
          ]
        }
      }
      
      that.loadingController.create({
        spinner: 'bubbles',
        message: 'Please wait...'
      }).then((loadEl) => {
        loadEl.present();
        that.heroService.testService123(dataObj123, function (err, response) {
          loadEl.dismiss();
          debugger
          if (response) {
            let s = response['SOAP:Envelope'];

            let faultString = s['SOAP:Body'][0]['SOAP:Fault'];
            if (faultString != undefined) {
              if (faultString[0].faultstring[0]._ == "Unable to bind the artifact to a SAML assertion.") {
                that.heroService._toastrErrorMsg("Token is expired. Please logout and login to access the app.");
                return;
              }
              console.log("fault string: ", faultString[0].faultstring[0]._);
            }

            if (key == 'search') {
              let a = s['SOAP:Body'][0].GetSearchDataResponse;
              if (a != undefined) {
                let obj = $.cordys.json.findObjects(a[0], "LOV_MASTER_AUTO");
                console.log("check search obj: ", obj);
                debugger
                // that.dataService.removeData();
                that.dataService.changeData(obj);
                that.menuCtrl.close();
                if (checkFrom != "initial") {
                  if (that.router.url == '/idea/menu' || that.router.url == '/idea/menu/tabs/tab1') {
                    that.router.navigateByUrl("/idea/menu/tabs/tab2");
                  }
                }
              }
            }

          } else {
            console.log("error found in err tag: ", err)
          }
        });
      })
    }
    if (key != "search") {
      that.loadingController.create({
        spinner: 'bubbles',
        message: 'Please wait...'
      }).then((loadEl) => {
        loadEl.present();
        that.heroService.testService1(parameters, function (err, response) {
          loadEl.dismiss();
          debugger
          if (response) {
            let s = response['SOAP:Envelope'];

            let faultString = s['SOAP:Body'][0]['SOAP:Fault'];
            if (faultString != undefined) {
              if (faultString[0].faultstring[0]._ == "Unable to bind the artifact to a SAML assertion.") {
                that.heroService._toastrErrorMsg("Token is expired. Please logout and login to access the app.");
                return;
              }
              console.log("fault string: ", faultString[0].faultstring[0]._);
            }

            if (key == 'userD') {
              let a = s['SOAP:Body'][0].GetUserMasterObjectResponse;
              let b = a[0].tuple;
              that.allData.userDetails = b[0].old[0].USER_MASTER[0];
              that._checkIfAdmin();
            } else if (key == 'admin') {
              let a = s['SOAP:Body'][0].CheckUserTypeWebResponse;
              if (a != undefined) {
                let obj = $.cordys.json.findObjects(a[0], "CheckUserTypeWeb");
                console.log("check admin obj: ", obj[0]);
                debugger
                if (obj.length > 0) {
                  let role = obj[0].CheckUserTypeWeb[0];
                  if (role.includes("ADMIN")) {
                    that.allData.isAdmin = true;
                  } else if (role.includes("MnM User")) {
                    that.allData.isAdmin = false;
                  } else if (role.includes("No IGP roles assigned")) {
                    that.heroService.logout();
                  }
                }
              }
            } else if (key == 'search') {
              let a = s['SOAP:Body'][0].GetSearchDataResponse;
              if (a != undefined) {
                let obj = $.cordys.json.findObjects(a[0], "LOV_MASTER_AUTO");
                console.log("check search obj: ", obj);
                debugger
                // that.dataService.removeData();
                that.dataService.changeData(obj);
                that.menuCtrl.close();
                if (checkFrom != "initial") {
                  if (that.router.url == '/idea/menu' || that.router.url == '/idea/menu/tabs/tab1') {
                    that.router.navigateByUrl("/idea/menu/tabs/tab2");
                  }
                }
              }
            }

          } else {
            console.log("error found in err tag: ", err)
          }
        });
      })
    }
  }

  private static dropdownParam(reqType): {}[] {
    var parameters: {}[] = [];
    parameters["reqType"] = reqType;
    // parameters["tk"] = tk;
    return parameters;
  }

  _getSectorList() {
    let that = this;
    let params = {
      reqType: "SECTOR"
    }
    var method: string = 'GetLOVObjectForIdeaGen';
    var parameters: {}[] = [];
    that.callSubCodeNew(parameters, method, params, 'sector');

  }

  callSubCodeNew(parameters, method, params, key) {
    let that = this;

    parameters[method + " xmlns='http://schemas.cordys.com/igp'"] = IdeaPage.dropdownParam(params.reqType);
    parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    // this.url.startLoading().present();
    that.heroService.testService1(parameters, function (err, response) {
      // that.url.stopLoading();
      // debugger
      if (response) {
        var s = response['SOAP:Envelope'];
        var a = s['SOAP:Body'][0].GetLOVObjectForIdeaGenResponse;
        var b = a[0].tuple;
        let daa = b.map((d) => { return d.old[0].LOV_MASTER_AUTO[0] })
        console.log("get drop down debugger:", b)
        if (key == 'sector') {
          that.allData.sectorsArray = daa;
          let method: string = 'GetLOVObjectForIdeaGen';
          let parameters: {}[] = [];
          let params1 = {
            reqType: "VAVE_LEVER"
          }
          that.callSubCodeNew(parameters, method, params1, 'ideaCat');
        } else if (key == 'VehiclePlt') {
          that.allData.vehiclePlt = undefined;
          that.allData.vehPlatformsArray = daa;
        } else if (key == 'appSys') {
          that.allData.appSys = undefined;
          that.allData.systemArray = daa;
        } else if (key == 'ideaCat') {
          that.allData.ideacategoryArray = daa;
        }


      } else {
        console.log("error found in err tag: ", err)
      }
    });
  }

  callthis(value: any) {
    debugger
    console.log("im here: ", value);
    let that = this;
    let params = {
      reqType: "MODEL_AFFECTED###" + value
    }
    var method: string = 'GetLOVObjectForIdeaGen';
    var parameters: {}[] = [];
    that.callSubCodeNew(parameters, method, params, 'VehiclePlt');
    let params1 = {
      reqType: "SYSTEM###" + value
    }
    that.callSubCodeNew(parameters, method, params1, 'appSys');
  }
  private static adminParam(user): {}[] {
    var parameters: {}[] = [];
    parameters["sector"] = "";
    return parameters;
  }
  _checkIfAdmin() {
    let that = this;
    var method: string = 'CheckUserTypeWeb';
    var parameters: {}[] = [];
    let param = {};
    that.callSubCode(parameters, method, param, 'admin', '');
  }
  _getSearchData(key) {
    let that = this;
    let user;
    if (that.allData.isAdmin == true) {
      if (that.allData.requestorId == '' || that.allData.requestorId == null || that.allData.requestorId == undefined) {
        user = "";
      } else {
        user = that.allData.requestorId;
      }
    } else {
      user = localStorage.getItem("loggedinuser");
    }
    var method: string = 'GetSearchData';
    var parameters: {}[] = [];
    let param = {
      cursor: {
        "@id": 0,
        "@position": 0,
        "@numRows": 10,
      },
      reqID: (that.allData.requestNumber ? that.allData.requestNumber : ""),
      ideaCat: (that.allData.idaCat ? that.allData.idaCat : ""),
      ideaDesc: (that.allData.ideaDesc ? that.allData.ideaDesc : ""),
      applSystem: (that.allData.appSys ? that.allData.appSys : ""),
      intiator: user,
      vechPlatf: (that.allData.vehiclePlt ? that.allData.vehiclePlt : ""),
      fromDate: (that.allData.fromDate ? moment(new Date(that.allData.fromDate), "DD-MM-YYYY").format("DD-MM-YYYY") : ""),
      toDate: (that.allData.toDate ? moment(new Date(that.allData.toDate), "DD-MM-YYYY").format("DD-MM-YYYY") : ""),
      ideaStatus: (that.allData.status ? that.allData.status : ""),
      sector: (that.allData.sector ? that.allData.sector : ""),
      fromReqID: (that.allData.fromUID ? that.allData.fromUID : ""),
      toReqID: (that.allData.toUID ? that.allData.toUID : "")

    };
    that.heroService.globalSet('searchParams', param);
    that.callSubCode(parameters, method, param, 'search', key);

  }
  // _getSearchData(key) {
  //   let that = this;
  //   let user;
  //   if (that.allData.isAdmin == true) {
  //     if (that.allData.requestorId == '' || that.allData.requestorId == null || that.allData.requestorId == undefined) {
  //       user = "";
  //     } else {
  //       user = that.allData.requestorId;
  //     }
  //   } else {
  //     user = localStorage.getItem("loggedinuser");
  //   }
  //   var method: string = 'GetSearchData';
  //   var parameters: {}[] = [];
  //   let param = {
  //     reqID: (that.allData.requestNumber ? that.allData.requestNumber : ""),
  //     ideaCat: (that.allData.idaCat ? that.allData.idaCat : ""),
  //     ideaDesc: (that.allData.ideaDesc ? that.allData.ideaDesc : ""),
  //     applSystem: (that.allData.appSys ? that.allData.appSys : ""),
  //     intiator: user,
  //     vechPlatf: (that.allData.vehiclePlt ? that.allData.vehiclePlt : ""),
  //     fromDate: (that.allData.fromDate ? moment(new Date(that.allData.fromDate), "DD-MM-YYYY").format("DD-MM-YYYY") : ""),
  //     toDate: (that.allData.toDate ? moment(new Date(that.allData.toDate), "DD-MM-YYYY").format("DD-MM-YYYY") : ""),
  //     ideaStatus: (that.allData.status ? that.allData.status : ""),
  //     sector: (that.allData.sector ? that.allData.sector : ""),
  //     fromReqID: (that.allData.fromUID ? that.allData.fromUID : ""),
  //     toReqID: (that.allData.toUID ? that.allData.toUID : "")

  //   };
  //   that.callSubCode(parameters, method, param, 'search', key);

  // }
  _clear() {
    let that = this;
    that.allData.requestNumber = undefined;
    that.allData.fromDate = undefined;
    that.allData.toDate = undefined;
    that.allData.status = undefined;
    that.allData.appSys = undefined;
    that.allData.vehiclePlt = undefined;
    that.allData.sector = undefined;
    that.allData.idaCat = undefined;
    that.allData.ideaDesc = undefined;
    that.allData.fromUID = undefined;
    that.allData.toUID = undefined;
    if (that.allData.isAdmin == true) {
      that.allData.requestorId = undefined;
    }
    // that.dataService.removeData();
    that._getSearchData('');
  }

}
