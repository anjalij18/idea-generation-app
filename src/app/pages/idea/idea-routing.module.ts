import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IdeaPage } from './idea.page';

const routes: Routes = [
  {
    path: '',
    component: IdeaPage,
    children: [
      {
        path: 'menu',
        loadChildren: () => import('../../tabs/tabs.module').then(m => m.TabsPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IdeaPageRoutingModule {}
