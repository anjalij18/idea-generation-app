import { Component, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, IonRouterOutlet, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  constructor(public router: Router, public alertController: AlertController, private platform: Platform,public splashScreen: SplashScreen,public statusBar: StatusBar) {
    // platform.ready().then(() => {

    //   statusBar.styleDefault();
    //   statusBar.hide();
    //   this.splashScreen.hide();
    // });
    this.backButtonEvent();
    
  }
  ngOnInit() {
    if (localStorage.getItem("loggedinuser") != null && localStorage.getItem("Token") != null) {
      this.router.navigateByUrl('/idea/menu');
    } else {
      if (localStorage.getItem("loggedinuser") != null && localStorage.getItem("base64String") != null) {
        this.router.navigateByUrl('/idea/menu');
      } else {
        this.router.navigateByUrl('/login');
      }
    }
  }
  backButtonEvent() {
    this.platform.backButton.subscribeWithPriority(0, () => {
      navigator['app'].exitApp();
      // this.routerOutlets.forEach(async (outlet: IonRouterOutlet) => {
      //   if (this.router.url != '/tabs/tab1') {
      //     await this.router.navigate(['/tabs/tab1']);
      //   } else if (this.router.url === '/tabs/tab1') {
      //     if (new Date().getTime() - this.lastTimeBackPress >= this.timePeriodToExit) {
      //       this.lastTimeBackPress = new Date().getTime();
      //       this.presentAlertConfirm();
      //     } else {
      //       navigator['app'].exitApp();
      //     }
      //   }
      // });
    });
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      // header: 'Confirm!',
      message: 'Are you sure you want to exit the app?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => { }
      }, {
        text: 'Close App',
        handler: () => {
          navigator['app'].exitApp();
        }
      }]
    });

    await alert.present();
  }
}
