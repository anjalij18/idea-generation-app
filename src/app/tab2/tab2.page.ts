import { Component, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { HeroService } from '../services/hero.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { IonInfiniteScroll, Platform } from '@ionic/angular';
declare var $: any;

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  allData: any = {};
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  isIos: boolean;
  constructor(
    public heroService: HeroService,
    public dataService: DataService,
    private router: Router,
    public platform: Platform
  ) {
    debugger

    this.allData.ideaListArray = [];
    this.allData.savedCount = 0;
    this.allData.submittedCount = 0;
    if (this.platform.is('ios')) {
      this.isIos = true;
      console.log("platform is ios")
    } else if (this.platform.is('android')) {
      this.isIos = false;
      console.log("platform is android")

    }
  }

  // ionViewDidEnter(){
  ionViewWillEnter() {
    debugger
    this.allData.showHeader = false;
    this.dataService.currentData.subscribe((data) => {
      let data1 = data;
      debugger
      this.allData.ideaListArray = data1;
      // if(this.allData.ideaListArray.length > 0) {

      // }
      this.allData.showHeader = true;
      this.heroService.otoa(this.allData.ideaListArray).map((d) => {
        // return d.IID_CREATED_ON[0] = moment(new Date(d.IID_CREATED_ON[0]), "DD-MM-YYYY").format("DD-MM-YYYY");
        return d.IID_CREATED_ON[0] = moment(new Date(d.IID_CREATED_ON[0])).format();
      })
      console.log("tab2 screen check data: ", this.allData.ideaListArray);
      let saved = this.allData.ideaListArray.filter((d) => {
        return d.IID_STATUS[0] == 'Saved';
      })
      this.allData.savedCount = saved.length;

      let submitted = this.allData.ideaListArray.filter((d) => {
        return d.IID_STATUS[0] == 'Submitted';
      })
      this.allData.submittedCount = submitted.length;

    })
  }
  page: number = 0;
  loadData(infiniteScroll) {
    let that = this;
    that.page = that.page + 10;
    debugger
    setTimeout(() => {
      // let baseURLp;

      // baseURLp = this.apiCall.mainUrl + "users/getCustomer?uid=" + that.islogin._id + "&pageNo=" + that.page + "&size=" + that.limit + '&search=' + that.searchKey_string + "&all=true";

      // that.ndata = [];
      // this.apiCall.getCustomersCall(baseURLp)
      //   .subscribe(data => {
      //     that.ndata = data;

      //     for (let i = 0; i < that.ndata.length; i++) {
      //       that.CustomerData.push(that.ndata[i]);
      //     }
      //     that.CustomerArraySearch = [];
      //     that.CustomerArraySearch = that.CustomerData;
      //   },
      //     err => {
      //       this.apiCall.stopLoading();
      //     });
      debugger
      that._getSearchData(infiniteScroll);
      // infiniteScroll.disabled = true;
    }, 500);

  }

  _showDetails(item) {
    console.log("check details obj: ", item)
    let that = this;
    that.dataService.changeIdeaDetailsData(item);
    that.router.navigateByUrl("/idea-details");
  }

  closeHeader() {
    this.allData.showHeader = false;
  }

  _getSearchData(infiniteScroll) {
    let that = this;
    let params = that.heroService.globalGet('searchParams');
    console.log("params on tab2 from search screen: " + params);

    // let user;
    // if (that.allData.isAdmin == true) {
    //   if (that.allData.requestorId == '' || that.allData.requestorId == null || that.allData.requestorId == undefined) {
    //     user = "";
    //   } else {
    //     user = that.allData.requestorId;
    //   }
    // } else {
    //   user = localStorage.getItem("loggedinuser");
    // }
    var method: string = 'GetSearchData';
    let param = {
      cursor: {
        "id": 0,
        "position": that.page,
        "numRows": 10,
      },
      reqID: params.reqID,
      ideaCat: params.ideaCat,
      ideaDesc: params.ideaDesc,
      applSystem: params.applSystem,
      intiator: params.intiator,
      vechPlatf: params.vechPlatf,
      fromDate: params.fromDate,
      toDate: params.toDate,
      ideaStatus: params.ideaStatus,
      sector: params.sector,
      fromReqID: params.fromReqID,
      toReqID: params.toReqID

    };
    that.callSubCode(method, param, infiniteScroll);

  }

  callSubCode(method, params, infiniteScroll) {
    let that = this;
    debugger;
    let dataObj123 = {
      _name: "SOAP:Envelope",
      _attrs: {
        "xmlns:SOAP": "http://schemas.xmlsoap.org/soap/envelope/"
      },
      _content: {
        "SOAP:Body": [
          {
            _name: method,
            _attrs: {
              "xmlns": "http://schemas.cordys.com/igp"
            },
            _content: [
              {
                _name: "cursor",
                _attrs: {
                  "id": "0",
                  "position": params.cursor.position,
                  "numRows": "10"
                },
              },
              {
                "fromReqID": params.fromReqID
              },
              { "reqID": params.reqID },
              { "toReqID": params.toReqID },
              { "ideaCat": params.ideaCat },
              { "ideaDesc": params.ideaDesc },
              { "applSystem": params.applSystem },
              { "intiator": params.intiator },
              { "vechPlatf": params.vechPlatf },
              { "fromDate": params.fromDate },
              { "toDate": params.toDate },
              { "ideaStatus": params.ideaStatus },
              { "sector": params.sector }
            ]
          }
        ]
      }
    }

    that.heroService.testService123(dataObj123, function (err, response) {

      debugger
      if (response) {
        let s = response['SOAP:Envelope'];

        let faultString = s['SOAP:Body'][0]['SOAP:Fault'];
        if (faultString != undefined) {
          if (faultString[0].faultstring[0]._ == "Unable to bind the artifact to a SAML assertion.") {
            that.heroService._toastrErrorMsg("Token is expired. Please logout and login to access the app.");
            return;
          }
          console.log("fault string: ", faultString[0].faultstring[0]._);
        }

        let a = s['SOAP:Body'][0].GetSearchDataResponse;
        if (a != undefined) {
          let obj = $.cordys.json.findObjects(a[0], "LOV_MASTER_AUTO");
          console.log("check search obj: ", obj);
          that.heroService.otoa(obj).map((d) => {
            return d.IID_CREATED_ON[0] = moment(new Date(d.IID_CREATED_ON[0])).format();
          })
          for (let i = 0; i < obj.length; i++) {
            that.allData.ideaListArray.push(obj[i]);
          }
          infiniteScroll.target.complete();
          // infiniteScroll.disabled = true;
          that.allData.showHeader = true;

          console.log("tab2 screen check data: ", that.allData.ideaListArray);
          let saved = that.allData.ideaListArray.filter((d) => {
            return d.IID_STATUS[0] == 'Saved';
          })
          that.allData.savedCount = saved.length;

          let submitted = that.allData.ideaListArray.filter((d) => {
            return d.IID_STATUS[0] == 'Submitted';
          })
          that.allData.submittedCount = submitted.length;
        }

      } else {
        console.log("error found in err tag: ", err)
      }
    });

    // }

  }

}
