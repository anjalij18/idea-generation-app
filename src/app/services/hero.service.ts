import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { URLS } from '../services/urls';
import * as xml2js from "xml2js";
import { toXML } from "jstoxml";
// import * as js2xmlparser from "js2xmlparser";
// var js2xmlparser = require("js2xmlparser");
import { Router } from '@angular/router';
// declare var $: any;
@Injectable({
  providedIn: 'root'
})
export class HeroService {
  globalUser: any = {};

  globalGet(key) {
    return key ? this.globalUser[key] : this.globalUser;
  }

  globalSet(key, data) {
    this.globalUser[key] = data;
  }

  headers: any;
  private envelopeBuilder_: (requestBody: string) => string = null;

  constructor(
    public urls: URLS,
    private http: HTTP,
    private toastController: ToastController,
    public router: Router,
    public navCtrl: NavController,
    private loadingController: LoadingController) {
    console.log('Hello HttpSoapProvider Provider');
    // this.headers = new HttpHeaders({ 'Content-Type': 'text/xml;charset="utf-8"' })
  }

  private toXml123(parameters: any): string {
    var xml: string = "";
    var parameter: any;

    switch (typeof (parameters)) {
      case "string":
        xml += parameters.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        break;

      case "number":
      case "boolean":
        xml += parameters.toString();
        break;

      case "object":
        if (parameters.constructor.toString().indexOf("function Date()") > -1) {
          let year: string = parameters.getFullYear().toString();
          let month: string = ("0" + (parameters.getMonth() + 1).toString()).slice(-2);
          let date: string = ("0" + parameters.getDate().toString()).slice(-2);
          let hours: string = ("0" + parameters.getHours().toString()).slice(-2);
          let minutes: string = ("0" + parameters.getMinutes().toString()).slice(-2);
          let seconds: string = ("0" + parameters.getSeconds().toString()).slice(-2);
          let milliseconds: string = parameters.getMilliseconds().toString();

          let tzOffsetMinutes: number = Math.abs(parameters.getTimezoneOffset());
          let tzOffsetHours: number = 0;

          while (tzOffsetMinutes >= 60) {
            tzOffsetHours++;
            tzOffsetMinutes -= 60;
          }

          let tzMinutes: string = ("0" + tzOffsetMinutes.toString()).slice(-2);
          let tzHours: string = ("0" + tzOffsetHours.toString()).slice(-2);

          let timezone: string = ((parameters.getTimezoneOffset() < 0) ? "-" : "+") + tzHours + ":" + tzMinutes;

          xml += year + "-" + month + "-" + date + "T" + hours + ":" + minutes + ":" + seconds + "." + milliseconds + timezone;
        }
        else if (parameters.constructor.toString().indexOf("function Array()") > -1) { // Array
          for (parameter in parameters) {
            if (parameters.hasOwnProperty(parameter)) {
              if (!isNaN(parameter)) {  // linear array
                (/function\s+(\w*)\s*\(/ig).exec(parameters[parameter].constructor.toString());

                var type = RegExp.$1;
                // debugger
                switch (type) {
                  case "":
                    type = typeof (parameters[parameter]);
                    break;
                  case "Object":
                    type = "tuple";
                    break;
                  case "String":
                    type = "string";
                    break;
                  case "Number":
                    type = "int";
                    break;
                  case "Boolean":
                    type = "bool";
                    break;
                  case "Date":
                    type = "DateTime";
                    break;
                }
                xml += this.toElement(type, parameters[parameter]);
              }
              else { // associative array
                xml += this.toElement(parameter, parameters[parameter]);
              }
            }
          }
        }
        else { // Object or custom function
          for (parameter in parameters) {
            if (parameters.hasOwnProperty(parameter)) {
              xml += this.toElement(parameter, parameters[parameter]);
            }
          }
        }
        break;

      default:
        throw new Error("SoapService error: type '" + typeof (parameters) + "' is not supported");
    }

    return xml;
  }

  private static stripTagAttributes(tagNamePotentiallyWithAttributes: string): string {
    tagNamePotentiallyWithAttributes = tagNamePotentiallyWithAttributes + ' ';

    return tagNamePotentiallyWithAttributes.slice(0, tagNamePotentiallyWithAttributes.indexOf(' '));
  }

  private toElement(tagNamePotentiallyWithAttributes: string, parameters: any): string {
    var elementContent: string = this.toXml123(parameters);

    if ("" == elementContent) {
      return "<" + tagNamePotentiallyWithAttributes + "/>";
    }
    else {
      return "<" + tagNamePotentiallyWithAttributes + ">" + elementContent + "</" + HeroService.stripTagAttributes(tagNamePotentiallyWithAttributes) + ">";
    }
  }

  set envelopeBuilder(envelopeBuilder: (response: {}) => string) {
    this.envelopeBuilder_ = envelopeBuilder;
  }

  set envelopeBuilder2(envelopeBuilder2: (response: {}) => string) {
    this.envelopeBuilder_ = envelopeBuilder2;
  }

  testService(input, cb: any) {
    let that = this;
    var request: string = this.toXml123(input);
    var envelopedRequest: string = null != this.envelopeBuilder_ ? this.envelopeBuilder_(request) : request;
    console.log(envelopedRequest);

    /////////// New code ////////////////
    let headers = {};
    let soapurl: any;
    // let finalUser = that.globalGet('option');
    let finalUser = localStorage.getItem('option');
    if (finalUser == 'Supplier') {
      headers = {
        'Content-Type': 'text/xml;charset="utf-8"'
      };
      soapurl = this.urls._baseURL;
    } else if (finalUser == 'MnM') {
      let str = localStorage.getItem("base64String");
      headers = {
        'Authorization': 'Basic ' + str,
        'Content-Type': 'text/xml;charset="utf-8"'
      };
      soapurl = this.urls._authURL;
    }
    // that.http.setSSLCertMode('pinned').then((res) => {


    let xml = envelopedRequest;
    that.http.setDataSerializer('utf8');
    that.http.post(soapurl, xml, headers)
      .then((data) => {
        debugger
        xml2js.parseString(data.data, function (err, result) {
          if (result == undefined) {
            cb(err, result)
          } else {
            cb(err, result)
          }
        });
      }).catch((err) => {
        debugger
        xml2js.parseString(err.error, function (err, result) {
          if (result == undefined) {
            cb(err, result)
          } else {
            cb(err, result)
          }
        });
      })
    // });
  }

  testService1(input, cb: any) {
    let that = this;
    // debugger
    var request: string = this.toXml123(input);
    var envelopedRequest: string = null != this.envelopeBuilder_ ? this.envelopeBuilder_(request) : request;
    
    // function groupChildren(obj) {
    //   for(key in obj) {
    //     if (typeof obj[key] === 'object') {
    //       groupChildren(obj[key]);
    //     } else {
    //       obj['$'] = obj['$'] || {};
    //       obj['$'][key] = obj[key];
    //       delete obj[key];
    //     }
    //   }
    
    //   return obj;
    // }
    
  
    // var obj = {
    //   Level1: {
    //     attribute: 'value',
    //     Level2: {
    //       attribute1: '05/29/2020',
    //       attribute2: '10',
    //       attribute3: 'Pizza'
    //     }
    //   }
    // };

    // var builder = new xml2js.Builder();
    // var xml123 = builder.buildObject(groupChildren(obj));
    // console.log(xml123)

  

    console.log(envelopedRequest);
    /////////// New code ////////////////
    let headers = {};
    let soapurl: any;
    let token: any;
    // let finalUser = that.globalGet('option');
    let finalUser = localStorage.getItem('option');
    console.log("finalUser: " + finalUser)
    if (finalUser == 'Supplier') {
      headers = {
        'Content-Type': 'text/xml;charset="utf-8"'
      };
      token = localStorage.getItem("Token");
      soapurl = this.urls._baseURL + '&SAMLart=' + token;
    } else if (finalUser == 'MnM') {
      let str = localStorage.getItem("base64String");
      headers = {
        'Authorization': 'Basic ' + str,
        'Content-Type': 'text/xml;charset="utf-8"'
      };
      soapurl = this.urls._authURL;
    }

    console.log("soapurl: " + soapurl)
    console.log("token: " + token)
    // that.http.setSSLCertMode('pinned').then((res) => {
    // debugger
    // let headers = { 'Content-Type': 'text/xml;charset="utf-8"' };
    let xml = envelopedRequest;
    that.http.setDataSerializer('utf8');
    that.http.post(soapurl, xml, headers)
      .then((data) => {
        debugger
        xml2js.parseString(data.data, function (err, result) {
          if (result == undefined) {
            cb(err, result)
          } else {
            cb(err, result)
          }
        });
      }).catch((err) => {
        debugger
        xml2js.parseString(err.error, function (err, result) {
          if (result == undefined) {
            cb(err, result)
          } else {
            cb(err, result)
          }
        });
      })
    // });
  }

  testService123(input, cb: any) {
    let that = this;
    debugger
    var request: string = toXML(input);
    var envelopedRequest: string = null != this.envelopeBuilder_ ? this.envelopeBuilder_(request) : request;

    console.log(envelopedRequest);
    /////////// New code ////////////////
    let headers = {};
    let soapurl: any;
    let token: any;
    // let finalUser = that.globalGet('option');
    let finalUser = localStorage.getItem('option');
    console.log("finalUser: " + finalUser)
    if (finalUser == 'Supplier') {
      headers = {
        'Content-Type': 'text/xml;charset="utf-8"'
      };
      token = localStorage.getItem("Token");
      soapurl = this.urls._baseURL + '&SAMLart=' + token;
    } else if (finalUser == 'MnM') {
      let str = localStorage.getItem("base64String");
      headers = {
        'Authorization': 'Basic ' + str,
        'Content-Type': 'text/xml;charset="utf-8"'
      };
      soapurl = this.urls._authURL;
    }

    console.log("soapurl: " + soapurl)
    console.log("token: " + token)
    
    let xml = envelopedRequest;
    that.http.setDataSerializer('utf8');
    that.http.post(soapurl, xml, headers)
      .then((data) => {
        debugger
        xml2js.parseString(data.data, function (err, result) {
          if (result == undefined) {
            cb(err, result)
          } else {
            cb(err, result)
          }
        });
      }).catch((err) => {
        debugger
        xml2js.parseString(err.error, function (err, result) {
          if (result == undefined) {
            cb(err, result)
          } else {
            cb(err, result)
          }
        });
      })
  }

  _toastrMsg(msg) {
    return this.toastController.create({
      message: msg,
      duration: 2000,
      color: 'dark'
    }).then((toastEl) => {
      toastEl.present();
    })
  }

  _toastrErrorMsg(msg) {
    return this.toastController.create({
      message: msg,
      duration: 3000,
      color: 'danger',
      position: 'middle'
    }).then((toastEl) => {
      toastEl.present();
    })
  }

  logout() {
    let that = this;
    localStorage.clear();
    localStorage.setItem('option', 'MnM');
    that.loadingController.create({
      message: "Logging out...",
      spinner: "bubbles"
    }).then((loadEl) => {
      loadEl.present();
      setTimeout(() => {
        loadEl.dismiss();
        // that.router.navigateByUrl("/login");
        that.navCtrl.navigateRoot(['/login']);

      }, 1000);
    });
  }

  otoa(data) {
    return Array.isArray(data) ? data : [data];
  }
}
