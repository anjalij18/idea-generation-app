import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IonTabs, LoadingController, MenuController } from '@ionic/angular';
import { HeroService } from '../services/hero.service';
declare var $: any;
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  showSearchtab: boolean = true;
  constructor(private menuCtrl: MenuController, public router: Router, public heroService: HeroService, public loadingController: LoadingController) {
    this._getRoles();
   }
  openSideMenu() {
    this.menuCtrl.open();
  }

  gets(tab: IonTabs) {
    debugger
    if ("/tabs/" + tab.getSelected() != this.router.url) {
      this.router.navigateByUrl("idea/menu/tabs/" + tab.getSelected());
    }
  }

  _getRoles() {
    var method: string = 'GetRoles';
    var parameters: {}[] = [];
    this.callSubCode12(parameters, method, '', 'roles');
  }

  _doFurtherWithRoles(obj) {
    let that = this;
    debugger
    if (obj != undefined) {
      console.log("check roles123: ", obj);
      let mnmCount = that.heroService.otoa(obj[0].role).filter((d) => {
        return d.description == "IGP_MnM";
      })
      console.log("check mnmCount: ", mnmCount);

      let supplierCount = that.heroService.otoa(obj[0].role).filter((d) => {
        return d.description == "IGP_Supplier";
      })
      console.log("check supplierCount: ", supplierCount);
      if (mnmCount.length > 0) {
        that.showSearchtab = true;
      } else if (supplierCount.length > 0) {
        that.showSearchtab = false;
      }
    }
  }
  private static rolesParam(): {}[] {
    var parameters: {}[] = [];
    parameters['dn'] = "";
    return parameters;
  }
  callSubCode12(parameters, method, params, key) {
    let that = this;
    if (key == "roles") {
      parameters[method + " xmlns='http://schemas.cordys.com/1.0/ldap'"] = TabsPage.rolesParam();
      parameters = { "SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'": { "SOAP:Body": parameters } };
    }

    that.loadingController.create({
      spinner: 'bubbles',
      message: 'Please wait...'
    }).then((loadEl) => {
      loadEl.present();
      that.heroService.testService1(parameters, function (err, response) {
        loadEl.dismiss();
        if (response) {
          let s = response['SOAP:Envelope'];

          let faultString = s['SOAP:Body'][0]['SOAP:Fault'];
          if (faultString != undefined) {
            if (faultString[0].faultstring[0]._ == "Unable to bind the artifact to a SAML assertion.") {
              that.heroService._toastrErrorMsg("Token is expired. Please logout and login to access the app.");
              return;
            }
            console.log("fault string: ", faultString[0].faultstring[0]._);
          }
          debugger
          if (key == "roles") {
            let a = s['SOAP:Body'][0].GetRolesResponse;
            if (a != undefined) {
              console.log("check roles: ", a[0]);
              let obj = $.cordys.json.findObjects(a[0], "user");
              if (obj != undefined) {
                that._doFurtherWithRoles(obj);
              }
            } else {
              that.heroService._toastrErrorMsg("Error occured while fetching roles data. Please contact administrator.");
            }
          }
        } else {
          console.log("error found in err tag: ", err)
          console.log("no response cought")
          console.log("not getting response becz of err: ", err)
        }
      });
    })

  }

}
